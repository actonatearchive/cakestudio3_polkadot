<?php

class Offer extends AppModel
{
    public $actsAs = array(
        'Upload.Upload' => array(
            'offer_filename' => array(
                'mimetypes'=> array('image/jpg','image/jpeg', 'image/png'),
                'thumbnailMethod'=>"php",
                'extensions'=> array('jpg','png','JPG','PNG','jpeg','JPEG'),
                'thumbnailSizes' => array(
                    'offer' => '200X200',
                ),
                'fields' => array(
                    'dir' => 'file_dir'
                )
            )
        )
    );

}


?>