<?php

class Banner extends AppModel
{
    public $actsAs = array(
        'Upload.Upload' => array(
            'banner_filename' => array(
                'mimetypes'=> array('image/jpg','image/jpeg', 'image/png'),
                'thumbnailMethod'=>"php",
                'extensions'=> array('jpg','png','JPG','PNG','jpeg','JPEG'),
                'thumbnailSizes' => array(
                    'banner' => '620X382',
                    'thumb' => '150x60'
                ),
                'fields' => array(
                    'dir' => 'file_dir'
                )
            )
        )
    );

}


?>