<?php

class Item extends AppModel
{
    var $belongsTo = array('ItemCategory','Parent'=> array('className' => 'Item', 'foreignKey'=>'item_id'));
    // var $belongsToMany = ['ChildItems' => ['className' => 'Item', 'foreignKey' => 'item_id']];
    var $hasMany = array('ItemStock','ItemCity','ItemGalleryPhoto','ChildItems'=> array('className' => 'Item', 'foreignKey'=>'item_id'));

    public $actsAs = array(
        'Upload.Upload' => array(
            'primary_photo' => array(

                'mimetypes'=> array('image/jpg','image/jpeg', 'image/png'),
                'thumbnailMethod'=>"php",
                'extensions'=> array('jpg','png','JPG','PNG','jpeg','JPEG'),
                'thumbnailSizes' => array(
                    'big' => '600x600',
                    'large' => '400x400',
                    'medium' => '200x200',
                    'small' => '100x100'
                ),
                'fields' => array(
                    'dir' => 'primary_photo_directory'
                )
            )
        )
    );

}


?>
