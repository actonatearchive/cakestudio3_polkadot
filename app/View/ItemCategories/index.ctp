<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>

	                <th>Name</th>
	                <th>Type</th>
	                <th>Description</th>
                    <th>Parent</th>
                    <th>Addon Category?</th>
	                   <th>Disabled?</th>
                     <th>Edit</th>
                    <th>Delete</th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php foreach($item_categories as $item) 
	            	{
	            	?>
		            
		            	<tr>
			                <td><?php echo $item['ItemCategory']['name']; ?></td>
			                <td><?php echo $item['ItemType']['name']; ?></td>
			                <td><?php echo $item['ItemCategory']['desc']; ?></td>
			                <td><?php
                                if($item['ItemCategory']['item_category_id'] != null)
                                {
                                    echo $item['ItemCategory']['Parent']['name'];
                                }
                                 ?></td>
							               <td><?php
                                if($item['ItemCategory']['addon_category'] == 0)
                                {
                                    echo "No";
                                } else if ($item['ItemCategory']['addon_category'] == 1)
                                {
                                    echo "Yes";
                                }
                                 ?></td>	 
                            <td><?php
                                if($item['ItemCategory']['disabled'] == 0)
                                {
                                  echo $this->Html->link('Set Disabled',array('controller'=>'item_categories','action'=>'disable',$item['ItemCategory']['id']),array('class'=>'btn btn-xs btn-danger'));
                                } 
                                else if ($item['ItemCategory']['disabled'] == 1)
                                {
                                  echo $this->Html->link('Set Enabled',array('controller'=>'item_categories','action'=>'enable',$item['ItemCategory']['id']),array('class'=>'btn btn-xs btn-success'));
                                }
                                ?>
                            </td>
                            <td><?php
                                echo $this->Html->link('Edit',array('controller'=>'item_categories','action'=>'edit',$item['ItemCategory']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?>
                            </td>
                            <td><?php
                                echo $this->Html->link('Delete',array('controller'=>'item_categories','action'=>'delete',$item['ItemCategory']['id']),array('class'=>'btn btn-xs btn-danger btn-default delete-confirm'));
                                ?>
                            </td>
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>

</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">View Categories</a></li>
      <li class="">
          <?php echo $this->Html->link('Add Category',array('controller'=>'item_categories','action'=>'add')); ?>
      </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>