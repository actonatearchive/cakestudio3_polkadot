<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="add">
            <?php echo $this -> Form -> create('ItemCategory', array('controller' => 'item_categories', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Basic Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Name</label>
                                <?php
                                echo $this -> Form -> input('name', array('div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-name'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Type</label>
                                <?php
                                echo $this -> Form -> input('item_type_id', array('options' => $item_types, 'empty' => 'Select Item Type', 'class' => 'form-control m-b parsley-validated', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Parent Category</label>
                                <?php
                                echo $this -> Form -> input('item_category_id', array('options' => $item_category_parents, 'empty' => 'Choose Parent Category', 'class' => 'form-control m-b parsley-validated', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php
                                echo $this -> Form -> input('desc', array('div' => false, 'label' => false, 'title' => 'Description', 'class' => 'form-control parsley-validated', 'id' => 'item-short-desp'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Long Description</label>
                                <?php
                                echo $this -> Form -> input('long_description', array('div' => false, 'label' => false, 'title' => 'Long Description', 'class' => 'form-control parsley-validated', 'id' => 'item-long-desp'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <?php
                                echo $this -> Form -> input('tags', array('div' => false, 'label' => false, 'title' => 'Tags', 'class' => 'form-control parsley-validated', 'id' => 'item-keyword'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Meta</label>
                                <?php
                                echo $this -> Form -> input('meta', array('div' => false, 'label' => false, 'title' => 'Meta', 'class' => 'form-control parsley-validated', 'id' => 'item-alias'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Permalink</label>
                                <?php
                                echo $this -> Form -> input('permalink', array('div' => false, 'label' => false, 'title' => 'Permalink', 'class' => 'form-control parsley-validated','id' => 'permalink'));
                                ?>
                            </div>
							<div class="checkbox">
                                <label>
                                    <?php echo $this -> Form -> checkbox('addon_category', array('hiddenField' => false)); ?>  Addon Category?
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <?php echo $this -> Form -> checkbox('disabled', array('hiddenField' => false)); ?>  Disabled?
                                </label>
                            </div>
                            <div class="checkbox">
                                <label for="is_festival_cat">
                                    Is Festival category?
                                </label>
                                <?= $this->Form->checkbox('is_festival', array('id'=>'is_festival_cat', 'hiddenField' => '0')) ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">2. Choose Cities</header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-sm-8">
                                <label class="sr-only" for="exampleInputEmail2">Search City</label>
                                <input type="search" class="form-control " id="item-city-search" placeholder="Search City">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group m-l cities-list col-sm-12">

                                <?php


                                echo $this->Form->input('city_id', array(
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => 'checkbox',
                                    'options' => $cities,
                                    'selected'=>array(),
                                ));
                                ?>

                            </div>
                        </div>


                    </div>
                </section>
            </div>
            <div class="col-sm-6">
                <section class="panel">
                    <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Submit', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            </form>
        </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Categories',array('controller'=>'item_categories','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Category</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>

<?php
    $this->start('script');
?>
<script>
    $(document).ready(function(e){
        $("#item-city-search").keyup(function(){

            var search = $(this).val().toLowerCase();

            if(search==""){
                $(".cities-list .checkbox label").css("background",'none');
                $(".cities-list .checkbox label").css("color","#717171");
                return;
            }
            $(".cities-list label").each(function(){
                var tag = $(this).text().toLowerCase();
                if(tag.indexOf(search) != -1){
                    $(this).css("background",'#7dc63c');
                    $(this).css("color","#FFF");
                }
                else{
                    $(this).css("background",'none');
                    $(this).css("color","#717171");
                }

            })

        })
    })
</script>
<?php
    $this->end('script');
?>
