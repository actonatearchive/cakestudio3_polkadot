<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
                    <th>Name</th>
                    <th>Website</th>
	                <th>Contact</th>
	                <th>Email</th>
                    <th></th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php foreach($couriers as $courier) 
	            	{
	            	?>
		            
		            	<tr>
                            <td><?php echo $courier['Courier']['name']; ?></td>
                            <td><?php echo $courier['Courier']['website']; ?></td>
                            <td><?php echo $courier['Courier']['contact']; ?></td>
                            <td><?php echo $courier['Courier']['email']; ?></td>
                            <td>
                                <?php
                                  echo $this->Html->link('Edit',array('controller'=>'couriers','action'=>'edit',$courier['Courier']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?>
								<?php
                                  echo $this->Html->link('Delete',array('controller'=>'couriers','action'=>'delete',$courier['Courier']['id']),array('class'=>'delete-confirm btn btn-xs btn-default'));
                                ?>                                

                            </td>

		                </tr>            
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">View Couriers</a></li>
    <li class="">
        <?php echo $this->Html->link('Add Courier',array('controller'=>'couriers','action'=>'add')); ?>
    </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>