<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
        <?php echo $this -> Form -> create('Courier', array('type'=>'file','controller' => 'couriers', 'action' => 'edit', 'data-validate' => 'parsley', 'role' => 'form')); ?>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">Courier Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Courier Name</label>
                                <?php
                                echo $this -> Form -> input('id', array('type'=>'hidden','div' => false,'value'=>$courier['Courier']['id'], 'label' => false, 'title' => 'Courier Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'courier-name'));
                                
                                echo $this -> Form -> input('name', array('div' => false,'value'=>$courier['Courier']['name'], 'label' => false, 'title' => 'Courier Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'courier-name'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Courier Website</label>
                                <?php
                                echo $this -> Form -> input('website', array('div' => false,'value'=>$courier['Courier']['website'], 'label' => false, 'title' => 'Courier Website', 'class' => 'form-control parsley-validated', 'id' => 'courier-website'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Contact</label>
                                <?php
                                echo $this -> Form -> input('contact', array('div' => false,'value'=>$courier['Courier']['contact'], 'label' => false, 'title' => 'Courier Contact', 'class' => 'form-control parsley-validated', 'id' => 'courier-contact'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <?php
                                echo $this -> Form -> input('email', array('div' => false,'value'=>$courier['Courier']['email'], 'label' => false, 'title' => 'Courier Email', 'class' => 'form-control parsley-validated', 'id' => 'courier-email'));
                                ?>
                            </div>                            
                        </div>


                        <div class="col-sm-6">
                        </div>
                    </div>
                </section>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Courier Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'courier-submit'));  ?>
                    </div>
                </section>
            </div>

        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Couriers',array('controller'=>'couriers','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Edit Courier</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


<?php
  $this->start('script');
?>
<script type="text/javascript">
    $(function(){
        function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                    'Times New Roman', 'Verdana'],
                fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function (idx, fontName) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
            });
            $('a[title]').tooltip({container:'body'});
            $('.dropdown-menu input').click(function() {return false;})
                .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
                .keydown('esc', function () {this.value='';$(this).change();});

            $('[data-role=magic-overlay]').each(function () {
                var overlay = $(this), target = $(overlay.data('target'));
                overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });
            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();
                // $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
            } else {
                $('#voiceBtn').hide();
            }
        };

        initToolbarBootstrapBindings();
        $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );

    });


</script>
<?php
  $this->end('script');
?>