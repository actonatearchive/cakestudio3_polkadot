<?php
$this -> start('main-content');
?>
<h1>
  Enquiry
</h1>
<hr style="margin:5px 00px;">
<div class="tab-content">
    <div class="tab-pane active" id="view">
        <section class="panel">
            <div class="table-responsive">
              <table class="table table-striped m-b-none" data-ride="datatables">
                <thead>
                  <tr>
                   <th>#</th>
                   <th>Name</th>
                    <th>Company Name</th>
                    <th>City</th>
                     <th>Mobile</th>
                      <th>Email</th>
                       <th>Created</th> 
                  </tr>
                </thead>
                    
                    <tbody>
                    
                <?php 
                  $cout= 1;
                foreach($enquires as $enquiry) { ?>
                  <tr>
                    <td><?php echo $cout; ?></td>
                    <td><?php echo $enquiry['Enquiry']['name']; ?></td>
                    <td><?php echo $enquiry['Enquiry']['company_name']; ?></td>
                    <td><?php echo $enquiry['Enquiry']['city']; ?></td>
                    <td><?php echo $enquiry['Enquiry']['mobile']; ?></td>
                    <td><?php echo $enquiry['Enquiry']['email']; ?></td>
                    <td><?php echo $enquiry['Enquiry']['created']; ?></td>
                  </tr>
                    
                <?php  $cout++; } ?>                    
                    </tbody>
              </table>
              <?php ?>
            </div>
      </section>
    </div>
</div>
<?php
$this -> end('main-content');
?>
