<?php
$this -> start('main-content');

$plot_points = "[";
$plot_ticks = "";
foreach($final_orders as $order){
	$the_date = new DateTime($order['order_date']);
	$plot_points .= $order['id_count'].",";
	$plot_ticks .= "<li>".$the_date->format('d/m')."</li>";
}

$plot_points = substr($plot_points,0,-1);
$plot_points .= "]";

/*$plot_points1 = "[";
foreach($final_users as $user){
	$plot_points1 .= $user['id_count'].",";
}

$plot_points1 = substr($plot_points1,0,-1);
$plot_points1 .= "]";*/

$plot_points2 = "";
$plot_ticks2 = "";

$maxcount = 0;
foreach($final_cities as $key=>$value){

	if($maxcount < 10){
		$plot_points2 .= $value.",";
		$plot_ticks2 .= "<li>".$key."</li>";
	}

	$maxcount++;
}
$plot_points2 = substr($plot_points2,0,-1);
?>


        <section class="scrollable wrapper">
          <div class="tab-content">
            <div class="tab-pane active" id="sparkline">




<!--               <div class="row">
                <div class="col-lg-12">
                  <section class="panel">
                    <header class="panel-heading">
                      Past 30 Day Trends : <span class="label bg-danger">Registrations</span>
                    </header>
                    <div class="panel-body">

                      <div class="sparkline" data-type="line" data-resize="true" data-height="185" data-width="100%" data-line-width="1" data-line-color="#FB6B5B" data-spot-color="#afcf6f" data-fill-color="rgba(240,240,240,0.5)" data-highlight-line-color="#000000" data-spot-radius="4" data-data="<?php //echo $plot_points1; ?>" ></div>

                      <ul class="list-inline text-muted axis">

					  <?php //echo $plot_ticks; ?>

					  </ul>
                    </div>
                  </section>
                </div>
              </div>-->

              <div class="row">

                <div class="col-lg-12">
                  <section class="panel">
                    <header class="panel-heading">
                      <i class="icon-signal"></i> This month trends : <span class="label bg-success">Orders (Payment Complete + Payment & Dispatch Complete)</span>
                    </header>
                    <div class="panel-body">

                      <div class="sparkline" data-type="line" data-resize="true" data-height="185" data-width="100%" data-line-width="1" data-line-color="#92CF5C" data-spot-color="#FB6B5B" data-fill-color="rgba(240,240,240,0.5)" data-highlight-line-color="#000000" data-spot-radius="4" data-data="<?php echo $plot_points; ?>" ></div>

                      <ul class="list-inline text-muted axis">

					  <?php echo $plot_ticks; ?>

					  </ul>
                    </div>
                  </section>
                </div>

              </div>





			<div class="row">

                <div class="col-lg-6">
                  <section class="panel">
                    <header class="panel-heading">
                    <i class="icon-sort-by-attributes-alt"></i>Top 10 trending cities till today : <span class="label bg-success">Orders (Payment Complete + Payment & Dispatch Complete)</span> </header>
                    <div class="panel-body text-center">

                      <div class="sparkline inline" data-type="bar" data-height="185" data-bar-width="50" data-bar-spacing="2.5">
							<?php echo $plot_points2; ?>
					  </div>

                      <ul class="list-inline text-muted axis">

							<?php echo $plot_ticks2; ?>

					  </ul>
                    </div>
                  </section>
                </div>

	            <div class="col-lg-6">
	              <div class="list-group bg-white">

					<a href="<?php echo $this->Html->url(array('controller' => 'orders', 'action' => 'completed')); ?>" class="list-group-item">
	                  <i class="icon-chevron-right"></i>
	                  <span class="badge">View All</span>
	                  <i class="icon-ok"></i> Last 5 : <span class="label bg-success">Orders</span>
	                </a>

					<?php
					foreach($top_orders as $torder) {
						?>
						<a href="<?php echo $this->Html->url(array('controller' => 'orders', 'action' => 'vieworder',$torder['Order']['id'])); ?>" class="list-group-item">
						  <i class="icon-chevron-right"></i>
						  <span class="badge bg-info">View Order</span>
						 	<?php echo $torder['Order']['code']; ?>
						</a>
						<?php
					}
					?>

	              </div>
	            </div>

              </div>


          <div class="row">


<!--             <div class="col-sm-6">
              <div class="list-group bg-white">


				<a href="<?php //echo $this->Html->url(array('controller' => 'users', 'action' => 'index')); ?>" class="list-group-item">

                  <i class="icon-chevron-right"></i>
                  <span class="badge">View All</span>
                  <i class="icon-plus-sign-alt"></i> Last 5 : <span class="label bg-danger">Registrations</span>

                </a>

				<?php
				//foreach($top_users as $tuser) {

					?>
					<a href="<?php //echo $this->Html->url(array('controller' => 'users', 'action' => 'viewuser',$tuser['User']['id'])); ?>" class="list-group-item">
					  <i class="icon-chevron-right"></i>
					  <span class="badge bg-info">View User</span>
					  <i class="icon-user"></i> <?php //echo $tuser['User']['username']; ?>
					</a>
					<?php
				//}
				?>

              </div>
            </div> -->


			<div class="col-lg-4">
              <div class="list-group bg-white">
              		<a class="list-group-item">
                  		<i class="icon-ok"></i> Today's : <span class="label bg-success">Orders (Payment Complete + Payment & Dispatch Complete)</span>
                  	</a>

                  	<?php

                  	$today = end($final_orders);
                  	//pr($today);

                  	?>


					<a class="list-group-item">
					  Order count : <i class="icon-hash"></i><?php echo $today['id_count']; ?>
					</a>
					<a class="list-group-item">
					  Total sale : <i class="icon-inr"></i><?php echo $order_gt = $today['total_amt']+$today['additional_chg']+$today['eggless']-$today['discount_val']; ?>
					</a>
					<a class="list-group-item">
					  Average sale : <i class="icon-inr"></i><?php echo round($order_gt/24,2); ?> / hour
					</a>


              </div>
            </div>

			<!-- <div class="col-lg-4">
              <div class="list-group bg-white">
              	<a class="list-group-item">
                  <i class="icon-ok"></i> Last 7 days : <span class="label bg-success">Orders (Payment Complete + Payment & Dispatch Complete)</span>
                  </a>

                  <?php
                  /*$week_cnt = 0;
                  $week_total = 0;
                  for($counter = 24;$counter <= 30; $counter ++) {

                  	$week_cnt += $final_orders[$counter]['id_count'];
                  	$week_total +=  $final_orders[$counter]['total_amt']+$final_orders[$counter]['additional_chg']-$final_orders[$counter]['discount_val'];

					}*/
					?>

					<a class="list-group-item">
					  Order count : <i class="icon-hash"></i><?php //echo $week_cnt; ?>
					</a>
					<a class="list-group-item">
					  Total sale : <i class="icon-inr"></i><?php //echo $week_total; ?>
					</a>
					<a class="list-group-item">
					  Average sale : <i class="icon-inr"></i><?php //echo round($week_total/7,2); ?> / day
					</a>

              </div>
            </div> -->

			<div class="col-lg-4">
              <div class="list-group bg-white">

              	<a class="list-group-item">
                  <i class="icon-ok"></i> This month : <span class="label bg-success">Orders (Payment Complete + Payment & Dispatch Complete)</span>
                 </a>

                  <?php

                  $month_cnt = 0;
                  $month_total = 0;
                  for($counter = 0;$counter < count($final_orders); $counter ++) {

                  	$month_cnt += $final_orders[$counter]['id_count'];
                  	$month_total +=  $final_orders[$counter]['total_amt']+$final_orders[$counter]['additional_chg']+$final_orders[$counter]['eggless']-$final_orders[$counter]['discount_val'];

					}
					?>

					<a class="list-group-item">
					  Order count : <i class="icon-hash"></i><?php echo $month_cnt; ?>
					</a>
					<a class="list-group-item">
					  Total : <i class="icon-inr"></i><?php echo $month_total; ?>
					</a>
					<a class="list-group-item">
					  Average : <i class="icon-inr"></i><?php echo round($month_total/count($final_orders),2); ?> / day
					</a>

              </div>
            </div>


            <div class="col-lg-4">
              <div class="list-group bg-white">

              	<a class="list-group-item">
                  <i class="icon-ok"></i> Past 3 month totals : <span class="label bg-success">Orders (Payment Complete + Payment & Dispatch Complete)</span>
                 </a>



					<a class="list-group-item">
					  <?php echo $month_but1_name; ?> : <i class="icon-inr"></i><?php echo $total_amount_month_but1; ?>
					</a>
					<a class="list-group-item">
					  <?php echo $month_but2_name; ?> : <i class="icon-inr"></i><?php echo $total_amount_month_but2; ?>
					</a>
					<a class="list-group-item">
					  <?php echo $month_but3_name; ?> : <i class="icon-inr"></i><?php echo $total_amount_month_but3; ?>
					</a>

              </div>
            </div>







			</div>


            </div>




          </div>
        </section>

</div>
<?php
$this -> end('main-content');
?>

<?php
$this -> start('main-header');
?>
<header class="header bg-white b-b">
	<p>
        Welcome to PolkaDot! E-Commerce Admin.
    </p>
</header>
<?php
$this -> end('main-header');
?>
