<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
	                <th>Name</th>
	                <th>Code</th>
	                <th>State</th>
	                <th>Zip Prefix</th>
                    <th>Shipping &amp; Handling</th>
                    <th>Disable?</th>
                    <th>Edit</th>
                    <th>Delete</th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php foreach($cities as $item)
	            	{
	            	?>
		            
		            	<tr>
			                <td><?php echo $item['City']['name']; ?></td>
                            <td><?php echo $item['City']['code']; ?></td>
                            <td><?php echo $item['State']['name']; ?></td>
                            <td><?php echo $item['City']['zip_prefix']; ?></td>
                            <td><?php echo $item['City']['shipping_handling_charges']; ?></td>
                            <td>
								<?php
								if($item['City']['disabled']==0){
                                    echo $this->Html->link('Disable',array('controller'=>'cities','action'=>'toggle_disable',$item['City']['id']),array('class'=>'btn btn-xs btn-danger btn-default'));
								} else if ($item['City']['disabled'] == 1) {
									echo $this->Html->link('Enable',array('controller'=>'cities','action'=>'toggle_disable',$item['City']['id']),array('class'=>'btn btn-xs btn-success btn-default'));
								}
                                ?>
                            </td>
							<td><?php
                                    echo $this->Html->link('Edit',array('controller'=>'cities','action'=>'edit',$item['City']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?>
                            </td>
                            <td><?php
                                echo $this->Html->link('Delete',array('controller'=>'cities','action'=>'delete',$item['City']['id']),array('class'=>'btn btn-xs btn-danger btn-default delete-confirm'));
                                ?>
                            </td>
                        </tr>
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#view" data-toggle="tab">View Cities</a></li>
        <li class="">
            <?php echo $this->Html->link('Add City',array('controller'=>'cities','action'=>'add')); ?>
        </li>
    </ul>
</header><?php
$this -> end('main-header');
?>