<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="view">
            <section class="panel">
                <div class="view-items">

                    <?php
                        foreach($promos as $promo){
                            echo '<div class="list-item">';
                                echo $this->Html->image('/files/promotion_photo/img/'.$promo['PromotionPhoto']['id']."/".$promo['PromotionPhoto']['img'],array());
                                echo '<p>'.$promo['PromotionPhoto']['name'].'</p>';
                                echo '<div class="list-item-link">';
                            echo $this->Html->link('Edit',array('controller'=>'promotion_photos','action'=>'edit',$promo['PromotionPhoto']['id']),array('class'=>'btn btn-xs btn-default'));
                                echo $this->Html->link('Delete',array('controller'=>'promotion_photos','action'=>'delete',$promo['PromotionPhoto']['id']),array('class'=>'btn delete-confirm btn-xs btn-danger delete-confirm btn-danger'));
                                echo '</div>';
                            echo '</div>';
                        }
                        if(sizeof($promos)==0)
                        {
                            echo '<p class="large-padding">No promotion photos added.</p>';
                        }
                    ?>
                </div>
            </section>
        </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#view" data-toggle="tab">View Promotion Photos</a></li>
            <li class="">
                <?php echo $this->Html->link('Add Promotion Photos',array('controller'=>'promotion_photos','action'=>'add')); ?>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>