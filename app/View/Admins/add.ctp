<?php $this->Html->scriptStart(array('inline' => false));  ?>

$("#custom").spectrum({
    color: "#333",
	showInput: true
});
    
<?php $this->Html->scriptEnd(); ?>

<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
	
        <?php echo $this -> Form -> create('Admin', array('controller' => 'admins', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>
		
        <div class="row">
            <div class="col-sm-10">
                <section class="panel">
                    <header class="panel-heading font-bold">Admin Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Name</label>
                                <?php
                                echo $this -> Form -> input('name', array('div' => false,'placeholder'=>'Enter name', 'label' => false, 'title' => 'Name', 'class' => 'form-control parsley-validated', 'id' => 'banner-caption'));
                                ?>
                            </div>
							
							<div class="form-group">
                                <label>Userame</label>
                                <?php
                                echo $this -> Form -> input('username', array('div' => false,'placeholder'=>'Enter username', 'label' => false, 'title' => 'Name', 'class' => 'form-control parsley-validated', 'id' => 'banner-caption'));
                                ?>
                            </div>			
                            <div class="form-group">
                                <label>Admin type</label>
                                <?php
                                echo $this -> Form -> input('admin_type_id', array('div' => false, 'label' => false, 'title' => 'Name', 'class' => 'form-control parsley-validated', 'id' => 'banner-caption','options'=>$admin_types,'empty'=>'Select admin type'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <?php
                                echo $this -> Form -> input('password', array('class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'banner-link', 'div' => false,'type'=>'password','placeholder'=>'Enter password'));
                                ?>
                            </div>
							<?php
                                echo $this->Form->input('status',array('type'=>'hidden','value'=>0));
                            ?>
                             <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
										
                        </div>
                       
                    </div>
					
                </section>

            </div>
            			
        </div>
		
		
		
      

        </div>
        <?php echo $this->Form->end(); ?>
    </div>

    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('Admin Listing',array('controller'=>'admins','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Admin</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


<?php
  $this->start('script');
?>
<script type="text/javascript">

    $(document).ready(function(){

    });
</script>
<?php
  $this->end('script');
?>