<html>
<head>
	<?php
		echo $this->Html->css('bootstrap2');
		echo $this->Html->css('sb-admin');
		
		echo $this->fetch('script');
		
		echo $this->fetch('css');
		
	?>
	

	
	<title><?php echo $title_for_layout;?></title>	
</head>
<body style="background-color: #222222">
  <div class="container">
	<div class="row">
		<div class="col-lg-4 col-lg-offset-4">
			
			<?php echo $content_for_layout ?>
		</div>
	</div>
				
				
  </div>

   	<?php
		echo $this->Html->script('jquery.min');
		echo $this->Html->script('bootstrap');
	?>
  </body>
  </html>