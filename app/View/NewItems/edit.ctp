<?php
$this -> start('main-content');
?>
<!-- Experimental Code -->
<!-- ngController Start Here -->
<div ng-controller="EditItemController">
<!-- ngController Start here -->

<section class="content-header">
     <h1>
       Edit Item
       <small>Items</small>
     </h1>
     <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Items</li>
     </ol>
</section>

<!-- Hidden Form Fields -->
<input type="hidden" id="child_counts" value="<?= $child_counts ?>" />
<!-- =-=-=-=-=-=-=-=-=-=-=-=-=- -->
<div class="tab-content">
    <?php echo $this -> Form -> create('Item', array('type'=>'file', 'data-validate' => 'parsley', 'role' => 'form')); ?>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading font-bold">Item Details</header>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Item Name</label>
                                <?= $this -> Form -> input('name',
                                array('div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-name','onchange'=>'updateURLSlag()','value' => $data['Item']['name']
                                )); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Keywords</label>
                                <?= $this -> Form -> input('keyword', array('div' => false, 'label' => false, 'title' => 'Keywords', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'keyword', 'value' => $data['Item']['keyword'])); ?>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Item Category</label>
                                <?= $this -> Form -> input('item_category_id', array('options' => $categories, 'class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'item-category', 'label' => false, 'div' => false, 'value' => $data['Item']['item_category_id'])); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Alias</label>
                                <?= $this -> Form -> input('alias', array('div' => false, 'label' => false, 'title' => 'Keywords', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'alias', 'value' => $data['Item']['alias'])); ?>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Short Description</label>
                                <?= $this -> Form -> input('short_desc', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'item-category', 'label' => false, 'div' => false, 'value' => $data['Item']['short_desc'])); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Shipping Charges</label>
                                <?= $this -> Form -> input('shipping_charges', array('div' => false, 'label' => false, 'title' => 'Keywords', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'alias','value'=>$data['Item']['shipping_charges'])); ?>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>URL Slag</label>
                                <?= $this -> Form -> input('url_slag', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false,'value'=>$data['Item']['url_slag'])); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>SKU Code</label>
                                <?= $this -> Form -> input('sku_code', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'sku_code', 'label' => false, 'div' => false,'placeholder'=>'Eg. SKU0001','value'=>$data['Item']['sku_code'])); ?>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    <?= $this -> Form -> checkbox('featured', array('hiddenField' => false,'checked'=>$data['Item']['featured'])); ?> Set as Featured
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                    <?= $this -> Form -> checkbox('is_addon', array('hiddenField' => false,'checked'=>$data['Item']['is_addon'])); ?> Is addon?
                                </label>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
        </div>
    </div>


    <!-- =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- -->
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading font-bold">Item Variants</header>
                <div class="panel-body">

                    <!-- <div class="row">
                        <div class="col-sm-1">
                            <div class="form-group">
                                <button type="button" class="btn btn-info btn-xs" ng-click="selectAllCities('base_city')">Check All</button>
                                <button type="button" class="btn btn-warning btn-xs" ng-click="clearAllCities('base_city')">Uncheck</button>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Cities</label>
                                <?= $this -> Form -> input('base_city_id', array('options' => $cities, 'class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'base_city', 'label' => false, 'div' => false, 'multiple'=>"multiple",'value'=>$data['base_city_id'])); ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Variant name</label>
                                <?= $this -> Form -> input('base_variant_name', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false,'placeholder'=>'Eg. 1kg','value' => $data['Item']['variant_name'])); ?>
                            </div>
                        </div>


                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Price</label>
                                <?= $this -> Form -> input('base_price', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false,'value' => $data['ItemCity'][0]['price'])); ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Discount Price</label>
                                <?= $this -> Form -> input('base_discount_price', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false,'value' => $data['ItemCity'][0]['discount_price'])); ?>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label></label>
                                <button type="button" class="btn btn-s-md btn-success" ng-click="addVariant()">Add</button>
                            </div>
                        </div>

                    </div> -->

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label></label>
                            <button type="button" class="btn btn-block btn-success" ng-click="addVariant()">Add</button>
                        </div>
                    </div>

                    <!-- Variants -->
                    <?php $idx = 0; ?>
                    <?php foreach ($data['ChildItems'] as $key => $value): ?>

                    <div class="row">
                            <div class="col-sm-1">
                                <div class="form-group">
                                    <!-- <label></label> -->
                                    <button type="button" class="btn btn-xs btn-info" ng-click="selectAllCities('child_variant_<?= $idx ?>')">Select All</button>
                                    <button type="button" class="btn btn-xs btn-warning" ng-click="clearAllCities('child_variant_<?= $idx ?>')">Unselect</button>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Cities</label>
                                    <?= $this -> Form -> input('city_id'.$idx, array('options' => $cities, 'class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'child_variant_'.$idx, 'label' => false, 'div' => false, 'multiple'=>'multiple', 'value' => $value['city_id'])); ?>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Variant name</label>
                                    <?= $this -> Form -> input('variant_name.', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false,'placeholder'=>'Eg. 1kg', 'value' => $value['variant_name'])); ?>
                                </div>
                            </div>


                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Price</label>
                                    <?= $this -> Form -> input('price.', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false, 'value' => $value['ItemCity'][0]['price'])); ?>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Discount Price</label>
                                    <?= $this -> Form -> input('discount_price.', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false, 'value' => $value['ItemCity'][0]['discount_price'])); ?>
                                </div>
                            </div>

                            <div class="col-sm-1">
                                <div class="form-group">
                                    <label></label>
                                    <br/>
                                    <!-- <button type="button" class="btn btn-s-md btn-danger" ng-click="removeVariant('child_variant_<?= $idx ?>')">Remove</button> -->
                                    <?=
                                        $this->Html->link($this->Html->tag('i', 'Remove',array('class' => 'fa fa-times')),array('controller'=>'new_items','action'=>'removeItemChild/'.$value['id'].'/'.$data['Item']['id']),array('class'=>'btn btn-danger btn-circle', 'escape' => false,'onclick'=>'return confirm(\'Are you sure? This action wont be rollback.\')'));
                                    ?>
                                </div>
                            </div>

                    </div>
                    <?php $idx++; ?>
                    <?php endforeach; ?>
                    <!-- =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- -->

                    <!-- ANGULAR -->

                    <div class="row" ng-repeat="var in obj.variants">

                        <div class="col-sm-1">
                            <div class="form-group">
                                <!-- <label></label> -->
                                <button type="button" class="btn btn-xs btn-info" ng-click="selectAllCities(var.id)">Select All</button>
                                <button type="button" class="btn btn-xs btn-warning" ng-click="clearAllCities(var.id)">Unselect</button>

                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Cities</label>
                                <?= $this -> Form -> input('city_id{{$index -- org_idx}}', array('options' => $cities, 'class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'{{var.id}}', 'label' => false, 'div' => false, 'multiple'=>'multiple')); ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Variant name</label>
                                <?= $this -> Form -> input('variant_name.', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false,'placeholder'=>'Eg. 1kg')); ?>
                            </div>
                        </div>


                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Price</label>
                                <?= $this -> Form -> input('price.', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false)); ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Discount Price</label>
                                <?= $this -> Form -> input('discount_price.', array('class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'url_slag', 'label' => false, 'div' => false)); ?>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group">
                                <label></label>
                                <button type="button" class="btn btn-s-md btn-danger" ng-click="removeVariant(var.id)">Remove</button>

                            </div>
                        </div>

                    </div>

                    <!-- ENDS HERE -->
                </div>
            </section>
        </div>
    </div>
    <!-- =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- -->

    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading font-bold">Upload Primary Photo</header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">PNG, JPG or GIF</label>
                    <div class="col-sm-10">
                        <?php echo $this -> Form -> input('primary_photo', array('type'=>'file','div' => false, 'label' => false, 'title' => 'Primary Photo', 'id' => 'item-primary-photo')); ?>
                    </div>
                </div>
                <br/>
                <div class="form-group">
                    <div class="col-sm-12">
                        <?=     $this->Html->image('/files/item/primary_photo/'.$data['Item']['primary_photo_directory']."/medium_".$data['Item']['primary_photo'],array('style'=>'height: 250px;width: 250px;'));
                        ?>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <!-- PROCEED -->

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <div class="panel-body">
                    <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Proceed', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item1'));  ?>
                </div>
            </section>
        </div>

    </div>
    <?php echo $this->Form->end(); ?>

    <!-- ok -->
</div>
<!-- ngController Over here -->
</div>
<!-- NgController-->
<!-- TEST -->
<?php
$this -> end('main-content');
?>
