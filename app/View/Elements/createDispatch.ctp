<button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#dispatch-order-model-<?php echo $order['Order']['id']; ?>">Create</button>
		                                
<!-- Modal -->
<div class="modal fade" id="dispatch-order-model-<?php echo $order['Order']['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create dispatch for order id #<?php echo $order['Order']['code']; ?>
       </h4>
      </div>
      <div class="modal-body">
        <?php echo $this -> Form -> create('OrderDispatch', array('url' => array('controller' => 'orderdispatches','action' => 'add'))); ?>

        <input type="hidden" name="data[OrderDispatch][order_id]" value="<?php echo $order['Order']['id'] ?>" /> 

        <div class="row">
            <div class="col-sm-12">
                <hr />
				<div class="col-sm-12">
                <h4>Select Order Items For Dispatch</h4>
					<?php
                    $item_count=0;
					foreach($order['OrderItem'] as $item){
						

						echo $this->Html->link('View Item',array('controller'=>'Items','action'=>'viewitem',$item['Item']['id']),array('class'=>'btn btn-sm btn-info','target'=>'_blank'));
						?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php
						echo '<input type="checkbox" checked name="data[OrderDispatch][order_item_ids]['.$item_count.']" value="'.$item['OrderItem']['id'].'" required="required"></input>';
						
						echo '<span class=""><h3 style="color:#c0493b;display:inline"><b> '.$item['Item']['name'].'</b></h3></span><br />';

                        $item_count++;
					}
					?>
				</div>
            </div>
		</div>


                <br />
                
                <div class="row">
                    <div class="col-sm-12">
                    	<div class="col-sm-12">
                    		
                    		<div class="form-group">
                				<label>Bill Number</label><br/>
								<?php
									echo $this->Form->input('number',array('div' => false, 'label' => false, 'class' => 'form-control parsley-validated', 'required' => 'required'));
								?>
							</div>
							
                    		<div class="form-group">
                				<label>Courier Name</label><br/>
								<?php
									echo $this->Form->input('courier_id',array('options' => $couriers,'empty'=>'Self-Delivery', 'div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
								?>
							</div>	

							<div class="form-group">
                				<label>Tracking Code</label><br/>
								<?php
									echo $this->Form->input('tracking_code',array('div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
								?>
							</div>						
							
                    	</div>
                    	<br/>
                    	<div class="col-sm-12"><br/>
							<div class="form-group">
                				<label>Time</label><br />
								<?php
									$todays = new DateTime();
									$the_datetime = $todays->format('d-m-Y H:i:s');
									echo $this->Form->input('time',array('div' => false, 'label' => false, 'class' => 'combodate', 'required' => 'required', 'data-format'=>'DD-MM-YYYY HH:mm:ss','data-template'=>'DD  MM  YYYY  -  HH : mm : ss','value'=> $the_datetime));
								?>
							</div>                    		
                    		
                    	</div>

                    </div>
                </div>
           </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Dispatch</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>