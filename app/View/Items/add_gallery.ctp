<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
					<th>Add Gallery ?</th>
                    <th>Name</th>
                    <th>Alias</th>
	                <th>SKU</th>
                    <!-- <th>Photo</th>   -->
	                <th>Variant Name</th>
	                <th>Parent</th>
	                <th>Category</th>
	              </tr>
	            </thead>

	            	<tbody>

	            <?php foreach($items as $item)
	            	{
	            	?>

		            	<tr>
                            <td>
                                <?php
                                    echo $this->Html->link('Manage Gallery',array('controller'=>'item_gallery_photos','action'=>'manage_gallery',$item['Item']['id']),array('class'=>'btn btn-xs btn-success'));
                                ?>
                            </td>						
                            <td><?php echo $item['Item']['name']; ?></td>
                            <td><?php echo $item['Item']['alias']; ?></td>
                            <td><?php echo strtoupper($item['Item']['sku_code']); ?></td>
                           <!-- <td>
                                <?php
                                    if(isset($item['Item']['primary_photo']) && isset($item['Item']['primary_photo_directory'])){
                                         echo $this->Html->image('/files/'.$item['Item']['primary_photo_directory'].'/small_'.$item['Item']['primary_photo']);
                                    }
                                ?>
                            </td> -->
			                <td><?php echo $item['Item']['variant_name']; ?></td>
                            <td><?php
                                if(isset($item['Item']['item_id'])){
                                    if(isset($item['Parent']['variant_name'])){
                                        echo $item['Parent']['name'].' ('.$item['Parent']['variant_name'].')';
                                    }
                                    else{
                                        echo $item['Parent']['name'];
                                    }
                                }
                                 ?></td>
                            <td><?php
                                if(isset($item['Item']['item_category_id'])){
                                    echo $item['ItemCategory']['name'];
                                }
                                 ?></td>


		                </tr>

	            	<?php
					}
	            	?>

	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="">
		<?php echo $this->Html->link('View Items',array('controller'=>'items','action'=>'index')); ?>
	</li>
    <li class="">
        <?php echo $this->Html->link('Add Item',array('controller'=>'items','action'=>'add')); ?>
    </li>    
	<li class="active"><a href="#view" data-toggle="tab">Add Gallery</a></li>
    <li class="">
        <?php echo $this->Html->link('City Wise Items',array('controller'=>'items','action'=>'city')); ?>
    </li>    
  </ul>
</header>
<?php
$this -> end('main-header');
?>