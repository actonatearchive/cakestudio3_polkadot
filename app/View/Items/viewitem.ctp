<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">

        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Basic Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Item Name</label>
                                <?php
                                echo $this -> Form -> input('id', array('type'=>'hidden','default'=>$selectedItem['Item']['id'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-id'));
                                echo $this -> Form -> input('name', array('default'=>$selectedItem['Item']['name'],'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-name','disabled'=>'disabled'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Category</label>
                                <?php
                                echo $this -> Form -> input('item_category_id', array('default'=>$selectedItem['Item']['item_category_id'],'options' => $item_categories,'empty'=>'Choose a category', 'class' => 'form-control m-b parsley-validated', 'data-required' => 'true','id'=>'item-category', 'label' => false, 'div' => false,'disabled'=>'disabled'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Parent Item</label>
                                <?php
                                    echo $this -> Form -> input('item_id', array('default'=>$selectedItem['Item']['item_id'],'options' => $item_parents, 'empty' => 'Choose Parent Item', 'class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'item-parent', 'div' => false,'disabled'=>'disabled'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Variant Name</label>
                                <?php
                                echo $this -> Form -> input('variant_name', array('default'=>$selectedItem['Item']['variant_name'],'div' => false, 'label' => false, 'title' => 'Variant Name', 'class' => 'form-control parsley-validated', 'id' => 'item-variant-name','disabled'=>'disabled'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Short Description</label>
                                <?php
                                echo $this -> Form -> input('short_desc', array('default'=>$selectedItem['Item']['short_desc'],'div' => false, 'label' => false, 'title' => 'Short Description', 'class' => 'form-control parsley-validated', 'id' => 'item-short-desp','disabled'=>'disabled'));
                                ?>
                            </div>
                        </div>


                        <div class="col-sm-6">

                            <div class="form-group">
                                <label>Keywords</label>
                                <?php
                                echo $this -> Form -> input('keyword', array('default'=>$selectedItem['Item']['keyword'],'div' => false, 'label' => false, 'title' => 'Keywords', 'class' => 'form-control parsley-validated', 'id' => 'item-keyword','disabled'=>'disabled'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Alias</label>
                                <?php
                                echo $this -> Form -> input('alias', array('default'=>$selectedItem['Item']['alias'],'div' => false, 'label' => false, 'title' => 'Alias', 'class' => 'form-control parsley-validated', 'id' => 'item-alias','disabled'=>'disabled'));
                                ?>
                            </div>

                            <div class="form-group">
                                <label>Shipping Charges</label>
                                <?php
                                if($selectedItem['Item']['shipping_charges'] != -1)
                                {
                                    echo $this -> Form -> input('shipping_charges', array('div' => false,'default'=>$selectedItem['Item']['shipping_charges'],'type'=>'number', 'label' => false, 'title' => 'Shipping Charges', 'class' => 'form-control parsley-validated', 'id' => 'item-shipping-charges','disabled'=>'disabled'));
                                }
                                else{
                                    echo $this -> Form -> input('shipping_charges', array('div' => false,'default'=>"",'type'=>'number', 'label' => false, 'title' => 'Shipping Charges', 'class' => 'form-control parsley-validated', 'id' => 'item-shipping-charges','disabled'=>'disabled'));
                                }

                                ?>
                            </div>

                            <?php
                                if($selectedItem['Item']['stock_type']==1 && isset($selectedItem['ItemStock'][0]))
                                {
                                    echo $this -> Form -> input('stock_type', array('type'=>'hidden','default'=>$selectedItem['Item']['stock_type'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-stock_type','disabled'=>'disabled'));
                            ?>
                                    <div class="form-group">
                                        <label class="label-price">Price</label>
                                        <?php

                                        echo $this -> Form -> input('price', array('default'=>$selectedItem['ItemStock'][0]['price'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-price','disabled'=>'disabled'));
                                        ?>
                                    </div>

                                    <div class="form-group">
                                        <label class="label-discount">Discounted Price</label>
                                        <?php
                                        echo $this -> Form -> input('discount_price', array('default'=>$selectedItem['ItemStock'][0]['discount_price'],'div' => false, 'label' => false, 'title' => 'Discounted Price', 'class' => 'form-control parsley-validated', 'id' => 'item-discount_price','disabled'=>'disabled'));
                                        ?>
                                    </div>
                            <?php
                                }
                                else if($selectedItem['Item']['stock_type']==1 && !isset($selectedItem['ItemStock'][0])){
                                    echo $this -> Form -> input('stock_type', array('type'=>'hidden','default'=>$selectedItem['Item']['stock_type'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-stock_type','disabled'=>'disabled'));
                            ?>
                                    <div class="form-group">
                                        <label class="label-price">Price</label>
                                        <?php
                                        echo $this -> Form -> input('price', array('div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-price','disabled'=>'disabled'));
                                        ?>
                                    </div>

                                    <div class="form-group">
                                        <label class="label-discount">Discounted Price</label>
                                        <?php
                                        echo $this -> Form -> input('discount_price', array('div' => false, 'label' => false, 'title' => 'Discounted Price', 'class' => 'form-control parsley-validated', 'id' => 'item-discount_price','disabled'=>'disabled'));
                                        ?>
                                    </div>
                            <?php
                                }
                                else{
                                    echo $this -> Form -> input('stock_type', array('type'=>'hidden','default'=>$selectedItem['Item']['stock_type'],'div' => false, 'label' => false, 'title' => 'Item Price', 'class' => 'form-control parsley-validated', 'id' => 'item-stock_type','disabled'=>'disabled'));
                                }

                            ?>



                        </div>
                    </div>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">2. Set Description</header>
                    <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Item Description</label>
                                <div class="col-sm-10">
                                    <div class="btn-toolbar m-b-sm btn-editor" data-role="editor-toolbar" data-target="#editor">
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="icon-font"></i><b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                                                <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                                                <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                                            </ul>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="icon-strikethrough"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="icon-list-ul"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="icon-list-ol"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="icon-indent-right"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="icon-link"></i></a>
                                            <div class="dropdown-menu">
                                                <div class="input-group m-l-xs m-r-xs">
                                                    <input class="form-control input-sm" placeholder="URL" type="text" data-edit="createLink">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-white btn-sm" type="button">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="btn btn-white btn-sm" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="icon-cut"></i></a>
                                        </div>

                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm" title="" id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="icon-picture"></i></a>
                                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 39px; height: 29px;">
                                        </div>
                                        <div class="btn-group">
                                            <a class="btn btn-white btn-sm" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                                            <a class="btn btn-white btn-sm" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
                                        </div>
                                        <input type="text" class="form-control-trans pull-left" data-edit="inserttext" id="voiceBtn" x-webkit-speech="" "disabled"="disabled" style="width:25px;height:28px;">
                                    </div>
                                    <div id="editor" class="form-control" style="overflow:scroll;height:150px;max-height:150px" contenteditable="true">
                                        <?php
                                            echo html_entity_decode($selectedItem['Item']['long_desc']);
                                        ?>
                                    </div>
                                    <?php echo $this -> Form -> input('long_desc', array('default'=>html_entity_decode($selectedItem['Item']['long_desc']),'type'=>'textarea','div' => false, 'label' => false, 'title' => 'Item Description', 'class' => 'hide', 'id' => 'item-long_desc','disabled'=>'disabled')); ?>
                                </div>
                            </div>
                    </div>
                </section>
            </div>
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">4. Cities</header>
                    <div class="panel-body">
                        <div class="row">
                        
                        </div>
                        <div class="row">
                        <div class="form-group m-l cities-list col-sm-4">

                        <?php


                        echo $this->Form->input('city_id', array(
                            'label' => false,
                            'type' => 'select',
                            'multiple' => 'checkbox',
                            'options' => $cities,
                            'selected'=>$selectedCitiesList,
							'disabled'=> 'disabled'
                        ));
                        ?>

                        </div>
                        </div>


                    </div>
                </section>
            </div>
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">4. Primary Photo</header>
                    <div class="panel-body">
                            <div class="form-group">
                                
                                <div class="col-sm-5">
                                    <?php echo $this->Html->image('/files/item/primary_photo/'.$selectedItem['Item']['id']."/".$selectedItem['Item']['primary_photo'],array('height'=>'200px')); ?>
                                </div>
                            </div>
                    </div>
                </section>
            </div>

        <div class="row">
            <div class="col-sm-12">
                
                <section class="panel">
                    <header class="panel-heading font-bold">5. Gallery Photo/s </header>
                    <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                
                                    
                                    <?php 
                                    
                                    foreach($itemGalleryPhotos as $itemgp)
                                    {
                                    
                                        echo $this->Html->image('/files/item_gallery_photo/file_name/'.$itemgp['ItemGalleryPhoto']['file_dir']."/".$itemgp['ItemGalleryPhoto']['file_name'],array('height'=>'200px'));
                                        ?>
                                        &nbsp;&nbsp;&nbsp;
                                        <?php
                                    
                                    }
                                    ?>
                                    
                                </div>

                            </div>
                    </div>
                </section>              

            </div>
        </div>              

        </div>

        </div>
    </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Items',array('controller'=>'items','action'=>'index')); ?>
            </li>
			 <li class="">
				<?php echo $this->Html->link('Add Item',array('controller'=>'items','action'=>'add')); ?>
			</li>
			<li class="">
                <?php echo $this->Html->link('Add Gallery',array('controller'=>'items','action'=>'add_gallery')); ?>
            </li>
            <li class="">
                <?php echo $this->Html->link('City Wise Items',array('controller'=>'items','action'=>'city')); ?>
            </li>            
			<li class="active"><a href="#add" data-toggle="tab">View Item (<?php echo $selectedItem['Item']['name']; ?>)</a></li>

        </ul>
    </header>
<?php
$this -> end('main-header');
?>