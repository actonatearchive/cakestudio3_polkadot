<?php
$this -> start('main-content');
?>

    <div class="tab-content">
    <div class="tab-pane active" id="add">
        <?php echo $this -> Form -> create('Item', array('controller' => 'items', 'action' => 'item_of_month', 'data-validate' => 'parsley', 'role' => 'form')); ?>
        <div class="row">
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">Auto-fill from Existing Item</header>
                    <div class="panel-body">
                        <div class="row">
                            <?php    
                            foreach($cities as $city){
                            ?>
                            <div class="col-sm-3">
                                <label><?php echo $city['City']['name']; ?></label>                            
                            </div>
                            <div class="col-sm-9">
                                <?php
                                    if(!isset($city['Item'])){
                                        echo $this -> Form -> input('autofill', array('empty' => 'Choose Autofill Item', 'class' => 'form-control m-b parsley-validated autofill-list','option'=> $autofill, 'label' => false, 'div' => false,'autocomplete' => 'on'));
                                        echo $this->Form->input($city['City']['id'],array('type'=>'hidden','class'=>'autofill-value','label'=>false));                                    
                                    }
                                    else{
                                        $itemName = $city['Item']['name'];
                                        if(isset($city['Item']['variant_name']) || empty($city['Item']['variant_name'])){     
                                            $itemName = $itemName.' ('.$city['Item']['variant_name'].') ';                                        
                                        }
                                        if(isset($city['Item']['alias']) || empty($city['Item']['alias'])){     
                                            $itemName = $itemName.' ('.$city['Item']['alias'].') ';                                        
                                        }                                   

                                        echo $this -> Form -> input('autofill', array('value'=>$itemName,'empty' => 'Choose Autofill Item', 'class' => 'form-control m-b parsley-validated autofill-list','option'=> $autofill, 'label' => false, 'div' => false,'autocomplete' => 'on'));
                                        echo $this->Form->input($city['City']['id'],array('type'=>'hidden','value'=>$city['Item']['id'],'class'=>'autofill-value','label'=>false));                                    
                                    }
                                ?>
                            </div>
                            <?php 
                             }
                            ?>

                        </div>
                    </div>
                </section>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                    </div>
                </section>
            </div>

        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="active">
                <?php echo $this->Html->link('Item of the Month',array('controller'=>'items','action'=>'item_of_month')); ?>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


<?php
  $this->start('script');
?>
<script type="text/javascript">
    $(function(){
        function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                    'Times New Roman', 'Verdana'],
                fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function (idx, fontName) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
            });
            $('a[title]').tooltip({container:'body'});
            $('.dropdown-menu input').click(function() {return false;})
                .change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
                .keydown('esc', function () {this.value='';$(this).change();});

            $('[data-role=magic-overlay]').each(function () {
                var overlay = $(this), target = $(overlay.data('target'));
                overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });
            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();
                // $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
            } else {
                $('#voiceBtn').hide();
            }
        };
        function showErrorAlert (reason, detail) {
            var msg='';
            if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
            else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
                '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
        };
        initToolbarBootstrapBindings();
        $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );

    });

    $(document).ready(function(){

        // This is for autocomplete of autofill item and filling the form with item
        $( ".autofill-list" ).autocomplete({
            source: "find",
            minLength: 3,
            delay: 1,
       
            focus: function( event, ui ) {
                $(this).val( ui.item.name );
                return false;
            },            
            
            select: function( event, ui ) {
                var elem = $(this);
                var url = baseUrl + "items/json/" + ui.item.id;
                $.ajax({
                    url: url,
                    context: document.body
                }).done(function(data) {

                    axinusUI.hideLoader();
                    if(data == "error"){
                        alert("Sorry an error occurred. Could not retrieve item details.");
                        return;
                    }
                    var item = $.parseJSON(data);
                    elem.parent().find(".autofill-value").val(item.Item.id);
                });

                return false;
            }

        });         

        $('.autofill-list').each(function() {
            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $( "<li>" )
                .append( "<a>" + item.name + "</a>" )
                .appendTo( ul );
            };
        });
    });
</script>

<?php
  $this->end('script');
?>
