<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">



    	<section class="panel">

	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
                    <th>Name</th>
                    <th></th>
	              </tr>
	            </thead>

	            	<tbody>

	            <?php foreach($cities as $key=>$value)
	            	{
	            	?>

		            	<tr>
                            <td><?php echo $value; ?></td>                            
                            <td> 
                                <?php
                                echo $this->Html->link('View City Items',array('controller'=>'items','action'=>'cityitems',$key),array('class'=>'btn btn-xs btn-default','target'=>'_blank'));                               
                                ?>

                            </td>

		                </tr>

	            	<?php
					}
	            	?>

	            	</tbody>
	          </table>

	        </div>

      </section>

    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="">
        <?php echo $this->Html->link('View Items',array('controller'=>'items','action'=>'index')); ?>
    </li>    
    <li class="">
        <?php echo $this->Html->link('Add Item',array('controller'=>'items','action'=>'add')); ?>
    </li>
	<li class="">
                <?php echo $this->Html->link('Add Gallery',array('controller'=>'items','action'=>'add_gallery')); ?>
    </li>
    <li class="active"><a href="#add" data-toggle="tab">City Wise Items</a></li>    
  </ul>
</header>
<?php
$this -> end('main-header');
?>