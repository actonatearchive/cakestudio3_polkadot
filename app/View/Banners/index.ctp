<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="view">
            <section class="panel">
                <div class="view-items">

                    <?php
                        foreach($banners as $banner){
                            echo '<div class="list-item">';
                                echo $this->Html->image('/files/banner/banner_filename/'.$banner['Banner']['id']."/".$banner['Banner']['banner_filename'],array());
                                echo '<p>'.$banner['Banner']['caption'].'</p>';
                                echo '<div class="list-item-link">';
                            echo $this->Html->link('Edit',array('controller'=>'banners','action'=>'edit',$banner['Banner']['id']),array('class'=>'btn btn-xs btn-default'));
                                echo $this->Html->link('Delete',array('controller'=>'banners','action'=>'delete',$banner['Banner']['id']),array('class'=>'btn delete-confirm btn-xs btn-danger delete-confirm btn-danger'));
                                echo '</div>';
                            echo '</div>';
                        }
                        if(sizeof($banners)==0)
                        {
                            echo '<p class="large-padding">No Banners added.</p>';
                        }
                    ?>
                </div>
            </section>
        </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#view" data-toggle="tab">View Banners</a></li>
            <li class="">
                <?php echo $this->Html->link('Add Banner',array('controller'=>'banners','action'=>'add')); ?>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>