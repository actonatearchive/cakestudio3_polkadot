<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
                    <th>Code</th>
                    <th>Type</th>
	                <th>Item</th>
	                <th>Item Category</th>
	                <th>Percent</th>
	                <th>Value</th>
	                <th>Expiry Date</th>
	                <th>Created</th>
	                <th>Active ?</th>
                    <th></th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php foreach($coupons as $coupon) 
	            	{
	            	?>
		            
		            	<tr>
                            <td><?php echo $coupon['Coupon']['code']; ?></td>
                            <td>
                            <?php
                            if($coupon['Coupon']['type']==0) {
                            	echo "Percent";
                            } elseif($coupon['Coupon']['type']==1){
                            	echo "Value";
                            }
                            ?>
                            </td>
                            <!--Show item name and item category name here-->
                            <td><?php echo $coupon['Coupon']['item_id']; ?></td>
                            <td><?php echo $coupon['Coupon']['item_category_id']; ?></td>
                            <td><?php echo $coupon['Coupon']['percent']; ?></td>
                            <td><?php echo $coupon['Coupon']['value']; ?></td>
							<td><?php echo $coupon['Coupon']['expiry_date']; ?></td>
							<td><?php echo $coupon['Coupon']['created']; ?></td>
							<td>
                                <?php
                                if($coupon['Coupon']['active']==0)
                                {
                                    echo $this->Html->link('Set Active',array('controller'=>'coupons','action'=>'set_active',$coupon['Coupon']['id']),array('class'=>'btn btn-xs btn-success'));
                                }
                                else{
                                    echo $this->Html->link('Set Inactive',array('controller'=>'coupons','action'=>'set_inactive',$coupon['Coupon']['id']),array('class'=>'btn btn-xs btn-danger'));
                                }

                                ?>
							</td>

                            <td>
                                <?php
                                  echo $this->Html->link('Edit',array('controller'=>'Coupons','action'=>'edit',$coupon['Coupon']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?>
								<?php
                                  echo $this->Html->link('Delete',array('controller'=>'Coupons','action'=>'delete',$coupon['Coupon']['id']),array('class'=>'delete-confirm btn btn-xs btn-default'));
                                ?>                                

                            </td>

		                </tr>            
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">View Coupons</a></li>
    <li class="">
        <?php echo $this->Html->link('Add Coupon',array('controller'=>'coupons','action'=>'add')); ?>
    </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>