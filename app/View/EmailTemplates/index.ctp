<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="view">
            <section class="panel">
                <div class="table-responsive">
                    <table class="table table-striped m-b-none" data-ride="datatables">
                        <thead>
                        <tr>

                            <th>Title</th>
                            <th>Alias</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach($email_templates as $email_template)
                        {
                            ?>

                            <tr>
                                <td><?php echo $email_template['EmailTemplate']['title']; ?></td>
                                <td><?php echo $email_template['EmailTemplate']['alias']; ?></td>
                                <td><?php
                                    echo $this->Html->link('Edit',array('controller'=>'email_templates','action'=>'edit',$email_template['EmailTemplate']['id']),array('class'=>'btn btn-xs btn-default'));
                                    ?>
                                </td>
                                <td><?php
                                    echo $this->Html->link('Delete',array('controller'=>'email_templates','action'=>'delete',$email_template['EmailTemplate']['id']),array('class'=>'btn btn-xs btn-danger btn-default delete-confirm'));
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>

                        </tbody>
                    </table>
                    <?php ?>
                </div>
            </section>
        </div>

    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#view" data-toggle="tab">Email Templates</a></li>
            <li class="">
                <?php echo $this->Html->link('Add Email Template',array('controller'=>'email_templates','action'=>'add')); ?>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>