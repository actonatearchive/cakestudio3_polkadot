<?php
$this -> start('main-content');
?>
<script>
	function getMsg(){		
		var container=document.getElementById('email-desc');
		var msg=document.getElementById('editor').innerHTML;		
		container.value=msg;
	}
</script>
    <div class="tab-content">
        <div class="tab-pane active" id="add">
            <?php echo $this -> Form -> create('User', array('controller' => 'users', 'action' => 'export_csv', 'data-validate' => 'parsley', 'role' => 'form')); ?>
		<div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Export CSV <small>(Comma seperated email address of users) </small></header>
                    <!-- <header class="panel-heading font-bold">Subject</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
								<?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Submit', 'class' => 'btn btn-s-md btn-success', 'id' => 'item-submit'));  ?>
                            </div>
						</div>
                    </div>
                </section>

            </div>
        </div>					
						
			
            
		<?php
		if(isset($users)) {
			?>
				<?php
				$exported_csv = "";
				
				foreach($users as $user) {
					$exported_csv .= $user['User']['username'].",";
				}
				$exported_csv = substr($exported_csv,0,-1);
				?>
									
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">2. Exported CSV</header>
                    <div class="panel-body">
                            <div class="form-group">
                                   <div id="editor" class="form-control" style="overflow:scroll;height:150px;max-height:150px" contenteditable="true">
										<?php
											echo $exported_csv;
										?>
									</div>
									
                                    <?php echo $this -> Form -> input('message_body', array('default'=>$exported_csv,'type'=>'textarea','div' => false, 'label' => false, 'class' => 'hide', 'id' => 'email-desc')); ?>
									
                                </div>
                            </div>
                    </div>
                </section>
            </div>
			<?php
		}
		?>		
			
			
            </form>
        </div>
    </div>
<?php echo $this->Form->end(); ?>	
	
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
			<li class="">
				<?php echo $this->Html->link('User Listing',array('controller'=>'users','action'=>'index')); ?>
			</li>
            <li class="active">
				<a href="#add" data-toggle="tab">Export CSV</a>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>
