<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
                    <th>User Info</th>
					<th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
	                <th>Mobile</th>
	                <th>Fb Token</th>
	                <th>Google Token</th>
                    <th>Email ?</th>
                    <th>News ?</th>
                    <th>SMS ?</th>
                    <th>Created</th>
                    <th>Modified</th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php foreach($users as $user) 
	            	{
	            	?>
		            
		            	<tr>
							<td><?php
                                  echo $this->Html->link('View User',array('controller'=>'users','action'=>'viewuser',$user['User']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?></td>
                            <td><?php echo $user['User']['first_name']; ?></td>
                            <td><?php echo $user['User']['last_name']; ?></td>
                            <td><?php echo $user['User']['username']; ?></td>
                            <td><?php echo $user['User']['mobile']; ?></td>
                            <td><?php echo $user['User']['fb_token']; ?></td>
                            <td><?php echo $user['User']['goog_token']; ?></td>
							<td><?php
							if ($user['User']['recv_email'] == 0) {
							echo "Off";
							} elseif ($user['User']['recv_email'] == 1) {
							echo "On";
							}
							?></td>
							<td><?php
							if ($user['User']['recv_news'] == 0) {
							echo "Off";
							} elseif ($user['User']['recv_news'] == 1) {
							echo "On";
							}
							?></td>
							<td><?php
							if ($user['User']['recv_sms'] == 0) {
							echo "Off";
							} elseif ($user['User']['recv_sms'] == 1) {
							echo "On";
							}
							?></td>
                            <td><?php echo $user['User']['created']; ?></td>
                            <td><?php echo $user['User']['modified']; ?></td>

		                </tr>            
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">User Listing</a></li>
  
	<li class="">
		<?php echo $this->Html->link('Export CSV',array('controller'=>'users','action'=>'export_csv')); ?>
	</li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>