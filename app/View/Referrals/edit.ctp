<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
        <?php echo $this -> Form -> create('Referral', array('controller' => 'referrals', 'action' => 'edit', 'data-validate' => 'parsley', 'role' => 'form')); ?>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">Courier Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Name</label>
                                <?php
                                echo $this -> Form -> input('id', array('type'=>'hidden','div' => false,'value'=>$referral['Referral']['id'], 'label' => false, 'title' => 'Courier Name', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'courier-name'));
                                
                                echo $this -> Form -> input('name', array('div' => false,'value'=>$referral['Referral']['name'], 'label' => false, 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'courier-name'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Code</label>
                                <?php
                                echo $this -> Form -> input('code', array('div' => false,'value'=>$referral['Referral']['code'], 'label' => false, 'class' => 'form-control parsley-validated', 'id' => 'courier-website'));
                                ?>
                            </div>
                            <div class="checkbox">
                                <label>
                                <?php
                                    if($referral['Referral']['is_active']==1){
                                        echo $this->Form->input('is_active',array(
                                            'type'=>'checkbox',
                                            'checked'=>'checked',
                                            'div'=>false,
                                            'label'=>' Active?'
                                        ));
                                    }
                                    else{
                                        echo $this->Form->input('is_active',array(
                                            'type'=>'checkbox',
                                            'div'=>false,
                                            'label'=>' Active?'
                                        ));
                                    }
                                ?>
                                </label>
                            </div>                          
                        </div>


                        <div class="col-sm-6">
                        </div>
                    </div>
                </section>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Courier Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'courier-submit'));  ?>
                    </div>
                </section>
            </div>

        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Referrals',array('controller'=>'referrals','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Edit Referral</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


