<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Hits</th>
                    <th>Conversions</th>
	                <th>Active ?</th>
                    <th></th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php foreach($referrals as $referral) 
	            	{
	            	?>
		            
		            	<tr>
                            <td><?php echo $referral['Referral']['name']; ?></td>
                            <td><?php echo $referral['Referral']['code']; ?></td>
                            <td><?php echo $referral['Referral']['hits']; ?></td>
                            <td><?php echo $referral['Referral']['conversions']; ?></td>
                            <td><?php echo ($referral['Referral']['is_active']==1)?"Yes":"No"; ?></td>
                            <td>
                                <?php
                                  echo $this->Html->link('Edit',array('controller'=>'referrals','action'=>'edit',$referral['Referral']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?>
								<?php
                                  echo $this->Html->link('Delete',array('controller'=>'referrals','action'=>'delete',$referral['Referral']['id']),array('class'=>'delete-confirm btn btn-xs btn-default'));
                                ?>                                

                            </td>

		                </tr>            
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">View Referrals</a></li>
    <li class="">
        <?php echo $this->Html->link('Add Referral',array('controller'=>'referrals','action'=>'add')); ?>
    </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>