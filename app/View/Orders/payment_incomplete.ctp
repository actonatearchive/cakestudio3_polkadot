<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables1">
	            <thead>
	              <tr>
					<th>Order Info</th>
					<th>Completed?</th>
                    <th>Code</th>
                    <th>User Name</th>
	                <th>Payment Status</th>
                    <th>Total Amount</th>
                    <th>Additional Charges</th>
                    <th>Payment Mode</th>
                    <th>Created</th>
                    <th>Referrer</th>
	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php 
                $grand_total = 0;
                 foreach($orders as $order) 
                    {
                        $grand_total += $order['Order']['total_amount'] + $order['Order']['additional_charges'] - $order['Order']['discount_value'];                
	            	?>
		            
		            	<tr>
							<td><?php
                                  echo $this->Html->link('View Order',array('controller'=>'orders','action'=>'vieworder',$order['Order']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?></td>
                            <td><?php
                                if($order['Order']['status'] == 0 && $order['Order']['user_id'] != null && $order['Order']['shipping_address_id'] != null && $order['Order']['billing_address_id'] != null   )
                                {
                                  echo $this->Html->link('Mark Completed',array('controller'=>'orders','action'=>'mark_complete',$order['Order']['id']),array('class'=>'btn btn-xs btn-success'));
                                } 
                                ?>
                            </td>                                
                            <td><?php echo $order['Order']['code']; ?></td>
							<td><?php
                                  echo $this->Html->link($order['Order']['User']['username'],array('controller'=>'users','action'=>'viewuser',$order['Order']['User']['id']),array('class'=>'btn btn-xs btn-info'));
                                ?></td>
                            <!--<td><?php //echo $order['Order']['User']['username']; ?></td>-->
							<td><?php
							if ($order['Order']['status'] == 0) {
							echo "Pending";
							} elseif ($order['Order']['status'] == 1) {
							echo "Completed";
							}
							?></td>
                            <td><?php echo $order['Order']['total_amount']; ?></td>
                            <td><?php echo $order['Order']['additional_charges']; ?></td>
                            
                            <td>
                            	<?php
								if ($order['Order']['payment_mode'] == 1) {
									echo "COD";
								} elseif ($order['Order']['payment_mode'] == 2) {
									echo "Debit Card";
								} elseif ($order['Order']['payment_mode'] == 3) {
									echo "Credit Card";
								} elseif ($order['Order']['payment_mode'] == 4) {
									echo "Net Banking";
								}
								 ?>
                            </td>
                            <td><?php echo $order['Order']['created']; ?></td>
				<td><?php echo $order['Order']['Referral']['name']; ?></td>
		                </tr>            
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
              <?php
                // pagination section
                echo "<div class='paging'>";
             
                    // the 'first' page button
                    echo $this->Paginator->first("First")."  ";
                     
                    // 'prev' page button, 
                    // we can check using the paginator hasPrev() method if there's a previous page
                    // save with the 'next' page button
                    if($this->Paginator->hasPrev()){
                        echo $this->Paginator->prev("Prev")."  ";
                    }
                     

                    // the 'number' page buttons
                    echo $this->Paginator->numbers(array('modulus' => 50))."  ";
                                       

                    // for the 'next' button
                    if($this->Paginator->hasNext()){
                        echo $this->Paginator->next("Next")."  ";
                    }              
                     
                    // the 'last' page button
                    echo $this->Paginator->last("Last");
                 
                echo "</div>";
               ?>      
    </div>
</div>
  
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
	<li class="">
        <?php echo $this->Html->link('Pending Dispatch Orders (Payment Complete)',array('controller'=>'orders','action'=>'index')); ?>
    </li>
        <li class="">
        <?php echo $this->Html->link('Pending Mark Delivered',array('controller'=>'orders','action'=>'pending_mark_delivered')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Today\'s Orders',array('controller'=>'orders','action'=>'todays')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Completed Orders (Payment+Dispatch)',array('controller'=>'orders','action'=>'completed')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Canceled Orders',array('controller'=>'orders','action'=>'canceled')); ?>
    </li>
	<li class="active"><a href="#view" data-toggle="tab">Incomplete Orders (Payment Incomplete)</a></li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>