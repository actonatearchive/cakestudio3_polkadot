<?php
$this -> start('main-content');
?>


<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
                    <th></th>
           
					<th>Order Info</th>

                    <th>Code</th>
                    <th>User Name</th>
                    <th>Total Amount</th>
                    <th>Additional Charges</th>
                    <th>Payment Mode</th>
                    <th>Created</th>
                    <th>Referrer</th>

	              </tr>
	            </thead>
	            	
	            	<tbody>
	            	
	            <?php
	            $grand_total = 0;
	            $todays = new DateTime();
	             foreach($orders as $order) 
	            	{
	            		$date = new DateTime($order['Order']['created']);
	
	            		if($date->format('Y-m-d') == $todays->format('Y-m-d')){
	            			$grand_total += $order['Order']['total_amount'] + $order['Order']['additional_charges'] - $order['Order']['discount_value'];
	            		}
	            			            
	            	?>
		            
		            	<tr>
                            <td><?php
                            		if($order['Order']['status']==2) {
                              			?>
                              			<a href="http://polkadot.cakestudio.in/orderdispatches/mark_delivered/<?php echo $order['Order']['id']; ?>"><button class="btn btn-xs btn-primary" onclick="if(confirm('Press OK to mark delivered')) return true; else return false;">Mark Delivered</button></a>
                              			<?php
                              		}


                                ?></td>
                               
							<td><?php
                                  echo $this->Html->link('View Order',array('controller'=>'orders','action'=>'vieworder',$order['Order']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?></td>
                            <td><?php echo $order['Order']['code']; ?></td>
							<td><?php
                                  echo $this->Html->link($order['Order']['User']['username'],array('controller'=>'users','action'=>'viewuser',$order['Order']['User']['id']),array('class'=>'btn btn-xs btn-info'));
                                ?></td>
                            <!--<td><?php //echo $order['Order']['User']['username']; ?></td>-->

                            <td><?php echo $order['Order']['total_amount']; ?></td>
                            <td><?php echo $order['Order']['additional_charges']; ?></td>
                            
                            <td>
                            	<?php
								if ($order['Order']['payment_mode'] == 1) {
									echo "COD";
								} elseif ($order['Order']['payment_mode'] == 2) {
									echo "Debit Card";
								} elseif ($order['Order']['payment_mode'] == 3) {
									echo "Credit Card";
								} elseif ($order['Order']['payment_mode'] == 4) {
									echo "Net Banking";
								}
								 ?>
                            </td>
                            <td><?php echo $order['Order']['created']; ?></td>
                            <td><?php echo $order['Order']['Referral']['name']; ?></td>                            

		                </tr>            
		            
	            	<?php
					}
	            	?>
	            	
	            	</tbody>
	          </table>
	          <?php ?>
	        </div>
      </section>
    </div>
</div>
<br />
<br />
<!-- <div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">              
			<h3>&nbsp;&nbsp;Today's Total : <strong><i class="icon-inr"></i> <?php //echo $grand_total; ?></strong></h3>
      </section>

    </div>              
</div>  -->
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="">
        <?php echo $this->Html->link('Pending Dispatch Orders (Payment Complete)',array('controller'=>'orders','action'=>'index')); ?>
    </li>
    <li class="active"><a href="#view" data-toggle="tab">Pending Mark Delivered</a></li>
    <li class="">
        <?php echo $this->Html->link('Today\'s Orders',array('controller'=>'orders','action'=>'todays')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Completed Orders (Payment+Dispatch)',array('controller'=>'orders','action'=>'completed')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Canceled Orders',array('controller'=>'orders','action'=>'canceled')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Incomplete Orders (Payment Incomplete)',array('controller'=>'orders','action'=>'payment_incomplete')); ?>
    </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>