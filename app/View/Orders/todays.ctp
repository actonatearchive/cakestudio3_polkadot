<?php
$this -> start('main-content');
?>

<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
					<th>Order Info</th>
                    <th>Code</th>
                    <th>User Name</th>
	                <th>Payment Status</th>
                    <th>Eggless?</th>
                    <th>Order Amount</th>
                    <!-- <th>Additional Charges</th> -->
                    <th>Payment Mode</th>
                    <th>Order Placed at</th>
                    <th>Review</th>
                    <!-- <th>Created</th> -->
                    <!-- <th>Referrer</th> -->
	              </tr>
	            </thead>

	            	<tbody>

	            <?php
	            $grand_total = 0;
	             foreach($orders as $order)
	            	{
	            		if($order['Order']['status'] == 1 || $order['Order']['status'] == 2 || $order['Order']['status'] == 3){
	            			$grand_total += $order['Order']['total_amount'] + $order['Order']['additional_charges'] - $order['Order']['discount_value'] + $order['Order']['eggless'];
	            		}

	            	?>

		            	<tr>
							<td><?php
                                  echo $this->Html->link('View Order',array('controller'=>'orders','action'=>'vieworder',$order['Order']['id']),array('class'=>'btn btn-xs btn-default'));
                                ?></td>
                            <td><?php echo $order['Order']['code']; ?></td>
							<td><?php
                                  echo $this->Html->link($order['User']['username'],array('controller'=>'users','action'=>'viewuser',$order['User']['id']),array('class'=>'btn btn-xs btn-info'));
                                ?></td>
                            <!--<td><?php echo $order['User']['username']; ?></td>-->
							<td><?php
							if ($order['Order']['status'] == 0) {
							echo "Pending";
							} elseif ($order['Order']['status'] == 1) {
							echo "Payment Completed";
							} elseif ($order['Order']['status'] == 2) {
							echo "Dispatched";
							} elseif ($order['Order']['status'] == 3) {
							echo "Completed";
							}
							?></td>

                            <td>
                                <?php if ($order['Order']['eggless'] > 0): ?>
                                    <b>Yes</b>
                                <?php else: ?>
                                    -
                                <?php endif; ?>
                            </td>

                            <td><?php echo $order['Order']['total_amount']+$order['Order']['additional_charges']-$order['Order']['discount_value']+$order['Order']['eggless']; ?></td>
                            <!-- <td><?php //echo $order['Order']['additional_charges']; ?></td> -->

                            <td>
                            	<?php
								if ($order['Order']['payment_mode'] == 1) {
									echo "COD";
								} elseif ($order['Order']['payment_mode'] == 'DC') {
									echo "Debit Card";
								} elseif ($order['Order']['payment_mode'] == 'CC') {
									echo "Credit Card";
								} elseif ($order['Order']['payment_mode'] == 'NB') {
									echo "Net Banking";
								}
								 ?>
                            </td>
                            <!-- <td><?php //echo $order['Order']['created']; ?></td> -->
                            <!-- <td><?php //echo $order['Order']['Referral']['name']; ?></td> -->
                            <td>
                                <?php
                                    if ($order['Order']['status'] >= 1) {
                                        echo date('d-M-Y h:m a',strtotime($order['Order']['order_placed_at'])) ;
                                    } else {
                                        echo date('d-M-Y h:m a',strtotime($order['Order']['created'])) ;
                                    }
                                ?>
                            </td>
                            <td><?= $order['Order']['review'] ?></td>

		                </tr>

	            	<?php
					}
	            	?>

	            	</tbody>
	          </table>

	        </div>


      </section>

    </div>
</div>
<br />
<br />
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
			<h3>&nbsp;&nbsp;Today's Orders Total : <strong><i class="icon-inr"></i> <?php echo $grand_total; ?></strong></h3>
      </section>

    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">

    <li class="">
        <?php echo $this -> Html -> link('Pending Dispatch Orders (Payment Complete)', array('controller' => 'orders', 'action' => 'index')); ?>
    </li>
        <li class="">
        <?php echo $this->Html->link('Pending Mark Delivered',array('controller'=>'orders','action'=>'pending_mark_delivered')); ?>
    </li>

    <li class="active"><a href="#view" data-toggle="tab">Today's Orders</a></li>
		<li class="">
        <?php echo $this->Html->link('Completed Orders (Payment+Dispatch)',array('controller'=>'orders','action'=>'completed')); ?>
    </li>
    <li class="">
        <?php echo $this->Html->link('Canceled Orders',array('controller'=>'orders','action'=>'canceled')); ?>
    </li>
	<li class="">
        <?php echo $this->Html->link('Incomplete Orders (Payment Incomplete)',array('controller'=>'orders','action'=>'payment_incomplete')); ?>
    </li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>
