<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
        <?php echo $this -> Form -> create('Order', array('controller' => 'orders', array('action' => 'cancel',$order['Order']['id']), 'data-validate' => 'parsley', 'role' => 'form')); ?>
        <div class="row">
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">Courier Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Order Code</label>
                                <?php
                                echo $this -> Form -> input('id', array('type'=>'hidden','div' => false,'value'=>$order['Order']['id'], 'label' => false, 'class' => 'form-control parsley-validated', 'data-required' => 'true'));

                                echo $this -> Form -> input('status', array('type'=>'hidden','div' => false,'value'=>-1, 'label' => false, 'class' => 'form-control parsley-validated', 'data-required' => 'true'));
                                
                                echo $this -> Form -> input('code', array('div' => false,'value'=>$order['Order']['code'], 'label' => false, 'title' => 'Order Code', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'order-code', 'readonly'=>'readonly'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Cancellation Remark</label>
                                <?php
                                echo $this -> Form -> input('admin_remarks', array('div' => false, 'label' => false, 'title' => 'Cancellation Remark', 'class' => 'form-control parsley-validated', 'id' => 'admin_remark', 'data-required' => 'true',));
                                ?>
                            </div>

                            <div class="form-group">
                                <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Courier Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'courier-submit'));  ?>
                            </div>                            
                
                    </div>
                </section>

            </div>
        </div>

        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('Pending Dispatch Orders (Payment Complete)',array('controller'=>'orders','action'=>'index')); ?>
            </li>
            <li class="">
                <?php echo $this->Html->link('Today\'s Orders',array('controller'=>'orders','action'=>'todays')); ?>
            </li>
            <li class="">
                <?php echo $this->Html->link('Completed Orders (Payment+Dispatch)',array('controller'=>'orders','action'=>'completed')); ?>
            </li>
            <li class="">
                <?php echo $this->Html->link('Incomplete Orders (Payment Incomplete)',array('controller'=>'orders','action'=>'payment_incomplete')); ?>
            </li>
            <li class="active"><a href="#view" data-toggle="tab">Cancel Order</a></li>                
        </ul>
    </header>
<?php
$this -> end('main-header');
?>
