<?php
$this->start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="view">
            <section class="panel">
                <div class="table-responsive">
                    <table class="table table-striped m-b-none" data-ride="datatable">
                        <thead>
                        <tr>
                            <th>Ref No</th>
                            <th>Item</th>
                            <th>Item Alias</th>
                            <th>Order Date</th>
                            <th>Delivery Date</th>
                            <th>Price</th>
                            <th>Discount Price</th>
                            <th>Quantity</th>
                            <th>Exhausted</th>
                            <th>Expiry Date</th>
                            <th>Supplier</th>
                            <!-- <th>Increment</th>
                            <th>Edit</th> -->
                            <th>Delete</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach ($item_stocks as $item)
                        {
                        ?>

                        <tr>
                            <td><?php echo strtoupper($item['ItemStock']['ref_no']); ?></td>
                            <td><?php echo $item['Item']['name'] . " (" . $item['Item']['variant_name'] . ") "; ?></td>
                            <td><?php echo $item['Item']['alias']; ?></td>
                            <td><?php echo $item['ItemStock']['order_date']; ?></td>
                            <td><?php echo $item['ItemStock']['delivery_date']; ?></td>
                            <td>
                                <?php
                                echo '<span href="#" class="btn btn-xs btn-default btn-rounded">' . $item['ItemStock']['price'] . '</span>';
                                ?>
                            </td>
                            <td>
                                <?php
                                echo '<span href="#" class="btn btn-xs btn-danger btn-rounded">' . $item['ItemStock']['discount_price'] . '</span>';
                                ?>
                            </td>
                            <td><?php
                                if ($item['Item']['stock_type'] != 1) {
                                    echo '<span href="#" class="btn btn-xs btn-info btn-rounded">' . $item['ItemStock']['total_quantity'] . '</span>';
                                } else {
                                    echo '<span href="#" class="btn btn-xs btn-info btn-rounded">infinite</span>';
                                }
                                ?></td>
                            <td><?php echo '<span href="#" class="btn btn-xs btn-info btn-rounded">' . $item['ItemStock']['exhausted_quantity'] . '</span>'; ?></td>
                            <td><?php echo $item['ItemStock']['expiry_date']; ?></td>
                            <td><?php echo $item['ItemStock']['supplier']; ?></td>
                            <!-- <td><?php
                                /*if ($item['Item']['stock_type'] != 1) {
                                    echo $this->Html->link('Increment', array('controller' => 'item_stocks', 'action' => 'increment',$item['ItemStock']['item_id'], $item['ItemStock']['id']), array('class' => 'btn btn-xs btn-default'));
                                }
                                ?>
                            </td>
                            <td><?php
                                if ($item['Item']['stock_type'] != 1) {
                                    echo $this->Html->link('Edit', array('controller' => 'item_stocks', 'action' => 'edit', $item['ItemStock']['id']), array('class' => 'btn btn-xs btn-default'));
                                }*/
                                ?>
                            </td> -->
                            <td><?php
                                if ($item['Item']['stock_type'] != 1) {
                                    echo $this->Html->link('Delete', array('controller' => 'item_stocks', 'action' => 'delete', $item['ItemStock']['id']), array('class' => 'btn btn-xs btn-danger btn-default delete-confirm'));
                                }
                                ?>
                            </td>

                            <?php
                            }
                            ?>

                        </tbody>
                    </table>
                    <?php ?>
                </div>
            </section>
<?php
                // pagination section
                echo "<div class='paging'>";
             
                    // the 'first' page button
                    echo $this->Paginator->first("First")."  ";
                     
                    // 'prev' page button, 
                    // we can check using the paginator hasPrev() method if there's a previous page
                    // save with the 'next' page button
                    if($this->Paginator->hasPrev()){
                        echo $this->Paginator->prev("Prev")."  ";
                    }
                     

                    // the 'number' page buttons
                    echo $this->Paginator->numbers(array('modulus' => 50))."  ";
                                       

                    // for the 'next' button
                    if($this->Paginator->hasNext()){
                        echo $this->Paginator->next("Next")."  ";
                    }              
                     
                    // the 'last' page button
                    echo $this->Paginator->last("Last");
                 
                echo "</div>";
               ?>            
        </div>
        <div class="tab-pane" id="add_stock">
            <div class="row">
                <?php echo $this -> Form -> create('ItemStock', array('controller' => 'item_stocks', 'action' => 'index', 'data-validate' => 'parsley', 'role' => 'form')); ?>
                    <div class="col-sm-6">
                    <section class="panel">
                        <header class="panel-heading font-bold">1. Choose Item</header>
                        <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php echo $this -> Form -> input('selected_item', array('options' => $item_lists, 'empty' => 'Choose Item', 'class' => 'form-control m-b parsley-validated selected-item-list', 'label' => false, 'div' => false)); ?>
                                </div>
                                <div class="form-group">
                                    <?php  echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Submit', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-stock-submit'));  ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                </form>
            </div>
        </div>
    </div>
<?php
$this->end('main-content');
?>
<?php
$this->start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#view" data-toggle="tab">View Stocks</a></li>
            <li><a href="#add_stock" data-toggle="tab">Add Stock</a></li>
        </ul>
    </header>
<?php
$this->end('main-header');
?>