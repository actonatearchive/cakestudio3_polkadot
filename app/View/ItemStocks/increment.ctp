<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="add">
            <?php echo $this -> Form -> create('ItemStock', array('controller' => 'item_stocks', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>
            <div class="row">
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Basic Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group m-b-lg">
                                <?php
                                    echo "<h3><b>".$selectedItem['Item']['name']."</b></h3>";
                                    echo "<h5>".$selectedItem['Item']['variant_name']."</h5>";
                                    echo "<h6>".$selectedItem['Item']['alias']."</h6>";
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Reference No</label>
                                <?php
                                echo $this -> Form -> input('item_id', array('default'=>$selectedItem['Item']['id'],'type'=>'hidden','div' => false, 'label' => false, 'title' => 'Ref No', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-stock-ref_no'));
                                echo $this -> Form -> input('ref_no', array('div' => false, 'label' => false, 'title' => 'Ref No', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-stock-ref_no'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Total Quantity</label>
                                <?php
                                    echo $this -> Form -> input('total_quantity', array('div' => false, 'label' => false, 'title' => 'Total Quantity', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-stock-total_quantity'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <?php
                                echo $this -> Form -> input('price', array('default'=>$selectedItemStock['ItemStock']['price'],'div' => false, 'label' => false, 'title' => 'Price', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-stock-price','readonly'=>'readonly'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Discounted Price</label>
                                <?php
                                echo $this -> Form -> input('discount_price', array('default'=>$selectedItemStock['ItemStock']['discount_price'],'div' => false, 'label' => false, 'title' => 'Discount Price', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'id' => 'item-stock-discount_price','readonly'=>'readonly'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Order Date</label>
                                <?php
                                echo $this -> Form -> input('order_date', array('type'=>'text','div' => false, 'label' => false, 'title' => 'Order Date', 'class' => 'form-control datepicker-format parsley-validated', 'id' => 'item-stock-order_date'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Delivery Date</label>
                                <?php
                                echo $this -> Form -> input('delivery_date', array('type'=>'text','div' => false, 'label' => false, 'title' => 'Delivery Date', 'class' => 'form-control datepicker-format parsley-validated','id' => 'item-stock-delivery_date'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Expiry Date</label>
                                <?php
                                echo $this -> Form -> input('expiry_date', array('type'=>'text','div' => false, 'label' => false, 'title' => 'Expiry Date', 'class' => 'form-control datepicker-format parsley-validated','id' => 'item-stock-expiry_date'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Supplier</label>
                                <?php
                                echo $this -> Form -> input('supplier', array('div' => false, 'label' => false, 'title' => 'Supplier', 'class' => 'form-control parsley-validated', 'id' => 'item-stock-supplier'));
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                <section class="panel">
                    <!-- <header class="panel-heading font-bold">Add Item Category</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php  echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Submit', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-stock-submit'));  ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            </div>
            </form>
        </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Stocks',array('controller'=>'item_stocks','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Stock</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>

<?php
    $this->start('script');
?>
<script>
    $(document).ready(function(e){
        $('.datepicker-format').datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#item-stock-submit").click(function(e){
            e.preventDefault();

            var price = parseInt($("#item-stock-price").val());
            var discount_price = parseInt($("#item-stock-discount_price").val());

            if(price<discount_price)
            {
                alert("Please keep the discount price equal or less than the price.");
            }
            else{
                $("#ItemStockAddForm").submit();
            }
        })

    })
</script>
<?php
    $this->end('script');
?>