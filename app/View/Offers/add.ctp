<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">
        <?php echo $this -> Form -> create('Offer', array('type'=>'file','controller' => 'offers', 'action' => 'add', 'data-validate' => 'parsley', 'role' => 'form')); ?>
        <div class="row">
            <div class="col-sm-6">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Offer Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Description</label>
                                <?php
                                    echo $this -> Form -> input('description', array('type'=>'textarea', 'class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'offer-description', 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Link</label>
                                <?php
                                echo $this -> Form -> input('link', array('class' => 'form-control m-b parsley-validated', 'label' => false,'id'=>'offer-link', 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Offer (200X200)</label>
                                <div class="col-sm-12">
                                    <?php echo $this -> Form -> input('offer_filename', array('type'=>'file','div' => false, 'label' => false, 'title' => 'Offer', 'id' => 'offer-offer_filename')); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                    </div>
                </section>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>

    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="">
                <?php echo $this->Html->link('View Offers',array('controller'=>'offers','action'=>'index')); ?>
            </li>
            <li class="active"><a href="#add" data-toggle="tab">Add Offer</a></li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>


<?php
  $this->start('script');
?>
<script type="text/javascript">

    $(document).ready(function(){

    });
</script>
<?php
  $this->end('script');
?>