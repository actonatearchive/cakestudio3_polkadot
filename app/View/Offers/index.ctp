<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="view">
            <section class="panel">
                <div class="view-items">

                    <?php
                        foreach($offers as $offer){
                            echo '<div class="list-item">';
                                echo $this->Html->image('/files/offer/offer_filename/'.$offer['Offer']['id']."/".$offer['Offer']['offer_filename'],array());
                                echo '<p>'.$offer['Offer']['description'].'</p>';
                                echo '<div class="list-item-link">';
                            echo $this->Html->link('Edit',array('controller'=>'offers','action'=>'edit',$offer['Offer']['id']),array('class'=>'btn btn-xs btn-default'));
                                echo $this->Html->link('Delete',array('controller'=>'offers','action'=>'delete',$offer['Offer']['id']),array('class'=>'btn delete-confirm btn-xs btn-danger delete-confirm btn-danger'));
                                echo '</div>';
                            echo '</div>';
                        }
                        if(sizeof($offers)==0)
                        {
                            echo '<p class="large-padding">No Offers added.</p>';
                        }
                    ?>
                </div>
            </section>
        </div>
    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#view" data-toggle="tab">View Offers</a></li>
            <li class="">
                <?php echo $this->Html->link('Add Offer',array('controller'=>'offers','action'=>'add')); ?>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>