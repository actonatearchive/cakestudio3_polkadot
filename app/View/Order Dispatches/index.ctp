<?php
$this -> start('main-content');
?>
<div class="tab-content">
    <div class="tab-pane active" id="view">
    	<section class="panel">
	        <div class="table-responsive">
	          <table class="table table-striped m-b-none" data-ride="datatables">
	            <thead>
	              <tr>
                    <th>Dispatch Number</th>
                    <th>Time</th>
                    <th>Courier Name</th>
	                <th>Order Code</th>
                    <th>Tracking Code</th>
                    <th>Created</th>
                    <th>Delete</th>
	              </tr>
	            </thead>
	            	
	            	<tbody>	            	
		            	<?php foreach($order_dispatches as $odispatch) 
		            	{
		            	?>
			            	<tr>
	                            <td><?php echo $odispatch['OrderDispatch']['number']; ?></td>
	                            <td><?php echo $odispatch['OrderDispatch']['time']; ?></td>
	                            <td><?php echo $odispatch['Courier']['name']; ?></td>
	                            <td><?php echo $odispatch['Order']['code']; ?></td>
	                            <td><?php echo $odispatch['OrderDispatch']['tracking_code']; ?></td>
	                            <td><?php echo $odispatch['OrderDispatch']['created']; ?></td>
                                <td><?php
                                    echo $this->Html->link('Delete',array('controller'=>'orderdispatches','action'=>'delete',$odispatch['OrderDispatch']['id']),array('class'=>'btn btn-xs btn-danger delete-confirm btn-default'));
                                ?></td>
	
			                </tr>            
		            	<?php
						}
		            	?>	    
	            	</tbody>
	          </table>
	        </div>
      </section>
    </div>
</div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#view" data-toggle="tab">View Dispatches</a></li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>