<?php
$this -> start('main-content');
?>
    <div class="tab-content">
    <div class="tab-pane active" id="add">

<?php echo $this -> Form -> create('OrderDispatch', array('url' => '/orderdispatches/add', 'data-validate' => 'parsley')); ?>
        
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">1. Order Details</header>
                    <!-- <header class="panel-heading font-bold">Add Item</header> -->
                    <div class="panel-body">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Order Code</label>
                                <input type="hidden" name="data[OrderDispatch][order_id]" value="<?php echo $order['Order']['id']; ?>" /> 
                                <?php
                                //echo $this -> Form -> input('order_id', array('value'=> $order['Order']['id'],'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'label' => false, 'div' => false));
								
                                echo $this -> Form -> input('temp', array('div' => false,'value'=> $order['Order']['code'],'disabled'=>'disabled', 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <?php
                                	echo $this -> Form -> input('temp', array('value'=> $order['User']['username'],'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'data-required' => 'true', 'label' => false, 'div' => false));
                                ?>
                            </div>
							<?php
							if ($order['Order']['status'] == 0) {
								$os = "Pending";
							} elseif ($order['Order']['status'] == 1) {
								$os = "Completed";
							}
							?>                            
                            <div class="form-group">
                                <label>Payment Status</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $os,'disabled'=>'disabled', 'class' => 'form-control parsley-validated', 'label' => false, 'div' => false));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Discount Coupon</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $coupon['Coupon']['code'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Order Created</label>
                                <?php
                                $display_created = new DateTime($order['Order']['created']);
                                $display_c = $display_created->format("D, F j, Y, g:i a");
                                echo $this -> Form -> input('temp', array('value'=> $display_c,'disabled'=>'disabled','div' => false, 'label' => false,  'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
                        </div>


                        <div class="col-sm-6">

							<div class="form-group">
                                <label>Total Amount</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $order['Order']['total_amount'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
						
                            <div class="form-group">
                                <label>Shipping Charges</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $order['Order']['additional_charges'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
							
							<div class="form-group">
                                <label>Discount Value</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=> $order['Order']['discount_value'],'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>
							
                            	<?php
								if ($order['Order']['payment_mode'] == 1) {
									$pm = "COD";
								} elseif ($order['Order']['payment_mode'] == 2) {
									$pm = "Debit Card";
								} elseif ($order['Order']['payment_mode'] == 3) {
									$pm = "Credit Card";
								} elseif ($order['Order']['payment_mode'] == 4) {
									$pm = "Net Banking";
								}
								 ?>                            
                            <div class="form-group">
                                <label>Payment Mode</label>
                                <?php
                                echo $this -> Form -> input('temp', array('value'=>$pm,'disabled'=>'disabled','div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
                                ?>
                            </div>

                            <div class="form-group">
                                <label>Order Modified</label>
                                <?php
                                $display_modified = new DateTime($order['Order']['modified']);
                                $display_m = $display_modified->format("D, F j, Y, g:i a");                                
                                echo $this -> Form -> input('temp', array('value'=> $display_m,'disabled'=>'disabled','div' => false, 'label' => false,  'class' => 'form-control parsley-validated'));
                                ?>
                            </div>                            
                        </div>
                    </div>
                </section>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">2. Addresses</header>
                    <div class="panel-body">
						<div class="col-sm-6">
							<h3>Shipping Address :</h3>
							<?php
							echo "<b>".$order['ShippingAddress']['name']."</b><br />";
							echo $order['ShippingAddress']['line1']."<br />";
							echo $order['ShippingAddress']['line2']."<br />";
							echo $order['ShippingAddress']['landmark']."<br />";
							echo $shipping['City']['name']." - ".$order['ShippingAddress']['pin_code']."<br />";
							echo $shipping['State']['name']."<br />";
							echo "Phone : ".$order['ShippingAddress']['mobile']."<br />";
							?>
						</div>
						<div class="col-sm-6">
							<h3>Billing Address :</h3>
							<?php
							echo "<b>".$order['BillingAddress']['name']."</b><br />";
							echo $order['BillingAddress']['line1']."<br />";
							echo $order['BillingAddress']['line2']."<br />";
							echo $order['BillingAddress']['landmark']."<br />";
							echo $order['BillingAddress']['name']." - ".$order['BillingAddress']['pin_code']."<br />";
							echo "Phone : ".$order['BillingAddress']['mobile']."<br />";							
							?>							
						</div>
                    </div>
                </section>
            </div>

        </div>



        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">3. Select Order Items For Dispatch</header>
                    <div class="panel-body">
						<div class="col-sm-6">
							<?php
							foreach($order_items as $key=>$value){
								echo $this->Html->link('View Item',array('controller'=>'items','action'=>'viewitem',$value['item_id']),array('class'=>'btn btn-xs btn-danger','target'=>'_blank'));
								?>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<?php
								echo '<input type="checkbox" name="data[OrderDispatch][order_item_ids][OrderItem]" value="'.$key.'"></input>';
								
								
								//echo $this->Form->checkbox('order_item_ids', array('value' => $key));
								echo '<span class=""><h3 style="color:#c0493b;display:inline"><b> '.$value['name'].'</b></h3></span><br />';

                                if($value['item_params'] != ""){
                                    echo '<h4><b>Delivery Instructions</b></h4>';
                                    echo '<p><b>Message on Cake - </b>'.$value['item_params']['delivery_message'];
                                    echo '<p><b>Delivery Date - </b>'.$value['item_params']['delivery_date'];
                                    echo '</p>';
                                }

							}
							?>
						</div>
                    </div>
                </section>
            </div>

        </div>
		
		<!--<div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">4. Gift Details</header>
                    <div class="panel-body">
						<div class="col-sm-6">
							<?php
								//$display_ddate = new DateTime($order['Order']['delivery_date']);
                                //$display_deldate = $display_created->format("D, F j, Y");
								//echo "<b>Delivery Date : </b>".$display_deldate."<br />";
								//echo "<b>Gift To : </b>".$order['Order']['gift_to']."<br />";
								//echo "<b>Gift Message : </b>".$order['Order']['gift_message']."<br />";
							?>
						</div>
                    </div>
                </section>
            </div>

        </div>--> 		
        
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading font-bold">4. Dispatch Details</header>
                    <div class="panel-body">
                    	<div class="col-sm-6">
                    		
                    		<div class="form-group">
                				<label>Dispatch Number</label>
								<?php
									echo $this->Form->input('number',array('div' => false, 'label' => false, 'class' => 'form-control parsley-validated', 'data-required' => 'true'));
								?>
							</div>
							
                    		<div class="form-group">
                				<label>Dispatch Courier Name</label>
								<?php
									echo $this->Form->input('courier_id',array('options' => $couriers,'empty'=>'Self-Delivery', 'div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
								?>
							</div>							
							
                    	</div>
                    	<div class="col-sm-6">
                    		
                    		<div class="form-group">
                				<label>Tracking Code</label>
								<?php
									echo $this->Form->input('tracking_code',array('div' => false, 'label' => false, 'class' => 'form-control parsley-validated'));
								?>
							</div>														
							
							<div class="form-group">
                				<label>Time</label><br />
								<?php
									$todays = new DateTime();
									$the_datetime = $todays->format('d-m-Y H:i:s');
									echo $this->Form->input('time',array('div' => false, 'label' => false, 'class' => 'combodate', 'data-required' => 'true', 'data-format'=>'DD-MM-YYYY HH:mm:ss','data-template'=>'DD  MM  YYYY  -  HH : mm : ss','value'=> $the_datetime));
								?>
							</div>                    		
                    		
                    	</div>
                    </div>
                </section>
            </div>

        </div>        
        
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                        <?php   echo $this -> Form -> input('Proceed', array('type' => 'submit', 'div' => false, 'label' => false, 'title' => 'Item Name', 'class' => 'btn btn-s-md btn-success', 'data-required' => 'true', 'id' => 'item-submit'));  ?>
                    </div>
                </section>
            </div>

        </div>
        <?php echo $this->Form->end(); ?>        
    </div>
    </div>        
        






<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
<header class="header bg-primary bg-gradient">
  <ul class="nav nav-tabs">
    <li class="">
        <?php echo $this->Html->link('View Dispatches',array('controller'=>'orderdispatches','action'=>'index')); ?>
    </li>
    <li class="active"><a href="#view" data-toggle="tab">Create Dispatch</a></li>
  </ul>
</header>
<?php
$this -> end('main-header');
?>