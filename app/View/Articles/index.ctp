<?php
$this -> start('main-content');
?>
    <div class="tab-content">
        <div class="tab-pane active" id="view">
            <section class="panel">
                <div class="table-responsive">
                    <table class="table table-striped m-b-none" data-ride="datatables">
                        <thead>
                        <tr>

                            <th>Title</th>
                            <th>Alias</th>
                            <th>Keywords</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach($articles as $article)
                        {
                            ?>

                            <tr>
                                <td><?php echo $article['Article']['title']; ?></td>
                                <td><?php echo $article['Article']['alias']; ?></td>
                                <td><?php echo $article['Article']['keywords']; ?></td>
                                <td><?php
                                    echo $this->Html->link('Edit',array('controller'=>'articles','action'=>'edit',$article['Article']['id']),array('class'=>'btn btn-xs btn-default'));
                                    ?>
                                </td>
                                <td><?php
                                    echo $this->Html->link('Delete',array('controller'=>'articles','action'=>'delete',$article['Article']['id']),array('class'=>'btn btn-xs btn-danger btn-default delete-confirm'));
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>

                        </tbody>
                    </table>
                    <?php ?>
                </div>
            </section>
        </div>

    </div>
<?php
$this -> end('main-content');
?>
<?php
$this -> start('main-header');
?>
    <header class="header bg-primary bg-gradient">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#view" data-toggle="tab">Article Index</a></li>
            <li class="">
                <?php echo $this->Html->link('Add Article',array('controller'=>'articles','action'=>'add')); ?>
            </li>
        </ul>
    </header>
<?php
$this -> end('main-header');
?>