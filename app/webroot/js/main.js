$(document).ready(function(){
	$('.colorpicker').colorpicker();

    $(".delete-confirm").click(function(e){
        if(!confirm("Are you sure you want to delete this item?")){
            return false;
        }
    })
});

function AxinusUI(){

}
AxinusUI.prototype.showLoader = function(){
    $(".main-loader").fadeIn(500);
}

AxinusUI.prototype.hideLoader = function(){
    $(".main-loader").fadeOut(500);
}

var axinusUI = new AxinusUI();

