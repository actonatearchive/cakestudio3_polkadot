<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class ReferralsController extends AppController {

	//last edit - Ishan
	public $name = 'Referrals';

	//last edit - Ishan
	public $uses = array('Referral');

	//last edit - Ishan
	public function index() {
		$referrals = $this -> Referral -> find('all');
		$this -> set('referrals', $referrals);

		$this -> set('page_title', 'View Referrals');
		$this -> layout = 'polka_shell';
	}

	//last edit - Ishan
	public function add() {
		if ($this -> request -> is('post')) {
			$referral = $this -> request -> data;

			if ($this -> Referral -> save($referral)) {

				//This flash message has to be set in the view properly
				$this -> Session -> setFlash('New courier added.', 'default', array('class' => 'alert alert-success'), 'success');
				$this -> redirect(array('controller' => 'referrals', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'referrals', 'action' => 'index'));
			}
		} else {

			//display the page for adding couriers
			$this -> set('page_title', 'Add Referral');
			$this -> layout = 'polka_shell';

		}

	}

	//last edit - Ishan
	public function edit($id) {

		if ($this -> request -> is('post')) {
			
			$referral = $this -> request -> data;

			if ($this -> Referral -> save($referral)) {

				//This flash message has to be set in the view properly
				$this -> Session -> setFlash('Referral saved.', 'default', array('class' => 'alert alert-success'), 'success');
				$this -> redirect(array('controller' => 'referrals', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'referrals', 'action' => 'index'));
			}
		} else {
			
			if ($id == null) {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. Data not found.");
				$this -> redirect(array('controller' => 'referrals', 'action' => 'index'));
			} else {
				//fetch and display the courier details

				$referral = $this -> Referral -> findById($id);
				$this -> set('referral', $referral);

				//print_r($referral);die();

				//display the page for adding couriers
				$this -> set('page_title', 'Edit Referral');
				$this -> layout = 'polka_shell';
			}
		}
	}

	//last edit - Ishan
	public function delete($id) {
			
			//$referral = $this -> Referral -> findById($id);

			if ($this -> Referral -> delete($id)) {

				//This flash message has to be set in the view properly
				$this -> Session -> setFlash('Referral deleted.', 'default', array('class' => 'alert alert-success'), 'success');
				$this -> redirect(array('controller' => 'referrals', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'referrals', 'action' => 'index'));
			}

	}

}
