<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class OffersController extends AppController {

    public $name = 'Offers';

    public $uses = array('Offer', 'State');

    public function index() {
        $offers = $this -> Offer -> find('all',array('order'=>array('Offer.created')));
        $this -> set('offers', $offers);

        $this -> set('page_title', 'Offers');
        $this -> layout = 'polka_shell';
    }

    public function add() {

        if ($this -> request -> is('post')) {
            $offer = $this -> request -> data;


            if ($this -> Offer -> save($offer)) {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('New offer added.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
            }
        }
        else{

        }
        $this -> set('page_title', 'Add Offer');
        $this -> layout = 'polka_shell';
    }


    public function edit($id=null) {

        if ($this -> request -> is('post')) {
            $offer = $this -> request -> data;

            if ($this -> Offer -> save($offer)) {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Selected offer edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
            }
        }

        else{

            if($id == null){
                $this->Session->setFlash('Please choose a offer.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
            }

            $selectedOffer = $this->Offer->findById($id);

            if($selectedOffer == null){
                $this->Session->setFlash('Please choose a offer.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
            }

            $this->set('offer',$selectedOffer);
            $this -> set('page_title', 'Edit Offer');
            $this -> layout = 'polka_shell';

        }
    }


    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose a offer.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
        }

        $selectedOffer = $this->Offer->findById($id);

        if($selectedOffer == null){
            $this->Session->setFlash('Please choose a offer.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
        }

        if($this->Offer->delete($selectedOffer['Offer']['id'])){
            $this->Session->setFlash('Offer deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'offers', 'action' => 'index'));
        }

        $this -> set('page_title', 'Edit Offer');
        $this -> layout = 'polka_shell';
    }


}
