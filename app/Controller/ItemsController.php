<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class ItemsController extends AppController {

	public $name = 'Items';

	public $uses = array('Item', 'ItemCategory','ItemStock','City','ItemCity','ItemGalleryPhoto','ItemOfMonth');

	public function index() {

        //Get All Cities
        $cities = $this->City->find('list',array('order'=>array('City.name')));
        $this->set('cities',$cities);        

        // we prepare our query, the cakephp way!
        $this->paginate = array(
            'limit' => 100,
            'order' => array('name' => 'asc')
        );
     
        // we are using the 'User' model
        $items = $this->paginate('Item');

        //pr($items);

		//$items = $this -> Item -> find('all');
		$this -> set('items', $items);

		$this -> set('page_title', 'View Items');
		$this -> layout = 'polka_shell1';
	}

    public function city() {

        //Get All Cities
        $cities = $this->City->find('list',array('order'=>array('City.name')));
        $this->set('cities',$cities);

        //pr($cities);        

        $this -> set('page_title', 'View Items');
        $this -> layout = 'polka_shell1';
    }    
	
	public function add_gallery() {
		$items = $this -> Item -> find('all');
		$this -> set('items', $items);

		$this -> set('page_title', 'Add Gallery');
		$this -> layout = 'polka_shell';
	}



    public function edit($id=null) {
        if ($this -> request -> is('post')) {
            $item = $this->request->data;

            if($item['Item']['item_id']==""){
                $item['Item']['item_id'] = null;
            }
            if($item['Item']['item_category_id']==""){
                $item['Item']['item_category_id'] = null;
            }

            if($item['Item']['shipping_charges']==""){
                $item['Item']['shipping_charges'] = -1;
            }

            $item['Item']['long_desc'] = htmlentities($item['Item']['long_desc']);
            $discount_percent=0;

            if($item['Item']['stock_type']==1){
                if($item['Item']['price']!=""){
                    if($item['Item']['discount_price']==""){
                        $item['Item']['discount_price']=$item['Item']['price'];
                    }
                    if($item['Item']['discount_price']>$item['Item']['price']){
                        $this->Session->setFlash('Discounted price is greater than price.', 'default', array('class' => 'alert alert-danger') , 'error');
                        $this -> redirect(array('controller' => 'items', 'action' => 'add'));
                    }

                    //Calculate percentage
                    $discount_percent = round(($item['Item']['price'] - $item['Item']['discount_price'])/$item['Item']['price'],2)*100;
                }
                else{
                    $this->Session->setFlash('You must set a price.', 'default', array('class' => 'alert alert-danger') , 'error');
                    $this -> redirect(array('controller' => 'items', 'action' => 'add'));
                }
            }

            if ($this->Item ->save($item)) {

                $last_id = $item['Item']['id'];

                if($item['Item']['stock_type']==1){

                    $newStock = $this->ItemStock->findByItemId($last_id);

                    if($newStock == null)
                    {
                        $newStock = array();
                        $newStock['ItemStock']['ref_no'] = "IN000".uniqid();
                        $newStock['ItemStock']['item_id'] = $last_id;
                        $newStock['ItemStock']['total_quantity'] = -1;
                        $newStock['ItemStock']['price'] = $item['Item']['price'];
                        $newStock['ItemStock']['discount_price'] = $item['Item']['discount_price'];

                        $newStock['ItemStock']['discount_percentage'] = $discount_percent;
                    }
                    else{
                        $newStock['ItemStock']['price'] = $item['Item']['price'];
                        $newStock['ItemStock']['discount_price'] = $item['Item']['discount_price'];

                        $newStock['ItemStock']['discount_percentage'] = $discount_percent;
                    }
                    $this->ItemStock->save($newStock);
                }

                //Delete Cities
                $this->ItemCity->deleteAll(array('ItemCity.item_id'=>$last_id));

                //Set Cities
                $itemCities = array();
                foreach($item['Item']['city_id'] as $key=>$value){
                    $newItemCity = array();
                    $newItemCity['ItemCity']['city_id'] = $value;
                    $newItemCity['ItemCity']['item_id'] = $last_id;

                    array_push($itemCities, $newItemCity);
                }

                if(sizeof($itemCities)>0)
                {
                    $this->ItemCity->saveAll($itemCities);
                }

                //This flash message has to be set in the view properly
                $this->Session->setFlash('Item edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'items', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'index'));
            }
        } else {

            if($id == null){
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'index'));
            }

            $selectedItem = $this->Item->findById($id);
            //pr($selectedItem);die();

            if($selectedItem == null){
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'index'));
            }

            if($selectedItem['Item']['item_id'] != null){
                $selectedItemParent = $this->Item->findById($selectedItem['Item']['item_id']);
                $selectedItem['Item']['item_parents'] = $selectedItemParent['Item']['name'];
            }

            $this->set('selectedItem',$selectedItem);

            //Get Item Category Cities       1
            $itemCities = $this->ItemCity->find('all',array('conditions'=>array('ItemCity.item_id'=>$selectedItem['Item']['id'])));

            $itemCitiesList = array();
            foreach($itemCities as $itemCity){
                array_push($itemCitiesList, $itemCity['ItemCity']['city_id']);
            }

            $this->set('selectedCitiesList',$itemCitiesList);
            //pr($itemCitiesList);die();

            $itemCats = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC'));
            $this -> set('item_categories', $itemCats);

            //$itemParents = $this -> Item -> find('all', array('order' => 'Item.name ASC','conditions'=>array('Item.item_id'=>null)));
            $itemParentsList = array();
/*            foreach($itemParents as $item){
                $itemParentsList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }*/
            $this -> set('item_parents', $itemParentsList);

/*            $itemAutofill = $this -> Item -> find('all', array('order' => 'Item.name ASC'));
            $itemAutofillList = array();
            foreach($itemAutofill as $item){
                $itemAutofillList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }
            $this -> set('item_autofill', $itemAutofillList);*/


            $stockType = array(0=>'Limited', 1=>'Infinite');
            $this -> set('stock_type', $stockType);



            //Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);

            $this -> set('page_title', 'Edit Item');
            $this -> layout = 'polka_shell';

        }

        //$this->Session->setFlash('Sorry, an error occured.', 'default', array('class' => 'alert alert-success') , 'success');
    }
	
	
    public function viewitem($id=null) {
        if ($this -> request -> is('post')) {

        } else {

            if($id == null){
                $this->Session->setFlash('Invalid Item.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'index'));
            }

            $selectedItem = $this->Item->findById($id);
            //pr($selectedItem);die();

            if($selectedItem == null){
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'index'));
            }

            $this->set('selectedItem',$selectedItem);

            //Get Item Category Cities       1
            $itemCities = $this->ItemCity->find('all',array('conditions'=>array('ItemCity.item_id'=>$selectedItem['Item']['id'])));

            $itemCitiesList = array();
            foreach($itemCities as $itemCity){
                array_push($itemCitiesList, $itemCity['ItemCity']['city_id']);
            }

            $this->set('selectedCitiesList',$itemCitiesList);

            $itemCats = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC'));
            $this -> set('item_categories', $itemCats);

            /*$itemParents = $this -> Item -> find('all', array('order' => 'Item.name ASC','conditions'=>array('Item.item_id'=>null)));
            $itemParentsList = array();
            foreach($itemParents as $item){
                $itemParentsList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }
            $this -> set('item_parents', $itemParentsList);

            $itemAutofill = $this -> Item -> find('all', array('order' => 'Item.name ASC'));
            $itemAutofillList = array();
            foreach($itemAutofill as $item){
                $itemAutofillList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }
            $this -> set('item_autofill', $itemAutofillList);*/


            $stockType = array(0=>'Limited', 1=>'Infinite');
            $this -> set('stock_type', $stockType);

            $itemGalleryPhotos = $this -> ItemGalleryPhoto -> find('all', array('conditions' => array('ItemGalleryPhoto.item_id' => $id)));
            $this -> set('itemGalleryPhotos', $itemGalleryPhotos);

            //Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);

            $this -> set('page_title', 'View Item');
            $this -> layout = 'polka_shell';

        }

        //$this->Session->setFlash('Sorry, an error occured.', 'default', array('class' => 'alert alert-success') , 'success');
    }	

    //Item of the Month
    public function item_of_month(){
        //get all itemMonth data from the db
        $itemMonthData = $this->ItemOfMonth->find('all');

        if (!$this -> request -> is('post')) {
            //get all cities
            $this->City->Behaviors->load('Containable');
            $cities = $this->City->find('all',array('contain'=>array(),'order'=>array('City.name')));
            
            $updatedCities = array();

            //loop through the cities and set the value
            foreach($cities as $city){
                $updatedCity = $city;
                //check if the itemMonthData exists in the array 
                foreach($itemMonthData as $itemMonth){
                    if($itemMonth['City']['id'] == $city['City']['id']){
                        $updatedCity['Item'] = $itemMonth['Item'];
                    }
                }
                array_push($updatedCities, $updatedCity);
            }            

            //Set variables
            $this->set('cities',$updatedCities);

            $autofill = '';
            $this -> set('autofill', $autofill);
            $this -> set('page_title', 'Set Item of the Month');

            $this -> layout = 'polka_shell';
        }
        else{
            $itemsMonth = $this->request->data['Item'];
            unset($itemsMonth['autofill']);
        
            foreach ($itemsMonth as $city => $item) {
                if($item != ""){
                    //check for the city item of month
                    $exists = false;

                    for($i=0;$i<sizeof($itemMonthData);$i++){
                        if($itemMonthData[$i]['City']['id'] == $city){
                            $exists = true;
                            break;
                        }
                    }

                    //if it doesnt exist insert
                    if($exists){
                        $existItemMonth = $this->ItemOfMonth->find('first',array('conditions'=>array('ItemOfMonth.city_id'=>$city)));                    
                        $existItemMonth['ItemOfMonth']['item_id'] = $item;
                        $this->ItemOfMonth->save($existItemMonth);
                    }
                    else{        
                        $this->ItemOfMonth->create();
                        $newItemMonth = array();
                        $newItemMonth['ItemOfMonth'] = array('item_id'=>$item,'city_id'=> $city); 
                        
                        $this->ItemOfMonth->save($newItemMonth);
                    }
                }
            }
            
            $this->Session->setFlash('Changes saved successfully.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'items', 'action' => 'item_of_month'));
        }
    }

	public function add() {
		if ($this -> request -> is('post')) {
			$item = $this->request->data;

            $item['Item']['sku_code'] = strtoupper("SKU".uniqid());

            if($item['Item']['item_id']==""){
                $item['Item']['item_id'] = null;
            }
            if($item['Item']['item_category_id']==""){
                $item['Item']['item_category_id'] = null;
            }
            if($item['Item']['shipping_charges']==""){
                $item['Item']['shipping_charges'] = -1;
            }

            $item['Item']['long_desc'] = htmlentities($item['Item']['long_desc']);
            $discount_percent=0;

            if($item['Item']['stock_type']==1){
                if($item['Item']['price']!=""){
                    if($item['Item']['discount_price']==""){
                        $item['Item']['discount_price']=$item['Item']['price'];
                    }
                    if($item['Item']['discount_price']>$item['Item']['price']){
                        $this->Session->setFlash('Discounted price is greater than price.', 'default', array('class' => 'alert alert-danger') , 'error');
                        $this -> redirect(array('controller' => 'items', 'action' => 'add'));
                    }

                    //Calculate percentage
                    $discount_percent = round(($item['Item']['price'] - $item['Item']['discount_price'])/$item['Item']['price'],2)*100;
                }
                else{
                    $this->Session->setFlash('You must set a price.', 'default', array('class' => 'alert alert-danger') , 'error');
                    $this -> redirect(array('controller' => 'items', 'action' => 'add'));
                }
            }

			if ($this->Item ->save($item)) {

                $last_id = $this->Item->getInsertID();

                if($item['Item']['stock_type']==1){

                    $newStock = array();
                    $newStock['ItemStock']['ref_no'] = "IN000".uniqid();
                    $newStock['ItemStock']['item_id'] = $last_id;
                    $newStock['ItemStock']['total_quantity'] = -1;
                    $newStock['ItemStock']['price'] = $item['Item']['price'];
                    $newStock['ItemStock']['discount_price'] = $item['Item']['discount_price'];

                    $newStock['ItemStock']['discount_percentage'] = $discount_percent;

                    $this->ItemStock->save($newStock);
                }

                //Set Cities
                $itemCities = array();
                foreach($item['Item']['city_id'] as $key=>$value){
                    $newItemCity = array();
                    $newItemCity['ItemCity']['city_id'] = $value;
                    $newItemCity['ItemCity']['item_id'] = $last_id;

                    array_push($itemCities, $newItemCity);
                }

                if(sizeof($itemCities)>0)
                {
                   $this->ItemCity->saveAll($itemCities);
                }

                //This flash message has to be set in the view properly
                $this->Session->setFlash('New item added.', 'default', array('class' => 'alert alert-success') , 'success');
                
                if($item['Item']['stock_type']==0){
                    $this -> redirect(array('controller' => 'item_stocks', 'action' => 'add',$last_id));
                } else {
                    $this -> redirect(array('controller' => 'item_gallery_photos', 'action' => 'manage_gallery',$last_id));
                }
				
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'items', 'action' => 'add'));
			}
		} else {


            $itemCats = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC'));
            $this -> set('item_categories', $itemCats);

            // $itemParents = $this -> Item -> find('all', array('order' => 'Item.name ASC','conditions'=>array('Item.item_id'=>null)));
            $itemParentsList = array();
            /*foreach($itemParents as $item){
                $itemParentsList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }*/
            $this -> set('item_parents', $itemParentsList);

            /*$itemAutofill = $this -> Item -> find('all', array('order' => 'Item.name ASC'));
            $itemAutofillList = array();
            foreach($itemAutofill as $item){
                $itemAutofillList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }
            */
            $autofill = '';
            $this -> set('autofill', $autofill);


            $stockType = array(0=>'Limited', 1=>'Infinite');
            $this -> set('stock_type', $stockType);


            //Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);

            $this -> set('page_title', 'Add Items');
            $this -> layout = 'polka_shell';

		}

        //$this->Session->setFlash('Sorry, an error occured.', 'default', array('class' => 'alert alert-success') , 'success');
	}

    public function json($id=null){
        if($id == null){
            $this->set('message','error');
        }

        $selectedItem = $this->Item->findById($id);

        if($selectedItem == null){
            $this->set('message','error');

        }
        else{

            if($selectedItem['Item']['item_id'] != null){
                $parent = $this->Item->findById($selectedItem['Item']['item_id']);
                $selectedItem['Item']['item_parents'] = $parent['Item']['name'];
            }
            

            $selectedItem['Item']['long_desc'] = html_entity_decode($selectedItem['Item']['long_desc']);
            
            $this->set('message',json_encode($selectedItem));
        }


        $this->layout = 'ajax_layout';
    }

    public function set_featured($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose an item.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }

        $selectedItem = $this->Item->findById($id);

        if($selectedItem == null){
            $this->Session->setFlash('Please choose an item.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }

        $selectedItem['Item']['featured']=1;

        if($this->Item->save($selectedItem))
        {
            $this->Session->setFlash('Item is set as featured.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect($this->referer());
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect($this->referer());
        }
    }
    public function unset_featured($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose an item.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }

        $selectedItem = $this->Item->findById($id);

        if($selectedItem == null){
            $this->Session->setFlash('Please choose an item.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }

        $selectedItem['Item']['featured']=0;

        if($this->Item->save($selectedItem))
        {
            $this->Session->setFlash('Item is unset as featured.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect($this->referer());
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect($this->referer());
        }
    }

    public function delete($id=null,$city_id=null,$search=null) {

        if($id == null){
            $this->Session->setFlash('Please choose an item.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }

        $selectedItem = $this->Item->findById($id);

        if($selectedItem == null){
            $this->Session->setFlash('Please choose an item.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }

        $this->ItemStock->deleteAll(array('ItemStock.item_id'=>$selectedItem['Item']['id']));
        if($this->Item->delete($selectedItem['Item']['id'])){
            $this->Session->setFlash('Item & Stock deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect($this->referer());
            // if($this->referer() != Router::url(array('controller'=>'items','action'=>'delete')))
            // {
            //     $this->redirect(array('controller'=>'items','action'=>'search',$city_id,$search));
            // }

            // $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }
    }


    public function search($city_id=null,$search=null) {
        //if ($this -> request -> is('post')) {

            $item = $this->request->data;

            $city_id = $_GET['city_id'];
            $search = $_GET['search_term'];

            $this->set('search_city_id',$city_id);
            $this->set('search_keywords',$search);

            if(empty($city_id) || $city_id == "" || $city_id == null) {

                if(empty($search) || $search == "" || $search == null) {
                    $this->Session->setFlash('No search item defined.', 'default', array('class' => 'alert alert-danger') , 'error');
                    $this -> redirect(array('controller' => 'items', 'action' => 'index'));
                } else {
                    $finalSearchItems = $this->Item->find('all',array(
                    'conditions'=>array('Item.name LIKE'=>'%'.$search.'%'),
                    'order'=>array('Item.name')
                    ));
                }

                

            } else {

                $this -> set('selected_city',  $city_id);

                if(empty($search) || $search == "" || $search == null) {


                    if($city_id == null){
                        $this->Session->setFlash('Please choose a city.', 'default', array('class' => 'alert alert-danger') , 'error');
                        $this -> redirect(array('controller' => 'items', 'action' => 'index'));
                    }

                    $selectedCity = $this->City->findById($city_id);

                    if($selectedCity == null){
                        $this->Session->setFlash('Please choose a city.', 'default', array('class' => 'alert alert-danger') , 'error');
                        $this -> redirect(array('controller' => 'items', 'action' => 'index'));
                    } 

                    $this -> set('selected_city', $selectedCity['City']['id']);   

                    /*$this->Item->Behaviors->load('Containable');

                    $searchItems = $this->Item->find('all',array(
                        'order'=>array('Item.name'),
                        
                        ));

                    

                    $finalSearchItems = array();
                    foreach ($searchItems as $searchItem) {

                        foreach ($searchItem['ItemCity'] as $key => $value) {

                            if($value['city_id'] == $city_id){
                                array_push($finalSearchItems, $searchItem);
                            }
                            
                        }   

                    }*/

                            // Ishan's fix starts here
                            $this->Item->Behaviors->load('Containable');

                            // we prepare our query, the cakephp way!
                            $this->paginate = array(
                                //'conditions' => array('Item.ItemCity.city_id'=>$id),
                                'contain' => array('ItemCategory','ItemStock','ItemCity'),
                                //'conditions' => array('ItemCity.city_id'=>$id),
                                'limit' => 250,
                                'order' => array(
                                    'Item.name' => 'asc',
                                    'Item.item_category_id' => 'asc'
                                )
                            );
                         
                            // we are using the 'User' model
                            $searchItems = $this->paginate('Item');

                            $finalSearchItems = array();
                            foreach ($searchItems as $searchItem) {

                                foreach ($searchItem['ItemCity'] as $key => $value) {

                                    if($value['city_id'] == $city_id){
                                        array_push($finalSearchItems, $searchItem);
                                    }
                                    
                                }   

                            }        

                            $this -> set('items', $finalSearchItems);   

                } else {
                    $searchItems = $this->Item->find('all',array(
                    'conditions'=>array('Item.name LIKE'=>'%'.$search.'%'),
                    'order'=>array('Item.name')
                    ));

                    $finalSearchItems = array();
                    foreach ($searchItems as $searchItem) {

                        foreach ($searchItem['ItemCity'] as $key => $value) {

                            if($value['city_id'] == $city_id){
                                array_push($finalSearchItems, $searchItem);
                            }
                            
                        }   

                    }
                }

                

            }

            $this -> set('items', $finalSearchItems);

            //Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);              

            $this -> set('page_title', 'Search Items');
            $this -> layout = 'polka_shell';            

        /*} else {
            $this->Session->setFlash('Invalid page access.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }*/

    }


    public function find() {
       $this->Item->recursive = -1;

       if ($this->request->is('ajax')) {

          $this->autoRender = false;
          $this->layout = 'ajax';

          $results = $this->Item->find('all',
                array('fields' => array('Item.id','Item.name','Item.alias','Item.variant_name'),
                    'conditions' => array('OR' => array(array('Item.name LIKE' => '%'.$this->request->query['term'].'%'), array('Item.alias LIKE' => '%'.$this->request->query['term'].'%')))
                    ));

          $final_results = array();
          foreach ($results as $result) {
              $temp = array();  
              $temp['name'] = $result['Item']['name']." ( ".$result['Item']['variant_name']." ) ".$result['Item']['alias'];
              $temp['id'] = $result['Item']['id'];
              array_push($final_results, $temp);
          }

          //$autofill = Set::extract('../Item/name', $results);           
          echo json_encode($final_results);

       }
    }

    public function find_parent() {
       $this->Item->recursive = -1;

       if ($this->request->is('ajax')) {

          $this->autoRender = false;
          $this->layout = 'ajax';

          $results = $this->Item->find('all',
                array('fields' => array('Item.id','Item.name','Item.alias','Item.variant_name'),
                    'conditions' => array('OR' => array(array('Item.name LIKE' => '%'.$this->request->query['term'].'%'), array('Item.alias LIKE' => '%'.$this->request->query['term'].'%')),'item_id'=> null)
                    ));

          $final_results = array();
          foreach ($results as $result) {
              $temp = array();  
              $temp['name'] = $result['Item']['name']." ( ".$result['Item']['variant_name']." ) ".$result['Item']['alias'];
              $temp['id'] = $result['Item']['id'];
              array_push($final_results, $temp);
          }

          //$autofill = Set::extract('../Item/name', $results);           
          echo json_encode($final_results);

       }
    }    


    public function cityitems($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose a city.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'city'));
        }

        $selectedCity = $this->City->findById($id);

        if($selectedCity == null){
            $this->Session->setFlash('Please choose a city.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'city'));
        }

        // Ishan's fix starts here
        $this->Item->Behaviors->load('Containable');

        // we prepare our query, the cakephp way!
        $this->paginate = array(
            //'conditions' => array('Item.ItemCity.city_id'=>$id),
            'contain' => array('ItemCategory','ItemStock','ItemCity'),
            //'conditions' => array('ItemCity.city_id'=>$id),
            'limit' => 500,
            'order' => array(
                'Item.name' => 'asc',
                'Item.item_category_id' => 'asc'
            )
        );
     
        // we are using the 'User' model
        $searchItems = $this->paginate('Item');

        $finalSearchItems = array();
        foreach ($searchItems as $searchItem) {

            foreach ($searchItem['ItemCity'] as $key => $value) {

                if($value['city_id'] == $id){
                    array_push($finalSearchItems, $searchItem);
                }
                
            }   

        }        

        $this -> set('items', $finalSearchItems);

        //pr($finalSearchItems);die();


        /*$searchItems = $this->Item->find('all',array(
            'order'=>array('Item.name'),
            ));

        $this -> set('city',  $selectedCity);

        $finalSearchItems = array();
        foreach ($searchItems as $searchItem) {

            foreach ($searchItem['ItemCity'] as $key => $value) {

                if($value['city_id'] == $id){
                    array_push($finalSearchItems, $searchItem);
                }
                
            }   

        }*/

        
        //$this -> set('items', $items);

        $this -> set('city',  $selectedCity);
        $this -> set('page_title', $selectedCity['City']['name'].' Items');
        $this -> layout = 'polka_shell1';
    }

}
