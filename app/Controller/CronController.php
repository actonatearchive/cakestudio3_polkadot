<?php

App::uses('CakeTime', 'Utility');
App::uses('CakeEmail', 'Network/Email');
class CronController extends AppController {

	public function beforeFilter() {
	    parent::beforeFilter();
		$this->Auth->allow('incomplete_payment_cron','admin_reset_cron');
	    $this->layout=null;
	}

	public $uses = array('Order', 'OrderItem','User', 'Courier', 'OrderDispatch','City','State','Coupon','Admin','Item','ItemCategory');
	
	/*//This sends email to incomplete payment distinct users
	public function incomplete_payment_cron() {
		// Check the action is being invoked by the cron dispatcher 
		//if (!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); } 

		//no view
		$this->autoRender = false;

		$this->OrderItem->Behaviors->load('Containable');
		$orders = $this -> OrderItem -> find('all',
		array('conditions' => array('Order.status' => 0,
		array('OrderItem.order_dispatch_id'=>null),
		array('NOT'=>array('Order.user_id'=>null))),
		'contain' => array('Order','Order.User'),
		'fields' => array('DISTINCT Order.user_id')
		));

		
		$counter = 0;
		foreach($orders as $order){

			if($counter < 1) {
				//use this as per requirements
				$user = $this -> User -> findById($order['Order']['user_id']);
				$first_name = $user['User']['first_name'];
				$last_name = $user['User']['last_name'];
				$username = $user['User']['username'];
				$mobile = $user['User']['mobile'];
				
				$userEmail = "ishan.sheth@actonate.com";
				$subject = "Reminder mail to place order";
				$message = "Hey, ".$first_name." !<br/><br/>";
				$message .= "You have pending order/s with us at cakestudio. ";			
				$message .= "We'd love to see you ordering cakes.<br/><br/><br/>";
				$message .= "<a href='https://cakestudio.in/users/cart' target = '_blank'><button style='padding:10px;background-color:#C0493B;color:#FFFFFF;font-weight:bold;font-size:16px;border-radius: 3px;outline: none;border: 0px;'>Click Here</button></a> to complete your order.<br/><br/>or visit the following link : https://cakestudio.in/users/cart.<br/><br/>";

				$message .= "For any queries,<br/>";
				$message .= "Write to us at : <a style='color:#1176BD;text-decoration:none;' href='mailto:care@cakestudio.in' style='color:#999999;text-decoration:underline;white-space:nowrap' target='_blank'>care@cakestudio.in</a><br/>";
				$message .= "Call us at : +91 875 876 4638<br/><br/>";


				$message .= "Didn't find the cake you want? Whatsapp pic on +91 875 876 4638 to check availability.<br /><br /><br />";
				$message .= "Thank you<br />";
				$message .= "<strong>CakeStudio Team</strong></p>";
				
				$Email = new CakeEmail('default');
				$Email->config('default');
				$Email->emailFormat("html");
				$Email->template('default');
				$Email -> to($userEmail);
				$Email -> subject($subject);
				$Email -> send($message);
			} else {
				return;
			}

			$counter = $counter + 1;
		}
		return;
	}*/	
	
	
	
	
	//This sends email to polka admins after resetting their password to random string
	public function admin_reset_cron() {
		// Check the action is being invoked by the cron dispatcher 
		// if (!defined('CRON_DISPATCHER')) { $this->redirect('/'); exit(); } 

		//no view
		$this->autoRender = false;

		$length = 15;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#$+=-';
		
		
		$admins = $this -> Admin -> find('all');
		
		foreach($admins as $admin){
		
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, strlen($characters) - 1)];
			}
			$randomString;
			
			$admin['Admin']['password'] = $randomString;
			if ($this->Admin ->save($admin)) {
				$subject = "[Polkadot Security Agent] Your polkadot access password has been reset";
				$message = "Hey, ".$admin['Admin']['name']." !<br/><br/>";
				$message .= "Your polkadot access password has been reset, details are as follows :<br/><br/>";
				$message .= "Username : ".$admin['Admin']['username']."<br/>";
				$message .= "Password : ".$randomString."<br/>";
				
				$Email = new CakeEmail('default');
				$Email->config('default');
				$Email->emailFormat("html");
				$Email->template('default');
				$Email -> to(array('shoaib@actonate.com','pratik@actonate.com','ankur@brewberrys.com','info@cakestudio.in'));
				//$Email -> to(array('ishan.sheth@actonate.com'));
				$Email -> subject($subject);
				$Email -> send($message);				
			}
		}

		return;
	}




		/*public function promo_cron() {

		$this->Item->Behaviors->load('Containable');
		$top3_premium = $this->Item->find('all',array('conditions'=>array('Item.item_category_id'=>'52cd25f0-79a4-4691-b094-5febadc6fdca'),'order'=>'Item.item_viewed DESC','limit'=>6,'contain'=>array()));
		//pr($top3_premium);

		$top3_chocolate = $this->Item->find('all',array('conditions'=>array('Item.item_category_id'=>'52cbdea5-befc-4cec-b3eb-1a64adc6fdca'),'order'=>'Item.item_viewed DESC','limit'=>6,'contain'=>array()));
		//pr($top3_chocolate);

		$top3_customized = $this->Item->find('all',array('conditions'=>array('Item.item_category_id'=>'52cd2b05-5408-4804-af6d-63e6adc6fdca'),'order'=>'Item.item_viewed DESC','limit'=>3,'contain'=>array()));
		//pr($top3_customized);

		
		$this->User->Behaviors->load('Containable');
		$all_users = $this->User->find('all',array('fields'=>array('first_name','username'),'contain'=>array()));
		//pr($all_users);

		$counter = 0;
		foreach($all_users as $user){

			if($counter < 1 ) {
				//use this as per requirements
				$first_name = $user['User']['first_name'];
				$username = $user['User']['username'];
				
				$userEmail = "ishan.sheth@actonate.com";
				//$userEmail = $username;

				if($first_name != "") {
					$subject = $first_name.", Avail friendship day offer at cakestudio.in";
				} else {
					$subject = "Avail friendship day offer at cakestudio.in";
				}

				$tempcnt1 = 0;				
				$tempcnt2 = 0;				

				$message = "<body style='margin: 0; padding: 0;'><div style='background-color:#f5f5f5;margin-left:auto;margin-right:auto;width:730px;'><a href='http://www.cakestudio.in' style='border:0;outline:0;'><img alt='cakestudio.in' height='137' width='731' style='height:137px; width:731px; outline:0; padding:0px; padding-left:0px; padding-right:0px; text-align: center;' class='logo' src='http://cakestudio.in/polkadot/img/promo_mail_header.jpg' align='center' /></a>";			
				$message .= "<div style='background:#FBF8ED;color:#C1493B;font-weight:bold;text-align:center;font-size:35px;width:100%'>Friendship Day offer !!!<br />Flat 10 % Off on all orders.<br />Use coupon code CSL1012</div>";
				$message .= "<div style='background:#FBF8ED;color:#black;text-align:center;font-size:10px;width:100%'>* Offer valid only for cakes to be delivered on 1st, 2nd & 3rd August.<br /></div>";

				$message .= "<div style='background:#C1493B;color:#FBF8ED;font-weight:bold;text-align:center;font-size:25px;width:100%'>Premium Cakes</div>";


				$message .= "<ul>";
				foreach ($top3_premium as $premium) {
					if($tempcnt1 >=3){
						$message .= 	"<li style='display:inline-block;list-style:none;'><a href='https://cakestudio.in' target='_blank' style='color:black;text-decoration:none;'><img alt='cakestudio.in'  style='height:200px; width:200px;' src='http://cakestudio.in/polkadot/files/item/primary_photo/".$premium['Item']['id']."/medium_".rawurlencode($premium['Item']['primary_photo'])."' /><br /><center>".$premium['Item']['name']."</center></a></li>";
					}
					$tempcnt1++;
				}				
				$message .= "</ul>";				



				$message .= "<div style='background:#C1493B;color:#FBF8ED;font-weight:bold;text-align:center;font-size:25px;width:100%'>Chocolate Cakes</div>";

				$message .= "<ul>";
				foreach ($top3_chocolate as $chocolate) {
					if($tempcnt2 >=3){
						$message .= 			"<li style='display:inline-block;list-style:none;'><a href='https://cakestudio.in' target='_blank' style='color:black;text-decoration:none;'><img alt='cakestudio.in'  style='height:200px; width:200px;' src='http://cakestudio.in/polkadot/files/item/primary_photo/".$chocolate['Item']['id']."/medium_".rawurlencode($chocolate['Item']['primary_photo'])."' /><br /><center>".$chocolate['Item']['name']."</center></a></li>";
					}
					$tempcnt2++;
				}				
				$message .= "</ul>";				


				$message .= "<div style='background:#C1493B;color:#FBF8ED;font-weight:bold;text-align:center;font-size:25px;width:100%'>Wedding Cakes</div>";

				$message .= "<ul>";
				foreach ($top3_customized as $customized) {
					$message .= 			"<li style='display:inline-block;list-style:none;'><a href='https://cakestudio.in' target='_blank' style='color:black;text-decoration:none;'><img alt='cakestudio.in'  style='height:200px; width:200px;' src='http://cakestudio.in/polkadot/files/item/primary_photo/".$customized['Item']['id']."/medium_".rawurlencode($customized['Item']['primary_photo'])."' /><br /><center>".$customized['Item']['name']."</center></a></li>";
				}				
				$message .= "</ul>";


				$message .= "<div style='margin-left:20px;float:left;width:100%'><br/><br/>Didn't find the cake you want? Whatsapp pic on +91 875 876 4638 to check availability.<br /><br /><br />";
				$message .= "Thank you<br />";
				$message .= "<strong>CakeStudio Team</strong></div><br /><br />";
				$message .= "<div style='padding-top:5px;text-align:center;background-color:#F5F5F5;'>
								<p class='footer-content' style='font-family:Arial,Helvetica,sans-serif;font-size:10px;line-height:170%;color:#888;padding:12px; padding-top:20px; margin-top:-15px;'>

								      <span style='color:#c1493b;font-size: 14px;font-weight:bold;'>
								          Happiness Develivered @ your Doorstep!
								      </span>
								      <br/>

								      You received this email because you are subscribed to Cakestudio<br>To unsubscribe from e-mails, please <a style='color:#1176BD;text-decoration:none;' href='https://cakestudio.in/pages/contact_us' style='color:#999999;text-decoration:underline;white-space:nowrap' target='_blank'>contact</a>  us.
								      <br/>
								      <br/>

								      &copy; 2013 Cakestudio. All rights reserved.<br/>

								      <a href='http://cakestudio.in/'  style='color:#1176BD;text-decoration:none;' target='_blank'>www.cakestudio.in</a>

								      <br/>

								</div></div></body>";
				
				$Email = new CakeEmail('default');
				$Email->config('default');
				$Email->emailFormat("html");
				//$Email->template('default');
				$Email -> to($userEmail);
				$Email -> subject($subject);
				
				$Email -> send($message);
				//sleep(1);
				//die($message);
				

			} else {
				return;
			}

			$counter = $counter + 1;
		}
		echo "Done";
		return;
	}*/
	
}
?>