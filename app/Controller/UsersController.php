<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */
App::uses('CakeTime', 'Utility');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

	public $name = 'Users';
	var $uses = array('User', 'UserAddress');
	
	public function beforeFilter()
   {
       AppController::beforeFilter();
       $this->Auth->allow('cron_test');
   }	

	public function index() {
		
		$this->UserAddress->Behaviors->load('Containable');
		
		$users = $this -> User -> find('all');
		$this -> set('users', $users);
		
		//pr($users_with_city);die();

		$this -> set('page_title', 'View User Listing');
		$this -> layout = 'polka_shell';		
	
	}
	
	public function email_all() {
		$this -> set('page_title', 'Email all users');
		$this -> layout = 'polka_shell';		
		if ($this -> request -> is('post')) {
		
			$message_data = $this -> request -> data;
			$users = $this -> User -> find('all');
			//pr($message_data);die();
						
			foreach($users as $user){
					$userEmail = $user['User']['username'];
					$subject = $message_data['User']['subject'];
					$message = $message_data['User']['message_body'];
					
					$Email = new CakeEmail('default');
					$Email->config('default');
					$Email->emailFormat("html");
					$Email->template('default');
					$Email -> to($userEmail);
					$Email -> subject($subject);
					$Email -> send($message);
			}
			$this -> Session -> setFlash("Emails sent !",'default',array('class'=>'alert alert-success'),'success');
			$this->redirect(array('controller'=>'users','action'=>'email_all'));
		} 
	}
	
	public function email_city_listed() {
		$this -> set('page_title', 'Email users with city');
		$this -> layout = 'polka_shell';
		
		if ($this -> request -> is('post')) {
		
			$message_data = $this -> request -> data;
			
			$this->UserAddress->Behaviors->load('Containable');
			
			$users_with_city = $this -> UserAddress -> find('all',array('conditions' => array('NOT' => array('UserAddress.city_id' => null)),'contain' => array('User')));
			
			//pr($users_with_city);die();
						
			foreach($users_with_city as $user){
					$userEmail = $user['User']['username'];
					$subject = $message_data['User']['subject'];
					$message = $message_data['User']['message_body'];
					
					$Email = new CakeEmail('default');
					$Email->config('default');
					$Email->emailFormat("html");
					$Email->template('default');
					$Email -> to($userEmail);
					$Email -> subject($subject);
					$Email -> send($message);
			}
			$this -> Session -> setFlash("Emails sent !",'default',array('class'=>'alert alert-success'),'success');
			$this->redirect(array('controller'=>'users','action'=>'email_city_listed'));
		} 
	}	
	
	
	public function email_csv(){
		$this -> set('page_title', 'Email csv users');
		$this -> layout = 'polka_shell';	
		if($this->request->is('post')){
			$data=$this->request->data;			
			$userEmail=array();
			$userEmail=explode(",",$data['User']['csv']);
			$i = 0;
			
			for($y=0;$y<count($userEmail);$y++){
					$subject = $data['User']['subject'];
					$message = $data['User']['message_body'];
					$users = $userEmail[$y];
					
					$Email = new CakeEmail('default');
					$Email->config('default');
					$Email->emailFormat("html");
					$Email->template('default');
					$Email -> to($users);
					$Email -> subject($subject);
					$Email -> send($message);
			}
			
			$this -> Session -> setFlash("Emails sent !",'default',array('class'=>'alert alert-success'),'success');
			
			$this->redirect(array('controller'=>'users','action'=>'email_csv'));
		}
	
	}
	public function viewuser($id) {
		if ($this -> request -> is('post')) {

		} else {
			$user = $this -> User -> findById($id);
			$this -> set('user', $user);
			
			$useradd = $this -> UserAddress -> findById($user['User']['id']);
			$this -> set('useradd', $useradd);
			
			//pr($user);
			//pr($useradd);
			
			$this -> set('page_title', 'View User');
			$this -> layout = 'polka_shell';
		}
	}	

	public function save() {
		$newUser = array();

		$newUser['User']['name'] = "Shoaib";
		$newUser['User']['email'] = "shoaib@actonate.com";
		$newUser['User']['password'] = "sodamm";
		$newUser['User']['mobile'] = "8870511285";

		if ($this -> User -> save($newUser)) {
			$this -> Session -> setFlash("User has been added.");
		} else {
			$this -> Session -> setFlash("NOPE.");
		}
		$this -> redirect(array('controller' => 'users', 'action' => 'index'));
	}
	
	public function export_csv() {
		if ($this -> request -> is('post')) {

			$users = $this -> User -> find('all',array('fields' => array('User.username')));
			//pr($users);
			
			$exported_csv = "";
			foreach($users as $user) {
				$exported_csv .= $user['User']['username'].",";
			}
			$exported_csv = substr($exported_csv,0,-1);
			
			//pr($exported_csv);
			
			
			$this -> set('users', $users);
			
			$this -> set('page_title', 'Export CSV');
			$this -> layout = 'polka_shell';			
		
		} else {
			
			$this -> set('page_title', 'Export CSV');
			$this -> layout = 'polka_shell';
		}
	}

	public function add(){

	}		

}
