<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class CouriersController extends AppController {

	//last edit - Ishan
	public $name = 'Couriers';

	//last edit - Ishan
	public $uses = array('Courier');

	//last edit - Ishan
	public function index() {
		$couriers = $this -> Courier -> find('all');
		$this -> set('couriers', $couriers);

		$this -> set('page_title', 'View Couriers');
		$this -> layout = 'polka_shell';
	}

	//last edit - Ishan
	public function add() {
		if ($this -> request -> is('post')) {
			$courier = $this -> request -> data;

			if ($this -> Courier -> save($courier)) {

				//This flash message has to be set in the view properly
				$this -> Session -> setFlash('New courier added.', 'default', array('class' => 'alert alert-success'), 'success');
				$this -> redirect(array('controller' => 'couriers', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'couriers', 'action' => 'index'));
			}
		} else {

			//display the page for adding couriers
			$this -> set('page_title', 'Add Courier');
			$this -> layout = 'polka_shell';

		}

	}

	//last edit - Ishan
	public function edit($id) {

		if ($this -> request -> is('post')) {
			
			$courier = $this -> request -> data;

			if ($this -> Courier -> save($courier)) {

				//This flash message has to be set in the view properly
				$this -> Session -> setFlash('Courier saved.', 'default', array('class' => 'alert alert-success'), 'success');
				$this -> redirect(array('controller' => 'couriers', 'action' => 'edit'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'couriers', 'action' => 'index'));
			}
		} else {
			
			if ($id == null) {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. Data not found.");
				$this -> redirect(array('controller' => 'couriers', 'action' => 'index'));
			} else {
				//fetch and display the courier details

				$courier = $this -> Courier -> findById($id);
				$this -> set('courier', $courier);

				//print_r($courier);die();

				//display the page for adding couriers
				$this -> set('page_title', 'Edit Courier');
				$this -> layout = 'polka_shell';
			}
		}
	}

	//last edit - Ishan
	public function delete($id) {
			
			//$courier = $this -> Courier -> findById($id);

			if ($this -> Courier -> delete($id)) {

				//This flash message has to be set in the view properly
				$this -> Session -> setFlash('Courier deleted.', 'default', array('class' => 'alert alert-success'), 'success');
				$this -> redirect(array('controller' => 'couriers', 'action' => 'edit'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'couriers', 'action' => 'index'));
			}

	}

}
