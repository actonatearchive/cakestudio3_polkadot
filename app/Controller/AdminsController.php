<?php

class AdminsController extends AppController {

	public function beforeFilter()
    {
        AppController::beforeFilter();

        // Basic setup
        $this->Auth->authenticate = array('Form');
		
        // Pass settings in
        $this->Auth->authenticate = array(
            'Form' => array('userModel' => 'Admin')
        );
        $this->Auth->allow('login');
		
		//$this->Auth->authorize = 'Controller';
	//	parent::isAuthorized();
    }   

	public $name = 'Admins';
	
	var $uses = array('Admin','AdminType');

    public function logout(){
        if($this->Auth->logout()){
			$this->Session->setFlash('Successfully logged out !','default',array('class' =>'alert alert-success'),'good');
            $this->redirect(array('controller'=>'admins','action'=>'login'));
        }
        else{
            $this->redirect(array('controller'=>'admins','action'=>'login'));
        }
    }
    public function login(){
       // pr(AuthComponent::password(')fIzzUoQD#}k'));die();
       $this->layout = "loginlayout";	   

        if($this->request->is('post')){
            if($this->Auth->login()){

					$this->redirect(array('controller'=>'home','action'=>'index'));

            }
            else{
            	
				$this->Session->setFlash('Invalid Username or Password','default',array('class' =>'alert alert-danger'),'bad');
	
    			$this->redirect(array('controller'=>'admins','action'=>'login'));
            }
        }

    }

    public function add(){
        $this->layout="polka_shell";
        $this->set('page_title',"Polkadot users");
        $admin_types=$this->AdminType->find('list',array('fields'=>array('id','name')));
        $this->set('admin_types',$admin_types);
        if($this->request->is('post')){
            $data=$this->data;
            if($this->Admin->save($data)){
                $this->Session->setFlash('New user added.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'admins', 'action' => 'index'));
            } else {
               
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'admins', 'action' => 'index'));
            }
        }
    }
    public function index(){
        $this->layout="polka_shell";
        $this->set('page_title',"Polkadot users");
        $users=$this->Admin->find('all');
        $this->set('users',$users);
    }
    public function edit($id){
        $this->layout="polka_shell";
        $this->set('page_title',"Polkadot users");
        $admin_types=$this->AdminType->find('list',array('fields'=>array('id','name')));
        $this->set('admin_types',$admin_types);
        if(empty($this->data)){
            $this->data=$this->Admin->findById($id);
        }
        else{
            if($this->request->is('post')){
                $data=$this->data;
                 if($this->Admin->save($data)){
                $this->Session->setFlash('Admin updated.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'admins', 'action' => 'index'));
            } else {
               
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'admins', 'action' => 'index'));
            }
            }
        }
    }
	
}?>