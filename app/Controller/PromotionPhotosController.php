<?php
	class PromotionPhotosController extends AppController{
		public $uses = array('PromotionPhoto');

		public function index(){
			$this -> layout = 'polka_shell';
			$this -> set('page_title', 'Promotion Photos');
			$promo_pics = $this -> PromotionPhoto -> find('all');
			$this->set('promos',$promo_pics);
		}

		public function add(){
			$this -> layout = 'polka_shell';
			$this -> set('page_title', 'Promotion Photos');

			if($this->request->is('post')){
				$data=$this->data;

				if($this->PromotionPhoto->save($data)){
				 	$this->Session->setFlash('New promotion photo added.', 'default', array('class' => 'alert alert-success') , 'success');
	                $this -> redirect(array('controller' => 'promotion_photos', 'action' => 'index'));
	            } else {
	               
	                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
	                $this -> redirect(array('controller' => 'promotion_photos', 'action' => 'index'));
	            }
        	}
		}

		public function delete($id){
			$this -> layout = 'polka_shell';
			$this -> set('page_title', 'Promotion Photos');
			 if($id == null){
	            $this->Session->setFlash('Please choose a promotion photo.', 'default', array('class' => 'alert alert-danger') , 'error');
	            $this -> redirect(array('controller' => 'promotion_photos', 'action' => 'index'));
	        }

	        $selectedPromo = $this->PromotionPhoto->findById($id);

	        if( $selectedPromo == null){
	            $this->Session->setFlash('Please choose a promotion photo.', 'default', array('class' => 'alert alert-danger') , 'error');
	            $this -> redirect(array('controller' => 'promotion_photos', 'action' => 'index'));
	        }

	        if($this->PromotionPhoto->delete( $selectedPromo['PromotionPhoto']['id'])){
	            $this->Session->setFlash('Promotion photo deleted.', 'default', array('class' => 'alert alert-success') , 'success');
	            $this -> redirect(array('controller' => 'promotion_photos', 'action' => 'index'));
	        }
	        else{
	            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
	            $this -> redirect(array('controller' => 'promotion_photos', 'action' => 'index'));
	        }

		}

		 public function edit($id=null) {
		 	$this -> layout = 'polka_shell';
			$this -> set('page_title', 'Promotion Photos');

		 	if(empty($this->data)){
		 		$promo=$this->PromotionPhoto->findById($id);
		 		$this->data=$promo;
		 		$this->set('img_data',$promo);
		 	}
		 	else{

		 		$promo=$this->PromotionPhoto->findById($id);
		 		$this->data=$promo;
		 		$this->set('img_data',$promo);

				if($this->request->is('post')){
					$data=$this->data;

					if($this->PromotionPhoto->save($data)){
					 	$this->Session->setFlash('New promotion photo added.', 'default', array('class' => 'alert alert-success') , 'success');
		                $this -> redirect(array('controller' => 'promotion_photos', 'action' => 'index'));
		            } else {
		               
		                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
		                $this -> redirect(array('controller' => 'promotion_photos', 'action' => 'index'));
		            }
	        	}
		 	}

    	}
		
	}
?>