<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class HomeController extends AppController {

	public $name = 'Home';

	public $uses = array('Order', 'OrderItem','User', 'Courier', 'OrderDispatch','City','State','Coupon','Enquiry');

	public function index() {
		date_default_timezone_set('Asia/Calcutta');
		$todays = new DateTime();
		//$tdate1 = $todays->format('Y-m-d');
		//$threshold = $todays->sub(date_interval_create_from_date_string('+30 days'));
		//$tdate2 = $threshold->format('Y-m-d');
		$tdate2 = $todays->format('Y-m-d');
		$tdate1 = $todays->format('Y-m-01');
		//die();

		//This gets the count of completed orders in the last month grouped by date
		$final_orders = array();

		//$startTime = strtotime($tdate2);
		//$endTime = strtotime($tdate1);
		$startTime = strtotime($tdate1);
		$endTime = strtotime($tdate2);
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i);

			$orders = $this->Order->query("SELECT COUNT(id) as id_count, SUM(total_amount) as total_amt, SUM(additional_charges) as additional_chg, SUM(discount_value) as discount_val, SUM(eggless) as eggless, DATE(created) as order_date FROM axi_orders WHERE (status = 1 OR status = 2 OR status = 3) AND DATE(created) = '".$thisDate."' GROUP BY DATE(created);");

			//pr($orders);

			if($orders) {
				array_push($final_orders,$orders[0][0]);
			} else {
				$temparray = array();
				$temparray['id_count'] = 0;
				$temparray['total_amt'] = 0;
				$temparray['additional_chg'] = 0;
				$temparray['discount_val'] = 0;
				$temparray['eggless'] = 0;
				$temparray['order_date'] = $thisDate;
				array_push($final_orders,$temparray);
			}

		}
		//pr($final_orders);


		// Month -1 sales
		$final_orders_month_but1 = array();
		$monthBut1Start = date('Y-m-d', strtotime(date('Y-m')." -1 month"));
		$monthBut1End = date('Y-m-31', strtotime(date('Y-m')." -1 month"));

		$this -> set('month_but1_name', Date('F', strtotime(date('Y-m')." -1 month")));

		$startTimeMonthBut1 = strtotime($monthBut1Start);
		$endTimeMonthBut1 = strtotime($monthBut1End);

		$total_amount_month_but1 = 0;

		for ($i = $startTimeMonthBut1; $i <= $endTimeMonthBut1; $i = $i + 86400) {
			$thisDateMonthBut1 = date('Y-m-d', $i);

			$ordersMonthBut1 = $this->Order->query("SELECT COUNT(id) as id_count, SUM(total_amount) as total_amt, SUM(additional_charges) as additional_chg, SUM(discount_value) as discount_val, SUM(eggless) as eggless, DATE(created) as order_date FROM axi_orders WHERE (status = 1 OR status = 2 OR status = 3) AND DATE(created) = '".$thisDateMonthBut1."' GROUP BY DATE(created);");

			//pr($ordersMonthBut1);

			if($ordersMonthBut1) {
				array_push($final_orders_month_but1,$ordersMonthBut1[0][0]);

				$total_amount_month_but1 += $ordersMonthBut1[0][0]['total_amt']+$ordersMonthBut1[0][0]['additional_chg']+$ordersMonthBut1[0][0]['eggless']-$ordersMonthBut1[0][0]['discount_val'];
			} else {
				$temparray = array();
				$temparray['id_count'] = 0;
				$temparray['total_amt'] = 0;
				$temparray['additional_chg'] = 0;
				$temparray['discount_val'] = 0;
				$temparray['eggless'] = 0;
				$temparray['order_date'] = $thisDate;
				array_push($final_orders_month_but1,$temparray);
			}

		}
		// die($total_amount_month_but1);
		$this -> set('total_amount_month_but1', $total_amount_month_but1);


		// Month -2 sales
		$final_orders_month_but2 = array();
		$monthBut2Start = date('Y-m-d', strtotime(date('Y-m')." -2 month"));
		$monthBut2End = date('Y-m-31', strtotime(date('Y-m')." -2 month"));

		$this -> set('month_but2_name', Date('F', strtotime(date('Y-m')." -2 month")));

		$startTimeMonthBut2 = strtotime($monthBut2Start);
		$endTimeMonthBut2 = strtotime($monthBut2End);

		$total_amount_month_but2 = 0;

		for ($i = $startTimeMonthBut2; $i <= $endTimeMonthBut2; $i = $i + 86400) {
			$thisDateMonthBut2 = date('Y-m-d', $i);

			$ordersMonthBut2 = $this->Order->query("SELECT COUNT(id) as id_count, SUM(total_amount) as total_amt, SUM(additional_charges) as additional_chg, SUM(discount_value) as discount_val, SUM(eggless) as eggless, DATE(created) as order_date FROM axi_orders WHERE (status = 1 OR status = 2 OR status = 3) AND DATE(created) = '".$thisDateMonthBut2."' GROUP BY DATE(created);");

			if($ordersMonthBut2) {
				array_push($final_orders_month_but2,$ordersMonthBut2[0][0]);

				$total_amount_month_but2 += $ordersMonthBut2[0][0]['total_amt']+$ordersMonthBut2[0][0]['additional_chg']+$ordersMonthBut2[0][0]['eggless']-$ordersMonthBut2[0][0]['discount_val'];
			} else {
				$temparray = array();
				$temparray['id_count'] = 0;
				$temparray['total_amt'] = 0;
				$temparray['additional_chg'] = 0;
				$temparray['discount_val'] = 0;
				$temparray['eggless'] = 0;
				$temparray['order_date'] = $thisDate;
				array_push($final_orders_month_but2,$temparray);
			}

		}
		//die($total_amount_month_but2);
		$this -> set('total_amount_month_but2', $total_amount_month_but2);



		// Month -3 sales
		$final_orders_month_but3 = array();
		$monthBut3Start = date('Y-m-d', strtotime(date('Y-m')." -3 month"));
		$monthBut3End = date('Y-m-31', strtotime(date('Y-m')." -3 month"));

		$this -> set('month_but3_name', Date('F', strtotime(date('Y-m')." -3 month")));

		$startTimeMonthBut3 = strtotime($monthBut3Start);
		$endTimeMonthBut3 = strtotime($monthBut3End);

		$total_amount_month_but3 = 0;

		for ($i = $startTimeMonthBut3; $i <= $endTimeMonthBut3; $i = $i + 86400) {
			$thisDateMonthBut3 = date('Y-m-d', $i);

			$ordersMonthBut3 = $this->Order->query("SELECT COUNT(id) as id_count, SUM(total_amount) as total_amt, SUM(additional_charges) as additional_chg, SUM(discount_value) as discount_val, SUM(eggless) as eggless, DATE(created) as order_date FROM axi_orders WHERE (status = 1 OR status = 2 OR status = 3) AND DATE(created) = '".$thisDateMonthBut3."' GROUP BY DATE(created);");

			//pr($ordersMonthBut3);

			if($ordersMonthBut3) {
				array_push($final_orders_month_but3,$ordersMonthBut3[0][0]);

				$total_amount_month_but3 += $ordersMonthBut3[0][0]['total_amt']+$ordersMonthBut3[0][0]['additional_chg']+$ordersMonthBut3[0][0]['eggless']-$ordersMonthBut3[0][0]['discount_val'];
			} else {
				$temparray = array();
				$temparray['id_count'] = 0;
				$temparray['total_amt'] = 0;
				$temparray['additional_chg'] = 0;
				$temparray['discount_val'] = 0;
				$temparray['eggless'] = 0;
				$temparray['order_date'] = $thisDate;

				array_push($final_orders_month_but3,$temparray);
			}

		}
		//die($total_amount_month_but3);
		$this -> set('total_amount_month_but3', $total_amount_month_but3);



		//This gets the count of registered users in the last month grouped by date
/*		$final_users = array();

		$startTime = strtotime($tdate2);
		$endTime = strtotime($tdate1);
		for ($i = $startTime; $i <= $endTime; $i = $i + 86400) {
			$thisDate = date('Y-m-d', $i);

			$users = $this->User->query("SELECT COUNT(id) as id_count,DATE(created) as created FROM axi_users WHERE DATE(created) = '".$thisDate."' GROUP BY DATE(created);");

			if($users) {
				array_push($final_users,$users[0][0]);
			} else {
				$temparray = array();
				$temparray['id_count'] = 0;
				$temparray['created'] = $thisDate;
				array_push($final_users,$temparray);
			}

		}	*/
		//pr($final_users);

		//This gets all the completed orders till now
		$all_orders = $this->Order->find('all',array('conditions'=>array('OR' => array(array('Order.status'=>1),array('Order.status'=>2),array('Order.status'=>3)))));

		//This gets all the shipping addresses of completed orders till now
		$completed_shipping_cities = array();
		foreach($all_orders as $order) {
			if($order['ShippingAddress']['city_id'] != null) {
				array_push($completed_shipping_cities,$order['ShippingAddress']['city_id']);
			}

		}

		//pr($completed_shipping_cities);

		//This gets the final array of completed order count, grouped by city for all completed orders
		$weighted_cities = array_count_values($completed_shipping_cities);
		//pr($weighted_cities);

		arsort($weighted_cities);

		//pr($weighted_cities);

		//This gets all the cities
		$all_cities = $this->City->find('all',array('fields'=>array('City.id','City.name')));
		//pr($all_cities);

		//This gets the final array of completed order count, grouped by city for all cities
		$final_cities = array();
		$zero_cities = array();

		foreach($all_cities as $city) {
			if (!array_key_exists($city['City']['id'], $weighted_cities)) {
				//$temparray = array();
				//$temparray[$city['City']['name']] = 0;
				//array_push($zero_cities,$temparray);
				$zero_cities[$city['City']['name']] = 0;
			} else {
				//$temparray = array();
				//$temparray[$city['City']['name']] = $weighted_cities[$city['City']['id']];
				//array_push($final_cities,$temparray);
				$final_cities[$city['City']['name']] = $weighted_cities[$city['City']['id']];
			}
		}

		//pr($final_cities);

		arsort($final_cities);

		//pr($final_cities);

		//pr($zero_cities);

		foreach ($zero_cities as $key=>$value) {
			//array_push($final_cities,$city);
			$final_cities[$key] = $value;
		}
		//pr($final_cities);

		//arsort($zero_cities);

		//pr($final_cities);




		// This is for last 5 orders
		$this->OrderItem->Behaviors->load('Containable');

		$top_orders = $this -> OrderItem -> find('all',array(
		'conditions' => array('Order.status' => 3),
		'contain' => array('Order','Order.User'),
		'order' => array('Order.modified DESC'),
		'limit' => 5
		));
		$this -> set('top_orders', $top_orders);

		//pr($top_orders);

		/*$top_users = $this -> User -> find('all',array(
		'order' => array('User.created DESC'),
		'limit' => 5
		));
		$this -> set('top_users', $top_users);*/

		//pr($top_users);

		$this -> set('page_title', 'View Orders');


		//$this -> set('final_users', $final_users);
		$this -> set('final_orders', $final_orders);
		$this -> set('final_cities', $final_cities);
		$this -> set('page_title', 'Dashboard');
		$this -> layout = 'polka_shell';
	}

    public function states(){

        $statesJSON = file_get_contents("http://localhost/states.json");
        $states = json_decode($statesJSON, true);

        $newStates = array();
        foreach($states as $state){
            $newState = array();
            $newState['State']['name'] = $state['name'];
            $newState['State']['country_id'] = "d1d9b829-5f62-11e3-938c-24b6fd3dcdb4";

            echo $newState['State']['name']."<br/>";

            array_push($newStates, $newState);

        }

        $this->State->saveAll($newStates);

        die();
    }
    public function enquiry() {
      $enquires = $this->Enquiry->find('all',array('order'=>'created DESC'));
      $this -> set('enquires', $enquires);
      $this -> layout = 'polka_shell';
      $this -> set('page_title', 'View Enquires');
    }

}
