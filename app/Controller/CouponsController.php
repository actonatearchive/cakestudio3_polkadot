<?php

class CouponsController extends AppController {

	public $name = 'Coupons';
	
	var $uses = array('Coupon','Item','ItemCategory','City','CouponCity');

    public function index(){

        $coupons = $this -> Coupon -> find('all');

        $finalCoupons = array();
        foreach($coupons as $coupon) {
            $itemTemp = array();
            $itemCatTemp = array();

            $itemTemp = $this -> Item -> findById($coupon['Coupon']['item_id']);
            $itemCatTemp = $this -> ItemCategory -> findById($coupon['Coupon']['item_category_id']);

            //pr($itemTemp['Item']['name']);
            //pr($itemCatTemp['ItemCategory']['name']);

            $coupon['Coupon']['item_id'] =  isset($itemTemp['Item']['name'])? $itemTemp['Item']['name'] : null ;
            $coupon['Coupon']['item_category_id'] = isset($itemCatTemp['ItemCategory']['name'])? $itemCatTemp['ItemCategory']['name'] : null ;

            array_push($finalCoupons, $coupon);
        }

        

        $this -> set('coupons', $finalCoupons);

        $this -> set('page_title', 'Coupons');
        $this -> layout = 'polka_shell';
    }

    public function add() {
            
        if ($this -> request -> is('post')) {
            $coupon = $this -> request -> data;

            //die();

            if($coupon['Coupon']['item_category_id'] == ""){
                $coupon['Coupon']['item_category_id'] = null;
            }
            if($coupon['Coupon']['item_id'] == ""){
                $coupon['Coupon']['item_id'] = null;
            } 
            if($coupon['Coupon']['percent'] == ""){
                $coupon['Coupon']['percent'] = 0;
            } 
            if($coupon['Coupon']['value'] == ""){
                $coupon['Coupon']['value'] = 0;
            }  
            if($coupon['Coupon']['max_count'] == ""){
                $coupon['Coupon']['max_count'] = 0;
            }  
            if($coupon['Coupon']['max_value'] == ""){
                $coupon['Coupon']['max_value'] = 0;
            }                                    

            //pr($coupon);


            if ($this -> Coupon -> save($coupon)) {
                //Get the last insert id
                $last_id = $this->Coupon->getInsertID();

                //pr($last_id);

                //Set Cities
                $couponCities = array();
                foreach($coupon['Coupon']['city_id'] as $key=>$value){
                    $newCouponCity = array();
                    $newCouponCity['CouponCity']['city_id'] = $value;
                    $newCouponCity['CouponCity']['coupon_id'] = $last_id;

                    array_push($couponCities, $newCouponCity);
                }

                //pr($couponCities);

                if(sizeof($couponCities)>0)
                {
                    $this->CouponCity->saveAll($couponCities);
                }

                $this-> Session ->setFlash('Coupon added.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
            } else {

                $this-> Session ->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
            }            

            $this -> set('page_title', 'Add Coupon');
            $this -> layout = 'polka_shell';  


        } else {

            // Set the items
            $items = $this -> Item -> find('all', 
                array(
                    'conditions' => [
                        'Item.item_id !=' => null,
                        'Item.disabled' => 0
                    ],
                    'order' => 'Item.name ASC'
                )
            );
            $itemsList = array();
            foreach($items as $item){
                $itemsList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }
            $this -> set('items', $itemsList);        

            // Set the item categories
            $itemCats = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC'));
            $this -> set('item_categories', $itemCats);        

            // Set the type of coupon options here
            $couponType = array(0=>'Percent', 1=>'Value');
            $this -> set('coupon_type', $couponType);            
        
            // Set cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);

            $this -> set('page_title', 'Add Coupon');
            $this -> layout = 'polka_shell';  

        }            

     
    }

    public function edit($id=null) {

        if($this -> request -> is('post') ) {

            $coupon = $this -> request -> data;

            //pr($coupon);die();

            if(!isset($coupon['Coupon']['shipping_free'])) {
                $coupon['Coupon']['shipping_free'] = 0;
            }

            if(!isset($coupon['Coupon']['active'])) {
                $coupon['Coupon']['active'] = 0;
            }            

            if ($this -> Coupon -> save($coupon)) {
            
                //Remove Existing Cities
                $this->CouponCity->deleteAll(array('CouponCity.coupon_id'=>$coupon['Coupon']['id']));
                
                //Set Cities
                $couponCities = array();
                foreach($coupon['Coupon']['city_id'] as $key=>$value){
                    $newCouponCity = array();
                    $newCouponCity['CouponCity']['city_id'] = $value;
                    $newCouponCity['CouponCity']['coupon_id'] = $coupon['Coupon']['id'];

                    array_push($couponCities, $newCouponCity);
                }

                if(sizeof($couponCities)>0)
                {
                    $this->CouponCity->saveAll($couponCities);
                }       
                
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Selected Coupon edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'Coupons', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'Coupons', 'action' => 'index'));
            }




        } else {
            if($id == null){
                $this->Session->setFlash('Please choose a coupon.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
            }

            $selectedCoupon = $this->Coupon->findById($id);

            //pr($selectedCoupon);

            if($selectedCoupon == null){
                $this->Session->setFlash('Please choose a coupon.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
            }
            
            //Get Coupon Cities
            $couponCities = $this->CouponCity->find('all',array('conditions'=>array('CouponCity.coupon_id'=>$selectedCoupon['Coupon']['id'])));

            $couponCitiesList = array();
            foreach($couponCities as $CouponCity){
                array_push($couponCitiesList, $CouponCity['CouponCity']['city_id']);
            }

            $this->set('selectedCitiesList',$couponCitiesList);         
            
            //Get All Cities
            $cities = $this->City->find('list',array('order'=>array('City.name')));
            $this->set('cities',$cities);


            // Set the items
            $items = $this -> Item -> find('all', 
                array(
                    'conditions' => [
                        'Item.item_id !=' => null,
                        'Item.disabled' => 0                        
                    ],
                    'order' => 'Item.name ASC'
                )
            );
            $itemsList = array();
            foreach($items as $item){
                $itemsList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }
            $this -> set('items', $itemsList);        

            // Set the item categories
            $itemCats = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC'));
            $this -> set('item_categories', $itemCats); 

            // Set the type of coupon options here
            $couponType = array(0=>'Percent', 1=>'Value');
            $this -> set('coupon_type', $couponType);

            $this->set('coupon',$selectedCoupon);
            $this -> set('page_title', 'Edit Coupon');
            $this -> layout = 'polka_shell';
        }

    }

    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose a coupon.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
        }

        $selectedCoupon = $this->Coupon->findById($id);

        if($selectedCoupon == null){
            $this->Session->setFlash('Please choose a coupon.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }

        if($this->Coupon->delete($selectedCoupon['Coupon']['id'])){

            $this->CouponCity->deleteAll(array('CouponCity.coupon_id' => $id), false);

            $this->Session->setFlash('Coupon deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }
    }

    public function set_active($id=null) {
        if($id == null){
            $this->Session->setFlash('Please choose a coupon.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
        }

        $selectedCoupon = $this->Coupon->findById($id);

        if($selectedCoupon == null){
            $this->Session->setFlash('Please choose a coupon.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }


        $selectedCoupon['Coupon']['active']=1;

        if($this->Coupon->save($selectedCoupon)){
            $this->Session->setFlash('Coupon set to active.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }
        
    }

    public function set_inactive($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose a coupon.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
        }

        $selectedCoupon = $this->Coupon->findById($id);

        if($selectedCoupon == null){
            $this->Session->setFlash('Please choose a coupon.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }


        $selectedCoupon['Coupon']['active']=0;

        if($this->Coupon->save($selectedCoupon)){
            $this->Session->setFlash('Coupon set to inactive.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'coupons', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'items', 'action' => 'index'));
        }        
    }            
	
}?>