<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class CitiesController extends AppController {

	public $name = 'Cities';

	public $uses = array('City', 'State');

	public function index() {
		$cities = $this -> City -> find('all',array('order'=>array('City.name')));
		$this -> set('cities', $cities);

		$this -> set('page_title', 'Cities');
		$this -> layout = 'polka_shell';
	}

	public function add() {

		if ($this -> request -> is('post')) {
			$city = $this -> request -> data;

            if($city['City']['shipping_handling_charges']==""){
                $city['City']['shipping_handling_charges']="";
            }
            $city['City']['name'] = ucfirst($city['City']['name']);

            if(strlen($city['City']['name'])<3){
                $this->Session->setFlash('Min 3 characters in City Name', 'default', array('class' => 'alert alert-danger') , 'error');
                $this->redirect($this->referer());
            }

            $city['City']['code'] = $city['City']['name'][0] . $city['City']['name'][1] . $city['City']['name'][2];

            $city['City']['code'] = strtoupper($city['City']['code']);


			if ($this -> City -> save($city)) {
				//This flash message has to be set in the view properly
                $this->Session->setFlash('New city added.', 'default', array('class' => 'alert alert-success') , 'success');
				$this -> redirect(array('controller' => 'cities', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
				$this -> redirect(array('controller' => 'cities', 'action' => 'index'));
			}
		}
        else{
            $states = $this->State->find('list',array('order'=>array('State.name')));
            $this->set('states',$states);
        }
        $this -> set('page_title', 'Add City');
        $this -> layout = 'polka_shell';
	}

	public function toggle_disable($id=null) {
		if ($id == null) {
			//This flash message has to be set in the view properly
			$this->Session->setFlash('You must select a city.', 'default', array('class' => 'alert alert-danger') , 'error');
			$this -> redirect(array('controller' => 'cities', 'action' => 'index'));	
		} else {
			$city = $this->City->findById($id);
			
			if($city['City']['disabled']==0){
				$city['City']['disabled'] = 1;
			} else {
				$city['City']['disabled'] = 0;
			}
			
            if ($this -> City -> save($city)) {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Selected city updated.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
            }		
		}
		
		$this -> set('page_title', 'Toggle City Diabled');
        $this -> layout = 'polka_shell';
	}
	

    public function edit($id=null) {

        if ($this -> request -> is('post')) {
            $city = $this -> request -> data;

            if($city['City']['shipping_handling_charges']==""){
                $city['City']['shipping_handling_charges']="";
            }

            $city['City']['name'] = ucfirst($city['City']['name']);

            if(strlen($city['City']['name'])<3){
                $this->Session->setFlash('Min 3 characters in City Name', 'default', array('class' => 'alert alert-danger') , 'error');
                $this->redirect($this->referer());
            }

            $city['City']['code'] = $city['City']['name'][0] . $city['City']['name'][1] . $city['City']['name'][2];

            $city['City']['code'] = strtoupper($city['City']['code']);


            if ($this -> City -> save($city)) {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Selected city edited.', 'default', array('class' => 'alert alert-success') , 'success');
                $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
            } else {
                //This flash message has to be set in the view properly
                $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
            }
        }

        else{

            if($id == null){
                $this->Session->setFlash('Please choose a city.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
            }

            $states = $this->State->find('list',array('order'=>array('State.name')));
            $this->set('states',$states);

            $selectedCity = $this->City->findById($id);

            if($selectedCity == null){
                $this->Session->setFlash('Please choose a city.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
            }

            $this->set('selectedCity',$selectedCity);
            $this -> set('page_title', 'Edit City');
            $this -> layout = 'polka_shell';

        }
    }


    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose a city.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
        }

        $selectedCity = $this->City->findById($id);

        if($selectedCity == null){
            $this->Session->setFlash('Please choose a city.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
        }

        if($this->City->delete($selectedCity['City']['id'])){
            $this->Session->setFlash('City deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this -> redirect(array('controller' => 'cities', 'action' => 'index'));
        }

        $this -> set('page_title', 'Edit City');
        $this -> layout = 'polka_shell';
    }


}
