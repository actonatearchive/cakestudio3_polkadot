<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class ItemGalleryPhotosController extends AppController {

	public $name = 'ItemGalleryPhotos';
	
	public $uses = array('Item', 'ItemCategory','ItemStock','City','ItemCity','ItemGalleryPhoto');

	public function manage_gallery($id=null) {
	
		if ($this -> request -> is('post')) {
		
			$data_packets = $this->request->data;
			
			//echo "<br />This is the only post data array : <br/>";
			//pr($data_packets);
			
			//$this->ItemGalleryPhoto->save($data_packets)
			
			$id = $data_packets['ItemGalleryPhoto']['item_id'];
			//$file_dir = $data_packets['Items']['file_dir'];
			
			//Set Array
			$filenames = array();
			foreach($data_packets['ItemGalleryPhoto']['file_names'] as $key=>$value){
				$filename = array();
				$filename['item_id'] = $id;
				//$filename['file_dir'] = $file_dir;
				$filename['file_name']['name'] = $value['name'];
				$filename['file_name']['type'] = $value['type'];
				$filename['file_name']['tmp_name'] = $value['tmp_name'];
				$filename['file_name']['size'] = $value['size'];
				$filename['file_name']['error'] = $value['error'];
				$filename['featured'] = 0;
				$filename['model'] = 'ItemGalleryPhoto';
				
				array_push($filenames, $filename);
				
			}
			
			//echo "<br />This is the only ItemGalleryPhotos object array : <br/>";
			//pr($filenames);

			
			
			// Redirect if item id not set
			if($id == null){
                $this->Session->setFlash('You must select an item.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'add_gallery'));
            }
			
			// Fetch item details if item id is set
            $selectedItem = $this->Item->findById($id);
            

			// Redirect if details for the item id not available
            if($selectedItem == null){
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'index'));
            }
            $this->set('selectedItem',$selectedItem);
			
			
			
			// Here is the upload handler
			if($this->ItemGalleryPhoto->saveMany($filenames)) {
				$this->Session->setFlash('Item gallery photo/s added.', 'default', array('class' => 'alert alert-success') , 'success');
			} else {
				$this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
			}
			
			
			// Get item categories
            $itemCats = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC'));
            $this -> set('item_categories', $itemCats);

			// Get item parents
			
            $itemParents = $this -> Item -> find('all', array('order' => 'Item.name ASC','conditions'=>array('Item.item_id'=>null)));
            $itemParentsList = array();
            foreach($itemParents as $item){
                $itemParentsList[$item['Item']['id']] = $item['Item']['name']." (".$item['Item']['variant_name'].") ".$item['Item']['alias'];
            }

            $this -> set('item_parents', $itemParentsList);	

			$itemGalleryPhotos = $this -> ItemGalleryPhoto -> find('all', array('conditions' => array('ItemGalleryPhoto.item_id' => $id)));
			$this -> set('itemGalleryPhotos', $itemGalleryPhotos);
			
			//pr($itemGalleryPhotos);
		
			$this -> set('page_title', 'Manage Gallery');
            $this -> layout = 'polka_shell';
			
		} else {
		
			// Redirect if item id not set
			if($id == null){
                $this->Session->setFlash('You must select an item.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'addgallery'));
            }
			
			//pr($id);
			
			// Fetch item details if item id is set
            $selectedItem = $this->Item->findById($id);
            //pr($selectedItem);die();

			// Redirect if details for the item id not available
            if($selectedItem == null){
                $this->Session->setFlash('Sorry an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
                $this -> redirect(array('controller' => 'items', 'action' => 'index'));
            }
            $this->set('selectedItem',$selectedItem);

			// Get item categories
            $itemCats = $this -> ItemCategory -> find('list', array('order' => 'ItemCategory.name ASC'));
            $this -> set('item_categories', $itemCats);

			$itemGalleryPhotos = $this -> ItemGalleryPhoto -> find('all', array('conditions' => array('ItemGalleryPhoto.item_id' => $id)));
			$this -> set('itemGalleryPhotos', $itemGalleryPhotos);
			
			//pr($itemGalleryPhotos);
			
            $this -> set('page_title', 'Manage Gallery');
            $this -> layout = 'polka_shell';			
			
			
		}

	}

    public function delete($id=null) {

        if($id == null){
            $this->Session->setFlash('Please choose an item gallery photo.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this->redirect($this->referer());
        }

        $selectedItemGP = $this->ItemGalleryPhoto->findById($id);

        if($selectedItemGP == null){
            $this->Session->setFlash('Please choose an item gallery photo.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this->redirect($this->referer());
        }

        if($this->ItemGalleryPhoto->delete($id)){
            $this->Session->setFlash('Item gallery photo deleted.', 'default', array('class' => 'alert alert-success') , 'success');
            $this->redirect($this->referer());
        }
        else{
            $this->Session->setFlash('Sorry, an error occurred.', 'default', array('class' => 'alert alert-danger') , 'error');
            $this->redirect($this->referer());
        }
    }		
	
		
}