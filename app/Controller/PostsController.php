<?php
class PostsController extends AppController {
    var $name = 'Posts';
	var $uses = array('Post','EventRegistration');
	var $helpers = array('Html','Js', 'Form','Csv','excel');

	public function beforeFilter()
    {       
        $this->Auth->allow('exportMmber','eventExport','registrationExport');
    }	
	public function registration(){

		$all_user_list= $this->EventRegistration->find('all',array('conditions' => array('EventRegistration.Status' => 1),'order'=>array('EventRegistration.created DESC')));
		if(sizeof($all_user_list) <= 0 ){
			$this->Session->setFlash('Sorry, No data avilable.', 'default', array('class' => 'message error') , 'error');
	        $this->redirect(array('controller' => 'home','action' => 'dashboard'));
		}
		$temp_arr = array();
		// arranging field for csv file
		$srno = 1;
		foreach ($all_user_list as $key => $value) {

			$temp['EventRegistration']['Sr.No'] = $srno;
			$temp['EventRegistration']['first_name']  =  $value['EventRegistration']['first_name'];
			$temp['EventRegistration']['middle_name']  =  $value['EventRegistration']['middle_name'];
			$temp['EventRegistration']['last_name'] = $value['EventRegistration']['last_name'];
			$temp['EventRegistration']['email'] =$value['EventRegistration']['email'];
			$temp['EventRegistration']['mobile'] =$value['EventRegistration']['mobile'];
			
			$temp['EventRegistration']['address_line1'] =$value['EventRegistration']['addressline1'];
			$temp['EventRegistration']['address_line2'] =$value['EventRegistration']['addressline1'];
			$temp['EventRegistration']['zipcode'] =$value['EventRegistration']['pincode'];	
			$temp['EventRegistration']['city_name'] =$value['EventRegistration']['city'];
			$temp['EventRegistration']['state_name'] =$value['EventRegistration']['state'];
			$temp['EventRegistration']['code'] = $value['EventRegistration']['code'];
			$temp['EventRegistration']['name_of_institute'] = $value['EventRegistration']['name_of_institute'];
			$temp['EventRegistration']['membershipno'] = $value['EventRegistration']['membershipno'];			
			$temp['EventRegistration']['guj_council_reg_no'] = $value['EventRegistration']['guj_council_reg_no'];
			$temp['EventRegistration']['office_contact'] = $value['EventRegistration']['office_contact'];

			$temp['EventRegistration']['conference'] = $this->categoryArr[$value['EventRegistration']['main_package']];
			$temp['EventRegistration']['workshop'] = $this->categoryArr[$value['EventRegistration']['workshop']];
			$temp['EventRegistration']['cme'] = $this->categoryArr[$value['EventRegistration']['cme']];
			$temp['EventRegistration']['banquet'] = $this->categoryArr[$value['EventRegistration']['banquet']];

			$temp['EventRegistration']['accompany'] = $value['EventRegistration']['accompany'];
			$temp['EventRegistration']['residential package-3 days'] = $value['EventRegistration']['days3'];
			$temp['EventRegistration']['residential package-2 days'] = $value['EventRegistration']['days2'];
			
			$temp['EventRegistration']['Amount'] = $value['EventRegistration']['total_amt'];
			

			if($value['EventRegistration']['status'] == 3)
			{
				$temp['EventRegistration']['status'] = "Offline Registration";
			}
			else if ($value['EventRegistration']['status'] == 1) {
				$temp['EventRegistration']['status'] = "online Registration";
			}		
			
			$temp['EventRegistration']['created'] = date("d-m-Y", strtotime($value['EventRegistration']['created']));
			$srno++;
			array_push($temp_arr, $temp);
		}
		
		$this->set('posts',$temp_arr);
		$this->layout = null;
		$this->autoLayout = false;
		Configure::write('debug','0');
	}
	public function offlineReg(){

		$all_user_list= $this->EventRegistration->find('all',array('conditions' => array('EventRegistration.Status' =>3),'order'=>array('EventRegistration.created DESC')));
		if(sizeof($all_user_list) <= 0 ){
			$this->Session->setFlash('Sorry, No data avilable.', 'default', array('class' => 'message error') , 'error');
	        $this->redirect(array('controller' => 'home','action' => 'dashboard'));
		}
		$temp_arr = array();
		// arranging field for csv file
		$srno = 1;
		foreach ($all_user_list as $key => $value) {

			$temp['EventRegistration']['Sr.No'] = $srno;
			$temp['EventRegistration']['first_name']  =  $value['EventRegistration']['first_name'];
			$temp['EventRegistration']['middle_name']  =  $value['EventRegistration']['middle_name'];
			$temp['EventRegistration']['last_name'] = $value['EventRegistration']['last_name'];
			$temp['EventRegistration']['email'] =$value['EventRegistration']['email'];
			$temp['EventRegistration']['mobile'] =$value['EventRegistration']['mobile'];
			
			$temp['EventRegistration']['address_line1'] =$value['EventRegistration']['addressline1'];
			$temp['EventRegistration']['address_line2'] =$value['EventRegistration']['addressline1'];
			$temp['EventRegistration']['zipcode'] =$value['EventRegistration']['pincode'];	
			$temp['EventRegistration']['city_name'] =$value['EventRegistration']['city'];
			$temp['EventRegistration']['state_name'] =$value['EventRegistration']['state'];
			$temp['EventRegistration']['code'] = $value['EventRegistration']['code'];
			$temp['EventRegistration']['name_of_institute'] = $value['EventRegistration']['name_of_institute'];
			$temp['EventRegistration']['membershipno'] = $value['EventRegistration']['membershipno'];			
			$temp['EventRegistration']['guj_council_reg_no'] = $value['EventRegistration']['guj_council_reg_no'];
			$temp['EventRegistration']['office_contact'] = $value['EventRegistration']['office_contact'];

			$temp['EventRegistration']['conference'] = $this->categoryArr[$value['EventRegistration']['main_package']];
			$temp['EventRegistration']['workshop'] = $this->categoryArr[$value['EventRegistration']['workshop']];
			$temp['EventRegistration']['cme'] = $this->categoryArr[$value['EventRegistration']['cme']];
			$temp['EventRegistration']['banquet'] = $this->categoryArr[$value['EventRegistration']['banquet']];

			$temp['EventRegistration']['accompany'] = $value['EventRegistration']['accompany'];
			$temp['EventRegistration']['residential package-3 days'] = $value['EventRegistration']['days3'];
			$temp['EventRegistration']['residential package-2 days'] = $value['EventRegistration']['days2'];
			
			$temp['EventRegistration']['Amount'] = $value['EventRegistration']['total_amt'];

			if($value['EventRegistration']['status'] == 3)
			{
				$temp['EventRegistration']['status'] = "Offline Registration";
			}
			else if ($value['EventRegistration']['status'] == 1) {
				$temp['EventRegistration']['status'] = "online Registration";
			}		
			
			$temp['EventRegistration']['created'] = date("d-m-Y", strtotime($value['EventRegistration']['created']));
			$srno++;
			array_push($temp_arr, $temp);
		}
		
		$this->set('posts',$temp_arr);
		$this->layout = null;
		$this->autoLayout = false;
		Configure::write('debug','0');
	}
	public function allReg(){

		$all_user_list= $this->EventRegistration->find('all',array('conditions' => array('EventRegistration.Status' =>array(1,3)),'order'=>array('EventRegistration.created DESC')));
		if(sizeof($all_user_list) <= 0 ){
			$this->Session->setFlash('Sorry, No data avilable.', 'default', array('class' => 'message error') , 'error');
	        $this->redirect(array('controller' => 'home','action' => 'dashboard'));
		}
		$temp_arr = array();
		// arranging field for csv file
		$srno = 1;
		foreach ($all_user_list as $key => $value) {

			$temp['EventRegistration']['Sr.No'] = $srno;
			$temp['EventRegistration']['first_name']  =  $value['EventRegistration']['first_name'];
			$temp['EventRegistration']['middle_name']  =  $value['EventRegistration']['middle_name'];
			$temp['EventRegistration']['last_name'] = $value['EventRegistration']['last_name'];
			$temp['EventRegistration']['email'] =$value['EventRegistration']['email'];
			$temp['EventRegistration']['mobile'] =$value['EventRegistration']['mobile'];
			
			$temp['EventRegistration']['address_line1'] =$value['EventRegistration']['addressline1'];
			$temp['EventRegistration']['address_line2'] =$value['EventRegistration']['addressline1'];
			$temp['EventRegistration']['zipcode'] =$value['EventRegistration']['pincode'];	
			$temp['EventRegistration']['city_name'] =$value['EventRegistration']['city'];
			$temp['EventRegistration']['state_name'] =$value['EventRegistration']['state'];
			$temp['EventRegistration']['code'] = $value['EventRegistration']['code'];
			$temp['EventRegistration']['name_of_institute'] = $value['EventRegistration']['name_of_institute'];
			$temp['EventRegistration']['membershipno'] = $value['EventRegistration']['membershipno'];			
			$temp['EventRegistration']['guj_council_reg_no'] = $value['EventRegistration']['guj_council_reg_no'];
			$temp['EventRegistration']['office_contact'] = $value['EventRegistration']['office_contact'];

			$temp['EventRegistration']['conference'] = $this->categoryArr[$value['EventRegistration']['main_package']];
			$temp['EventRegistration']['workshop'] = $this->categoryArr[$value['EventRegistration']['workshop']];
			$temp['EventRegistration']['cme'] = $this->categoryArr[$value['EventRegistration']['cme']];
			$temp['EventRegistration']['banquet'] = $this->categoryArr[$value['EventRegistration']['banquet']];

			$temp['EventRegistration']['accompany'] = $value['EventRegistration']['accompany'];
			$temp['EventRegistration']['residential package-3 days'] = $value['EventRegistration']['days3'];
			$temp['EventRegistration']['residential package-2 days'] = $value['EventRegistration']['days2'];
			
			$temp['EventRegistration']['Amount'] = $value['EventRegistration']['total_amt'];

			if($value['EventRegistration']['status'] == 3)
			{
				$temp['EventRegistration']['status'] = "Offline Registration";
			}
			else if ($value['EventRegistration']['status'] == 1) {
				$temp['EventRegistration']['status'] = "online Registration";
			}		
			
			$temp['EventRegistration']['created'] = date("d-m-Y", strtotime($value['EventRegistration']['created']));
			$srno++;
			array_push($temp_arr, $temp);
		}
		
		$this->set('posts',$temp_arr);
		$this->layout = null;
		$this->autoLayout = false;
		Configure::write('debug','0');
	}
	public function eventExport(){
		$data = $this->Event->find('all',array('fields'=>array('name','short_des','long_des','from_date','event_time','to_date','link','last_registration_date','created')));
		//pr($data);die();
		if(sizeof($data) <= 0){
			$this->Session->setFlash('Sorry, No data available.', 'default', array('class' => 'alert alert-danger') , 'error');
			$this->redirect(array('controller'=>'Events','action'=>'index'));
		}
		$temp_arr = array();
		foreach ($data as $key => $value) {
			$value['Event']['long_des'] = strip_tags($value['Event']['long_des']);	
			array_push($temp_arr, $value);
		}
		$this->set('posts',$temp_arr);
		
			$this->layout = null;
			$this->autoLayout = false;
			Configure::write('debug','0');
	}
	
}

?>