<?php
/**
*   New Items Controller
*
*
* @author Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
*/
class NewItemsController extends AppController {
    public $uses = ['Item','ItemCategory','City','ItemCity'];
    public $components = array('Paginator');

    /**
    *   Add Item
    *
    * @author Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
    * @return void
    */
    public function add()
    {
        $this->layout = 'polka_shell';
        $this -> set('page_title', 'Add Item');


        if ($this->request->is('post')) {
            $data = $this->request->data;

            // $this->log("Data");
            // $this->log($data);
            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            $base_item = [];
            $parent_item_id = null;

            //check URL SLAG must be Unique.

            $chk_url_slag = $this->Item->findByUrlSlag($data['Item']['url_slag']);

            if (!empty($chk_url_slag)) {
                $this->Session->setFlash('URL Slag already exists. Please choose different one.', 'default', array('class' => 'alert alert-danger') , 'error');
                return $this -> redirect(array('controller' => 'new_items', 'action' => 'view'));
            }

            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            $base_item['Item']['name'] = $data['Item']['name'];
            $base_item['Item']['keyword'] = $data['Item']['keyword'];
            $base_item['Item']['item_category_id'] = $data['Item']['item_category_id'];
            $base_item['Item']['short_desc'] = $data['Item']['short_desc'];
            $base_item['Item']['shipping_charges'] = $data['Item']['shipping_charges'];
            $base_item['Item']['url_slag'] = $data['Item']['url_slag'];
            $base_item['Item']['alias'] = $data['Item']['alias'];
            $base_item['Item']['sku_code'] = $data['Item']['sku_code'];
            // $base_item['Item']['variant_name'] = $data['Item']['base_variant_name'];

            // $base_item['Item']['featured'] = $data['Item']['featured']; //old
            if (isset($data['Item']['featured'])) {
                $base_item['Item']['featured'] = $data['Item']['featured'];
            } else {
                $base_item['Item']['featured'] = 0;
                $data['Item']['featured'] = 0;
            }

            if (isset($data['Item']['is_addon'])) {
                $base_item['Item']['is_addon'] = $data['Item']['is_addon'];
            } else {
                $base_item['Item']['is_addon'] = 0;
                $data['Item']['is_addon'] = 0;
            }

            $child_item_data = $base_item;

            $base_item['Item']['primary_photo']['name'] = $data['Item']['primary_photo']['name'];
            $base_item['Item']['primary_photo']['type'] = $data['Item']['primary_photo']['type'];
            $base_item['Item']['primary_photo']['tmp_name'] = $data['Item']['primary_photo']['tmp_name'];
            $base_item['Item']['primary_photo']['size'] = $data['Item']['primary_photo']['size'];
            $base_item['Item']['primary_photo']['error'] = $data['Item']['primary_photo']['error'];

            // $item_primary_photo = $base_item['Item']['primary_photo']['name'];

            // $this->log("item_primary_photo: ");
            // $this->log($item_primary_photo);
            //Base Item
            if ($result = $this->Item->save($base_item)) {
                $parent_item_id = $result['Item']['id'];
                // $parent_discount_percentage = $this->getDiscountPercentage($data['Item']['base_price'], $data['Item']['base_discount_price']);

                // $this->log("BASE City Ids: ");
                // $this->log($data['Item']['base_city_id']);

                //addItemCities
                // Note: -> This Will be Executed After All Variants are inserted for Parent
                // $this->addItemCities($parent_item_id, $data['Item']['base_city_id'], $data['Item']['base_price'], $data['Item']['base_discount_price'], $parent_discount_percentage);


                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                //child_items / (Optional) if there are any child so this will be executed.
                //Rest it will be ignored.
                if (isset($data['Item']['variant_name'][0])) {
                    //It has Multiple Variants
                    //Let's make Proper Array
                    $variant_count = sizeof($data['Item']['variant_name']);
                    $all_city_ids = [];
                    $child_items = [];
                    for ($i = 0 , $j = 1; $i < $variant_count; $i++, $j++)
                    {
                        $tmp = [];
                        $tmp['city_id'] = $data['Item']['city_id'.$i];

                        if (!empty($tmp['city_id'])) {
                            foreach ($tmp['city_id'] as $val_city_id) {
                                array_push($all_city_ids, $val_city_id);
                            }
                        }

                        $tmp['variant_name'] = $data['Item']['variant_name'][$i];
                        $tmp['price'] = $data['Item']['price'][$i];
                        $tmp['discount_price'] = $data['Item']['discount_price'][$i];
                        $tmp['discount_percentage'] = $this->getDiscountPercentage($tmp['price'], $tmp['discount_price']);
                        //MultiPle Variants
                        $this->Item->create();
                        $child_item_data['Item']['variant_name'] = $tmp['variant_name'];
                        $child_item_data['Item']['item_id'] = $parent_item_id;
                        $child_item_data['Item']['sku_code'] = $data['Item']['sku_code'].$i;
                        $child_item_data['Item']['featured'] = $data['Item']['featured'];
                        $child_item_data['Item']['sequence'] = $j; //Item Sequence
                        // $child_item_data['Item']['primary_photo'] = $base_item['Item']['primary_photo']['name'];
                        // $child_item_data['Item']['primary_photo_directory'] = $result['Item']['id'];

                        if ($child_resp = $this->Item->save($child_item_data)) {
                            //Child Item Saved!
                            // $this->log("CHILD_RECORD");
                            // $this->log($child_resp);

                            $tmp['item_id'] = $child_resp['Item']['id'];
                            array_push($child_items, $tmp);
                        }
                    }
                    // $this->log("CHILD_ITEMS");
                    // $this->log($child_items);
                    if ($child_items != null) {
                        //Array Formed
                        //Let's Go through
                        $all_city_ids = array_unique($all_city_ids);
                        //addItemCities
                        // Note: -> This Will be Executed After All Variants are inserted for Parent
                        $this->addItemCities($parent_item_id, $all_city_ids, 0.0, 0.0, 0.0);

                        //-=====-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                        foreach ($child_items as $key => $value) {
                            //addItemCities
                            $this->addItemCities($value['item_id'], $value['city_id'], $value['price'], $value['discount_price'], $value['discount_percentage']);
                        }

                    }

                }
                //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                //All Records saved!!
                $this->Session->setFlash('New item added.', 'default', array('class' => 'alert alert-success') , 'success');
                return $this -> redirect(array('controller' => 'new_items', 'action' => 'view'));
            }

        }



        //-------------------------------------------
        // View Purpose Only
        $categories = $this->ItemCategory->find('list',
            [
                'conditions' => [
                    'ItemCategory.disabled' => 0
                ]
            ]
        );

        $cities = $this->City->find('list', [
                'conditions' => [
                    'City.disabled' => 0
                ]
            ]
        );

        $this->set('categories', $categories);
        $this->set('cities', $cities);
    }

    /**
    *   View Items
    *
    * @author Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
    * @return void
    */
    public function view()
    {
        $this->layout = 'polka_shell';
        $this -> set('page_title', 'View Items');

        $this->Item->Behaviors->load('Containable');

        if ($this->request->is('post')) {
            $data = $this->request->data;

            $this->Paginator->settings = array(
                'conditions' => [
                    'Item.item_id' => null,
                    'Item.name LIKE' => '%'.$data['Item']['search_term'].'%',
                    'Item.disabled !=' => 2
                ],
                'limit' => 30,
                'order' => 'Item.created desc',
                'contain' => [
                    'ItemCity',
                    'ItemCategory',
                    'ChildItems' => [
                        'conditions' => [
                            'ChildItems.disabled !='  => 2
                        ]
                    ],
                    'ChildItems.ItemCity'
                ]
            );

            $items = $this->Paginator->paginate('Item');
            $this -> set('items', $items);

            // pr($items);
            // die();
        } else {
            $this->Paginator->settings = array(
                'conditions' => [
                    'Item.item_id' => null,
                    'Item.disabled !=' => 2
                ],
                'limit' => 20,
                'order' => 'Item.created desc',
                'contain' => [
                    'ItemCity',
                    'ItemCategory',
                    'ChildItems' => [
                        'conditions' => [
                            'ChildItems.disabled !='  => 2
                        ]
                    ],
                    'ChildItems.ItemCity'
                ]
            );

           $items = $this->Paginator->paginate('Item');
           $this -> set('items', $items);
        }
    //    pr($items);
    }

    /**
    *   Edit Item
    *
    * @return void
    */
    public function edit($id = null)
    {
        if ($id != null) {


            $this->Item->Behaviors->load('Containable');
            $item = $this->Item->find('first',
                [
                    'conditions' => [
                        'Item.id' => $id,
                        'Item.disabled !=' => 2
                    ],
                    'contain' => [
                        'ItemCity',
                        'ItemCategory',
                        'ChildItems' => [
                            'conditions' => [
                                'ChildItems.disabled !='  => 2
                            ]
                        ],
                        'ChildItems.ItemCity'
                    ]
                ]
            );
            // pr($item);

            if ($this->request->is('post')) {
                //
                $data = $this->request->data;

                // $this->log("REQUEST_DATA");
                // $this->log($data);
                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                $base_item = [];
                $parent_item_id = null;

                //check URL SLAG must be Unique.
                $chk_url_slag = $this->Item->find('first',
                    [
                        'conditions' => [
                            'Item.id !='=> $id,
                            'Item.item_id'=>null,
                            'Item.url_slag' => $data['Item']['url_slag']
                        ]
                    ]
                );
                // pr($chk_url_slag);
                if (!empty($chk_url_slag)) {
                    $this->Session->setFlash('URL Slag already exists. Please choose different one.', 'default', array('class' => 'alert alert-danger') , 'error');
                    return $this -> redirect(array('controller' => 'new_items', 'action' => 'edit', $id));
                }
                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                $base_item['Item']['name'] = $data['Item']['name'];
                $base_item['Item']['keyword'] = $data['Item']['keyword'];
                $base_item['Item']['item_category_id'] = $data['Item']['item_category_id'];
                $base_item['Item']['short_desc'] = $data['Item']['short_desc'];
                $base_item['Item']['shipping_charges'] = $data['Item']['shipping_charges'];
                $base_item['Item']['url_slag'] = $data['Item']['url_slag'];
                $base_item['Item']['alias'] = $data['Item']['alias'];
                $base_item['Item']['sku_code'] = $data['Item']['sku_code'];
                // $base_item['Item']['variant_name'] = $data['Item']['base_variant_name'];
                if (isset($data['Item']['featured'])) {
                    $base_item['Item']['featured'] = $data['Item']['featured'];
                } else {
                    $base_item['Item']['featured'] = 0;
                    $data['Item']['featured'] = 0;
                }

                if (isset($data['Item']['is_addon'])) {
                    $base_item['Item']['is_addon'] = $data['Item']['is_addon'];
                } else {
                    $base_item['Item']['is_addon'] = 0;
                    $data['Item']['is_addon'] = 0;
                }
                
                $child_item_data = $base_item;

                if ($data['Item']['primary_photo']['size'] != 0) {
                    $base_item['Item']['primary_photo']['name'] = $data['Item']['primary_photo']['name'];
                    $base_item['Item']['primary_photo']['type'] = $data['Item']['primary_photo']['type'];
                    $base_item['Item']['primary_photo']['tmp_name'] = $data['Item']['primary_photo']['tmp_name'];
                    $base_item['Item']['primary_photo']['size'] = $data['Item']['primary_photo']['size'];
                    $base_item['Item']['primary_photo']['error'] = $data['Item']['primary_photo']['error'];
                }


                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                //Base Item
                $base_item['Item']['id'] = $id;
                if ($result = $this->Item->save($base_item)) {
                    $parent_item_id = $result['Item']['id'];

                    // pr($item);
                    // $parent_discount_percentage = $this->getDiscountPercentage($data['Item']['base_price'], $data['Item']['base_discount_price']);

                    // $this->log("BASE City Ids: ");
                    // $this->log($data['Item']['base_city_id']);

                    //addItemCities
                    // $this->addItemCities($parent_item_id, $data['Item']['base_city_id'], $data['Item']['base_price'], $data['Item']['base_discount_price'], $parent_discount_percentage);

                    //=-----------------------=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                    //child_items / (Optional) if there are any child so this will be executed.
                    //Rest it will be ignored.
                    if (isset($data['Item']['variant_name'][0])) {
                        //It has Multiple Variants
                        //Let's make Proper Array
                        $variant_count = sizeof($data['Item']['variant_name']);
                        $all_city_ids = [];
                        $child_items = [];
                        for ($i = 0, $j = 0 ; $i < $variant_count; $i++, $j++)
                        {
                            $tmp = [];
                            $tmp['city_id'] = $data['Item']['city_id'.$i];

                            if (!empty($tmp['city_id'])) {
                                foreach ($tmp['city_id'] as $val_city_id) {
                                    array_push($all_city_ids, $val_city_id);
                                }
                            }

                            $tmp['variant_name'] = $data['Item']['variant_name'][$i];
                            $tmp['price'] = $data['Item']['price'][$i];
                            $tmp['discount_price'] = $data['Item']['discount_price'][$i];
                            $tmp['discount_percentage'] = $this->getDiscountPercentage($tmp['price'], $tmp['discount_price']);

                            if (isset($item['ChildItems'][$i]['id'])) {
                                $child_item_data['Item']['id'] = $item['ChildItems'][$i]['id'];
                            } else {
                                //createID
                                // $this->Item->create();
                                $child_item_data['Item']['id'] = $this->UUIDv4();
                            }
                            $child_item_data['Item']['variant_name'] = $tmp['variant_name'];
                            $child_item_data['Item']['item_id'] = $parent_item_id;
                            $child_item_data['Item']['sku_code'] = $data['Item']['sku_code'].$i;
                            $child_item_data['Item']['featured'] = $data['Item']['featured'];
                            $child_item_data['Item']['sequence'] = $j; //Item Sequence
                            // $child_item_data['Item']['primary_photo'] = $item['Item']['primary_photo'];
                            // $child_item_data['Item']['primary_photo_directory'] = $item['Item']['primary_photo_directory'];

                            if ($child_resp = $this->Item->save($child_item_data)) {
                                $tmp['item_id'] = $child_resp['Item']['id'];
                                array_push($child_items, $tmp);
                            }
                        }

                        // $this->log("CHILD_ITEMS");
                        // $this->log($child_items);
                        if ($child_items != null) {
                            //Array Formed
                            //Let's Go through

                            $all_city_ids = array_unique($all_city_ids);
                            //addItemCities
                            // Note: -> This Will be Executed After All Variants are inserted for Parent
                            $this->addItemCities($parent_item_id, $all_city_ids, 0.0, 0.0, 0.0);

                            foreach ($child_items as $key => $value) {
                                //addItemCities
                                $this->addItemCities($value['item_id'], $value['city_id'], $value['price'], $value['discount_price'], $value['discount_percentage']);
                            }
                        }
                    }
                    //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                    //All Records saved!!
                    $this->Session->setFlash('Item updated.', 'default', array('class' => 'alert alert-success') , 'success');
                    return $this -> redirect(array('controller' => 'new_items', 'action' => 'view'));
                }
                // pr($data); die();
            }





            $this->layout = 'polka_shell';
            $this -> set('page_title', 'Edit Item');

            $item['base_city_id'] = [];
            foreach ($item['ItemCity'] as $key => $val2) {
                # code...
                array_push($item['base_city_id'], $val2['city_id']);
            }

            $idx = 0;
            foreach ($item['ChildItems'] as $key => $val2) {
                $item['ChildItems'][$idx]['city_id'] = [];
                foreach ($val2['ItemCity'] as $key => $value) {
                    $this->log($value);
                    array_push($item['ChildItems'][$idx]['city_id'], $value['city_id']);
                }
                $idx++;
            }

            $this->set('child_counts', sizeof($item['ChildItems']) - 1);

            // View Purpose Only
            $categories = $this->ItemCategory->find('list',
                [
                    'conditions' => [
                        'ItemCategory.disabled' => 0
                    ]
                ]
            );

            $cities = $this->City->find('list', [
                    'conditions' => [
                        'City.disabled' => 0
                    ]
                ]
            );

            $this->set('categories', $categories);
            $this->set('cities', $cities);
            $this->set('data', $item);
            // pr($item);
        }
    }

    /**
    *   Set/Unset Featured
    *
    * @return redirect
    */
    public function set_featured($item_id = null, $featured = 1)
    {
        if ($item_id == null) {
            $this->Session->setFlash('Item not found.', 'default', array('class' => 'alert alert-danger') , 'error');
            return $this -> redirect(array('controller' => 'new_items', 'action' => 'view'));
        }

        $this->Item->updateAll(
            ['Item.featured' => $featured],
            ['Item.id' => $item_id]
        );

        $this->Item->updateAll(
            ['Item.featured' => $featured],
            ['Item.item_id' => $item_id]
        );
        if ($featured == 1) {
            $this->Session->setFlash('Item is now featured.', 'default', array('class' => 'alert alert-success') , 'success');
        } else {
            $this->Session->setFlash('Item is no more featured.', 'default', array('class' => 'alert alert-info') , 'success');
        }
        return $this -> redirect(array('controller' => 'new_items', 'action' => 'view'));
    }

    /**
    *   Set/Unset Featured
    *
    * @return redirect
    */
    public function set_disabled($item_id = null, $disabled = 1)
    {
        if ($item_id == null) {
            $this->Session->setFlash('Item not found.', 'default', array('class' => 'alert alert-danger') , 'error');
            return $this -> redirect(array('controller' => 'new_items', 'action' => 'view'));
        }

        $this->Item->updateAll(
            ['Item.disabled' => $disabled],
            ['Item.id' => $item_id]
        );

        // $this->Item->updateAll(
        //     ['Item.disabled' => $disabled],
        //     ['Item.item_id' => $item_id]
        // );
        if ($disabled == 1) {
            $this->Session->setFlash('Item is now Disabled.', 'default', array('class' => 'alert alert-info') , 'success');
        } else {
            $this->Session->setFlash('Item is now Enabled.', 'default', array('class' => 'alert alert-success') , 'success');
        }
        return $this -> redirect(array('controller' => 'new_items', 'action' => 'view'));
    }

    private function getDiscountPercentage($price = null, $discount_price = null )
    {
        if ($price != null || $discount_price != null) {
            return round(($price - $discount_price)/$price,2)*100;
        }
        return 0;
    }

    private function addItemCities($item_id = null, $city_ids = [], $price = null, $discount_price = null, $discount_percentage = null)
    {
        //Delete Existing Items If it is exists.
        $this->ItemCity->deleteAll(
            [
                'ItemCity.item_id' => $item_id
            ]
        );
        //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



        $item_city = [];
        $item_city['ItemCity']['item_id'] = $item_id;
        $item_city['ItemCity']['price'] = $price;
        $item_city['ItemCity']['discount_price'] = $discount_price;
        $item_city['ItemCity']['discount_percentage'] = $discount_percentage;

        $this->log("City_ids");
        $this->log($city_ids);
        //addItemCities
        foreach ($city_ids as $key => $value) {
            $this->ItemCity->create();
            $tmp_item_city = [];

            $tmp_item_city = $item_city;
            $tmp_item_city['ItemCity']['city_id'] = $value;
            $this->log("BEFORE SAVE CITY_ID: ");
            $this->log($value);


            if ($aresp = $this->ItemCity->save($tmp_item_city)) {
                $this->log("AFTER SAVE CITY_ID: ");
                $this->log($value);
                $this->log($aresp);
                //Record Saved!
                //Let it Run!
            }
        }
    }

    /**
    *
    *
    * @return redirect
    */
    public function removeItemChild($child_id = null, $item_id = null)
    {
        if ($child_id != null && $item_id != null) {
            $this->Item->id = $child_id;
            $tmp = [];
            $tmp['Item']['disabled'] = 2; //Removed

            if ($this->Item->save($tmp)) {
                //All Records saved!!
                $this->Session->setFlash('Variant removed.', 'default', array('class' => 'alert alert-success') , 'success');
                return $this -> redirect(array('controller' => 'new_items', 'action' => 'edit',$item_id));
            }

            $this->Session->setFlash('Oops!! Something went wrong.', 'default', array('class' => 'alert alert-danger') , 'error');
            return $this -> redirect(array('controller' => 'new_items', 'action' => 'view'));
        }
    }


    /**
     *  Universal Unique Indentifier v4
     *    DATE: 30th January 2017
     *
     * @return uuid
     * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
     */
    private function UUIDv4()
    {
        return sprintf(
            '%s-%04x-%04x-%04x-%04x%04x%04x',
            substr(uniqid('', true), 0, 8), //UUIDv1 Based on time
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

}
?>
