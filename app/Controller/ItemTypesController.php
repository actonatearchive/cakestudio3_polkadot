<?php
/**
 * Created by IntelliJ IDEA.
 * User: Shoaib Merchant
 * Date: 10/4/13
 * Time: 2:37 AM
 * To change this template use File | Settings | File Templates.
 */

class ItemTypesController extends AppController {

	public $name = 'ItemTypes';

	public $uses = array('ItemType');

	public function index() {
		$itemTypes = $this -> ItemType -> find('all');
		$this -> set('item_types', $itemTypes);

		$this -> set('page_title', 'Item Types');
		$this -> layout = 'polka_shell';
	}

	public function add() {
		if ($this -> request -> is('post')) {
			$itemType = $this -> request -> data;
			if ($this -> ItemType -> save($itemType)) {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("New item type added.");
				$this -> redirect(array('controller' => 'itemtypes', 'action' => 'index'));
			} else {
				//This flash message has to be set in the view properly
				$this -> Session -> setFlash("Sorry. an error occurred.");
				$this -> redirect(array('controller' => 'itemtypes', 'action' => 'index'));
			}
		} else {
			//This flash message has to be set in the view properly
			$this -> Session -> setFlash("Sorry, an error occurred.");
			$this -> redirect(array('controller' => 'itemtypes', 'action' => 'index'));
		}
	}

}
